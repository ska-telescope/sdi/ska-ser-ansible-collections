"""
filters.py provides custom ansible filters usable in all collections
"""

import base64
import copy
import hashlib
import json
import os
import re
import textwrap
from collections import defaultdict
from pathlib import Path
from typing import Any

import yaml
from ansible.template import AnsibleUndefined


class CustomDumper(yaml.Dumper):  # pylint: disable=too-many-ancestors
    """
    CustomDumpers implements a dumper for yaml files to properly handle
    multiline strings
    """

    def represent_scalar(self, tag, value, style=None):
        if isinstance(value, str):
            if "\n" in value:
                style = "|"
            elif len(value) > 256:
                value = "\n".join(textwrap.wrap(value, width=128))
                style = "|"

        return super().represent_scalar(tag, value, style)


def get_variable_context():
    """
    Gets context to inject as variables for environment
    variable resolution
    """
    context_path = os.environ.get("ANSIBLE_VAR_CONTEXT", None)
    if context_path is None:
        return {}

    context = {}
    with open(context_path, "r", encoding="utf-8") as file:
        for line in [line.strip("\n") for line in file]:
            if "=" in line:
                name, var = line.split("=", 1)
                context[name.strip()] = var

    return context


def find_manifest_idx(manifests, kind):
    """
    Find manifest with a particular kind in a set of manifests
    """
    return next(
        (
            idx
            for idx, manifest in enumerate(manifests)
            if manifest.get("kind") == kind
        ),
        -1,
    )


def process_cluster_manifests(manifests: list[dict]):
    """
    Process cluster, adding hashed suffixes to manifest names
    and updating references accordingly.
    """
    if len(manifests) == 0:
        return manifests

    osc_manifest_idx = find_manifest_idx(manifests, "OpenStackCluster")
    cluster_manifest_idx = find_manifest_idx(manifests, "Cluster")

    if osc_manifest_idx == -1 or cluster_manifest_idx == -1:
        raise ValueError("Missing required manifests for 'cluster'")

    osc_spec_hash = hashlib.md5(
        base64.b64encode(
            json.dumps(
                manifests[osc_manifest_idx]["spec"], sort_keys=True
            ).encode("utf-8")
        )
    ).hexdigest()[:8]

    # Patch osc
    osc_name = manifests[osc_manifest_idx]["metadata"]["name"]
    osc_specd_name = f"{osc_name}-{osc_spec_hash}"
    manifests[osc_manifest_idx]["metadata"]["name"] = osc_specd_name

    # Patch cluster
    manifests[cluster_manifest_idx]["spec"]["infrastructureRef"][
        "name"
    ] = osc_specd_name

    return manifests


def process_control_plane_manifests(manifests: list[dict]):
    """
    Process control plane, adding hashed suffixes to manifest names
    and updating references accordingly.
    """
    if len(manifests) == 0:
        return manifests

    omt_manifest_idx = find_manifest_idx(manifests, "OpenStackMachineTemplate")
    kcp_manifest_idx = find_manifest_idx(manifests, "KubeadmControlPlane")

    if omt_manifest_idx == -1 or kcp_manifest_idx == -1:
        raise ValueError("Missing required manifests for 'control-plane'")

    omt_spec_hash = hashlib.md5(
        base64.b64encode(
            json.dumps(
                manifests[omt_manifest_idx]["spec"], sort_keys=True
            ).encode("utf-8")
        )
    ).hexdigest()[:8]

    # Patch omt
    omt_name = manifests[omt_manifest_idx]["metadata"]["name"]
    omt_specd_name = f"{omt_name}-{omt_spec_hash}"
    manifests[omt_manifest_idx]["metadata"]["name"] = omt_specd_name

    # Patch kct
    manifests[kcp_manifest_idx]["spec"]["machineTemplate"][
        "infrastructureRef"
    ]["name"] = omt_specd_name

    return manifests


def process_worker_group_manifests(group: str, manifests: list[dict]):
    """
    Process a single worker group, adding hashed suffixes to manifest names
    and updating references accordingly.
    """
    omt_manifest_idx = find_manifest_idx(manifests, "OpenStackMachineTemplate")
    kct_manifest_idx = find_manifest_idx(manifests, "KubeadmConfigTemplate")
    md_manifest_idx = find_manifest_idx(manifests, "MachineDeployment")

    if (
        omt_manifest_idx == -1
        or kct_manifest_idx == -1
        or md_manifest_idx == -1
    ):
        raise ValueError(
            f"Missing required manifests for worker group '{group}'"
        )

    omt_spec_hash = hashlib.md5(
        base64.b64encode(
            json.dumps(
                manifests[omt_manifest_idx]["spec"], sort_keys=True
            ).encode("utf-8")
        )
    ).hexdigest()[:8]

    kct_spec_hash = hashlib.md5(
        base64.b64encode(
            json.dumps(
                manifests[kct_manifest_idx]["spec"], sort_keys=True
            ).encode("utf-8")
        )
    ).hexdigest()[:8]

    # Patch omt
    omt_name = manifests[omt_manifest_idx]["metadata"]["name"]
    omt_specd_name = f"{omt_name}-{omt_spec_hash}"
    manifests[omt_manifest_idx]["metadata"]["name"] = omt_specd_name

    # Patch kct
    kct_name = manifests[kct_manifest_idx]["metadata"]["name"]
    kct_specd_name = f"{kct_name}-{kct_spec_hash}"
    manifests[kct_manifest_idx]["metadata"]["name"] = kct_specd_name

    # Patch md
    manifests[md_manifest_idx]["spec"]["template"]["spec"]["bootstrap"][
        "configRef"
    ]["name"] = kct_specd_name
    manifests[md_manifest_idx]["spec"]["template"]["spec"][
        "infrastructureRef"
    ]["name"] = omt_specd_name

    return manifests


class FilterModule:
    """
    FilterModule provides custom filters for ansible playbooks and roles
    """

    def filters(self):
        """
        Export filters
        """
        return {
            "oneliner": self.oneliner,
            "default_to_env": self.get_env,
            "stringify": self.stringify,
            "to_kubelet_args": self.to_kubelet_args,
            "get_mongo_url": self.get_mongo_url,
            "get_policy_files": self.get_policy_files,
            "inject_spec_hash_in_crds": self.inject_spec_hash_in_crds,
            "format_manifests": self.format_manifests,
        }

    def oneliner(self, line: str):
        """
        Converts a multiline command into a oneliner
        """
        if not line:
            return line

        return re.sub(r"[ ]+", " ", line.replace("\\\n", "").replace("\n", ""))

    def get_env(
        self,
        value: Any,
        env_var: str,
        required: bool = True,
        default: Any = None,
    ):
        """
        Gets the value of an environment variable or looks into other places
        """
        if not isinstance(value, AnsibleUndefined):
            return value

        var_value = get_variable_context().get(
            env_var, os.environ.get(env_var, default)
        )
        if var_value is None and required:
            raise ValueError("Unable to find default variable value")

        return var_value

    def stringify(self, value: Any, sep=",", kv_sep="="):
        """
        Converts any value to string. In the case of lists or dicts, it
        separates them using 'sep'. For dicts, separates the key from the
        value using 'kv_sep'
        """

        if isinstance(value, list):
            return sep.join(value)

        if isinstance(value, dict):
            return sep.join(
                [f"{key}{kv_sep}{val}" for key, val in value.items()]
            )

        return str(value)

    def to_kubelet_args(self, value, as_json=True):
        """
        Converts an object into a clean object or json. All
        values must be strings
        """
        new_value = copy.copy(value)
        flag_ops = {
            "eviction-hard": {"kv_sep": "<"},
        }

        if not isinstance(value, dict) or value is None:
            return value

        for key, val in value.items():
            key_ops = flag_ops.get(key, {})
            new_value[key] = (
                r"%s"  # pylint: disable=consider-using-f-string
                % self.stringify(
                    val, key_ops.get("sep", ","), key_ops.get("kv_sep", "=")
                )
            )

        if not as_json:
            return new_value

        return json.dumps(new_value, sort_keys=False)

    def format_manifests(self, docs: list[str]):
        """
        Formats a list of Kubernetes manifests
        """
        documents = list(yaml.safe_load_all("\n---\n".join(docs)))
        return yaml.dump_all(
            [doc for doc in documents if doc not in [None, ""]],
            Dumper=CustomDumper,
            default_flow_style=False,
            indent=2,
        )

    def get_mongo_url(
        self,
        mongo_config: dict,
        service_prefix="ska-cicd-artefact-validations",
        service_suffix="mongodb-headless:27017",
    ):
        """
        Creates the mongo url string, taking into account the various replicas
        """
        is_created_mongo = mongo_config.get("created", True)
        mongo_service = f"{service_prefix}-{service_suffix}"
        mongo_hosts = mongo_config.get("hosts", [mongo_service])
        if is_created_mongo:
            mongo_hosts = []
            for replica_id in range(0, mongo_config.get("replicas", 1)):
                mongo_hosts.append(
                    f"{service_prefix}-mongodb-{replica_id}.{mongo_service}"
                )

        if len(mongo_hosts) == 0:
            raise ValueError(
                "Couldn't determine Mongodb hosts.\
Please provide `hosts` information."
            )

        user = mongo_config.get("username", "root")
        password = mongo_config.get("password", "root")
        options = []
        for key, value in mongo_config.get("options", {}).items():
            options.append(f"{key}={value}")

        joined_hosts = ",".join(mongo_hosts)
        joined_opts = "&".join(options)
        return f"mongodb://{user}:{password}@{joined_hosts}/?{joined_opts}"

    def get_policy_files(
        self, dirs: list[str], exclude_policies: list[str] = None
    ):
        """
        Finds policy files in the specified directories, excluding policies
        by name
        """
        regex_excludes = [
            re.compile(expression.encode("utf-8"))
            for expression in (
                [] if exclude_policies is None else exclude_policies
            )
        ]
        policy_files = []
        for directory in dirs:
            for path in set(
                list(Path(directory).rglob("*.yml"))
                + list(Path(directory).rglob("*.yaml"))
            ):
                include_policy = True
                posix_path = path.as_posix()
                for exclude in regex_excludes:
                    if (
                        exclude.match(posix_path.encode("utf-8"))
                        or exclude.pattern.decode("utf-8") in posix_path
                    ):
                        include_policy = False
                        break

                if include_policy:
                    policy_files.append(posix_path)

        return policy_files

    def inject_spec_hash_in_crds(self, docs: str):
        """
        Injects a suffix in the name and resource references based on the
        md5 hash of the `spec` field. Supports multiple sets of manifests
        grouped by `metadata.labels["capi.skao.int/worker-group"]` or
        `metadata.labels["capi.skao.int/component"]`
        """
        manifests = [
            manifest
            for manifest in list(yaml.safe_load_all(docs))
            if manifest not in [None, ""]
        ]
        cluster_manifests = []
        control_plane_manifests = []
        grouped_manifests = defaultdict(list)
        updated_manifests = []
        for manifest in manifests:
            if "metadata" in manifest and "labels" in manifest["metadata"]:
                group = manifest["metadata"]["labels"].get(
                    "capi.skao.int/worker-group"
                )
                if group:
                    grouped_manifests[group].append(manifest)
                elif (
                    manifest["metadata"]["labels"].get(
                        "capi.skao.int/component"
                    )
                    == "control-plane"
                ):
                    control_plane_manifests.append(manifest)
                elif (
                    manifest["metadata"]["labels"].get(
                        "capi.skao.int/component"
                    )
                    == "cluster"
                ):
                    cluster_manifests.append(manifest)
                else:
                    updated_manifests.append(manifest)

        updated_manifests.extend(process_cluster_manifests(cluster_manifests))
        updated_manifests.extend(
            process_control_plane_manifests(control_plane_manifests)
        )
        for group, group_manifests in grouped_manifests.items():
            updated_manifests.extend(
                process_worker_group_manifests(group, group_manifests)
            )

        assert len(manifests) == len(updated_manifests)

        return yaml.dump_all(
            updated_manifests,
            Dumper=CustomDumper,
            default_flow_style=False,
            indent=2,
        )

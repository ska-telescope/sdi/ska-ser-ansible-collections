"""
trigger_sbom_scan allows triggering SBOM generation for OCI artefacts
in Harbor.

This script:
- Retrieves projects, repositories, and artefacts from a Harbor
instance
- Identifies artefacts that have a notation signature but do not yet
have an SBOM
- Triggers SBOM generation scans on those artefacts
- Polls until the SBOM is generated or a timeout is reached
"""

import argparse
import logging
import os
import sys
import time

import requests

POLL_INTERVAL_SECONDS = 5

LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "INFO")
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("sbom-triggerer")
log.debug("Logging level is: %s", LOGGING_LEVEL)


def parse_args():
    """
    Parse command line arguments.

    Returns:
        argparse.Namespace: The parsed arguments including harbor_url,
            username, password, project, and repository list.
    """
    parser = argparse.ArgumentParser(description="Harbor SBOM Triggerer")
    parser.add_argument(
        "--harbor-url",
        required=False,
        default="https://harbor.skao.int",
        help="Harbor URL",
    )
    parser.add_argument(
        "--username", required=False, default="admin", help="Harbor username"
    )
    parser.add_argument("--password", required=True, help="Harbor password")
    parser.add_argument(
        "--project",
        required=False,
        default="production",
        help="Harbor project",
    )
    parser.add_argument(
        "--repository",
        action="append",
        help=(
            "Specify a repository name. Can be used multiple times. "
            "If not provided, all repositories in the project are processed."
        ),
    )

    return parser.parse_args()


def get_projects(api_base_url):
    """
    Retrieve the list of all projects from Harbor.

    Args:
        api_base_url (str): The base API URL for Harbor.

    Returns:
        list: A list of project dictionaries as returned by Harbor's API.

    Raises:
        requests.HTTPError: If the HTTP request fails.
    """
    url = f"{api_base_url}/projects"
    projects = []
    page = 1

    while True:
        resp = requests.get(
            url, params={"page": page, "page_size": 100}, timeout=5
        )
        resp.raise_for_status()
        data = resp.json()
        if not data:
            break

        projects.extend(data)
        page += 1

    return projects


def get_repositories(api_base_url, project):
    """
    Retrieve the list of repositories for a given project.

    Args:
        api_base_url (str): The base API URL for Harbor.
        project (str): The name of the project.

    Returns:
        list: A list of repository dictionaries.

    Raises:
        requests.HTTPError: If the HTTP request fails.
    """
    url = f"{api_base_url}/projects/{project}/repositories"
    repos = []
    page = 1

    while True:
        resp = requests.get(
            url,
            params={"sort": "creation_date", "page": page, "page_size": 100},
            timeout=5,
        )
        resp.raise_for_status()
        data = resp.json()
        if not data:
            break

        repos.extend(data)
        page += 1

    return repos


def find_accessory_type(artefact, accessory_type):
    """
    Check if a given artefact has an accessory of a specified type.
    An accessory is any artefact attached to another artefact, like
    a signature, SBOM, etc.

    Args:
        artefact (dict): The artefact dictionary from Harbor's API.
        accessory_type (str): The accessory type to look for.

    Returns:
        bool: True if the accessory type is found, False otherwise.
    """
    accessories = artefact.get("accessories", [])
    if not accessories:
        return False

    return accessory_type in [
        accessory.get("type", None) for accessory in accessories
    ]


def get_artefacts(api_base_url, project, repository):
    """
    Retrieve artefacts for a given repository in a project, filtering
    only those that have a notation signature but do not have an SBOM
    accessory.

    Args:
        api_base_url (str): The base API URL for Harbor.
        project (str): The project name.
        repository (str): The repository name.

    Returns:
        list: A list of artefact dictionaries meeting the criteria.

    Raises:
        requests.HTTPError: If the HTTP request fails.
    """
    url = f"{api_base_url}/projects/{project}/repositories\
/{repository.replace('/', '%252F')}/artifacts"
    artefacts = []
    page = 1

    while True:
        resp = requests.get(
            url,
            params={
                "sort": "-push_time",
                "with_accessory": True,
                "with_sbom_overview": True,
                "with_tag": True,
                "page": page,
                "page_size": 100,
            },
            timeout=5,
        )
        resp.raise_for_status()
        data = resp.json()
        if not data:
            break

        artefacts.extend(data)
        page += 1

    return [
        artefact
        for artefact in artefacts
        if (
            find_accessory_type(artefact, "signature.notation")
            and not find_accessory_type(artefact, "sbom.harbor")
        )
    ]


def generate_sbom(api_base_url, auth, project, repository, artefact):
    """
    Trigger SBOM generation for a single artefact

    Args:
        api_base_url (str): The base API URL for Harbor.
        auth (tuple): A tuple of (username, password) for HTTP Basic Auth.
        project (str): The project name.
        repository (str): The repository name.
        artefact (dict): The artefact dictionary.

    Raises:
        RuntimeError: If the SBOM is not generated within the allowed
        polling attempts.
    """
    if not artefact:
        return

    digest = artefact["digest"]
    trigger_url = f"{api_base_url}/projects/{project}/repositories\
/{repository.replace('/', '%252F')}/artifacts/{digest}/scan"
    resp = requests.post(
        trigger_url, json={"scan_type": "sbom"}, auth=auth, timeout=30
    )
    if resp.status_code not in (200, 202):
        resp.raise_for_status()


def main():
    """
    Parse arguments and generate SBOMs for the relevant artefacts
    """
    args = parse_args()
    harbor_url = args.harbor_url
    api_base_url = f"{harbor_url}/api/v2.0"
    auth = (args.username, args.password)
    project = args.project
    repositories = args.repository

    try:
        projects = get_projects(api_base_url)
    except requests.HTTPError as e:
        log.error("Error fetching projects: %s", e)
        sys.exit(1)

    if project not in [proj["name"] for proj in projects]:
        log.error("Project '%s' not found", project)
        sys.exit(1)

    if repositories is None or len(repositories) == 0:
        log.info("Finding repositories ...")
        try:
            repositories = [
                repo["name"].replace(f"{project}/", "")
                for repo in get_repositories(api_base_url, project)
            ]
        except requests.HTTPError as e:
            log.error(
                "Error fetching repositories for project '%s': %s", project, e
            )
            raise e

    log.info("Processing %d repositories", len(repositories))
    for repository in repositories:
        log.debug("Processing repository '%s'", repository)
        try:
            artefacts = get_artefacts(api_base_url, project, repository)
        except requests.HTTPError as e:
            log.error(
                "Error fetching artefacts for project '%s': %s", project, e
            )
            raise e

        if len(artefacts) == 0:
            log.info(
                "All artefacts in repository '%s' have an SBOM.", repository
            )
            continue

        log.info(
            "Repository '%s' has %d artefacts without SBOM. Generating ...",
            repository,
            len(artefacts),
        )
        for artefact in artefacts:
            generate_sbom(api_base_url, auth, project, repository, artefact)

        sboms_generated = False
        attempts = 0
        max_poll_attempts = max(max(len(artefacts), 6) * 2, 60)
        while attempts < max_poll_attempts:
            try:
                artefacts = get_artefacts(api_base_url, project, repository)
            except requests.HTTPError as e:
                log.error(
                    "Error fetching artefacts for project '%s': %s", project, e
                )
                raise e

            if len(artefacts) > 0:
                time.sleep(POLL_INTERVAL_SECONDS)
                attempts += 1
                log.info(
                    "Still %d SBOMs to generate. Waiting ...", len(artefacts)
                )
            else:
                sboms_generated = True
                log.info("All SBOMs generated for `%s`", repository)
                break

        if not sboms_generated:
            for artefact in artefacts:
                log.error(
                    "Error generting SBOM for %s@%s: %s",
                    repository,
                    artefact["digest"],
                    artefact.get("sbom_overview", {}).get(
                        "scan_status", "unknown"
                    ),
                )

            raise RuntimeError(f"Failed to generate SBOMs for `{repository}`")


if __name__ == "__main__":
    main()

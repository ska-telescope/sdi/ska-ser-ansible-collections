"""
Fetches multiple secret resources from HashiCorp Vault to the
local environment
"""

import base64
import logging
import os
import pathlib
from argparse import ArgumentParser

import hvac
from utils import (  # pylint: disable=import-error
    get_secret_data,
    get_vault_client,
    secure_opener,
)

LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "INFO")
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("fetch_secret_files")
log.debug("Logging level is: %s", LOGGING_LEVEL)

ALLOWED_SECRET_SUFFIXES = [
    ".pem",
    "-kubeconfig",
]


def is_allowed_secret(secret):
    """
    Checks if a secret is allowed to be pulled given its suffix
    """
    for suffix in ALLOWED_SECRET_SUFFIXES:
        if secret.endswith(suffix):
            return True

    return False


def fetch_resources(
    client: hvac.Client, resource, mount_point="ska-ser-infra-machinery"
):
    """
    Fetches all secrets data in a secret to a resource location, as files
    """
    secret = resource.split("=")[0]
    path = "=".join(resource.split("=")[1:])
    is_base64 = path.endswith(":base64")
    if is_base64:
        path = path.split(":", maxsplit=1)[0]

    if not os.path.isdir(path):
        log.info("Creating resource directory for '%s' at '%s'", secret, path)
        pathlib.Path(os.path.dirname(path)).mkdir(
            mode=0o644, parents=True, exist_ok=True
        )
    else:
        pathlib.Path(path).touch()

    log.info("Fetching '%s/%s' to '%s'", mount_point, secret, path)
    secrets = get_secret_data(client, secret, mount_point, log)
    for secret_name, secret_data in secrets.items():
        if is_allowed_secret(secret_name):
            with open(
                os.path.join(path, secret_name), "wb+", opener=secure_opener
            ) as file:
                secret_data: str
                data = secret_data.encode("utf-8")
                if is_base64:
                    data = base64.b64decode(data)

                file.write(data)


if __name__ == "__main__":
    parser = ArgumentParser(description="SKAO Vault files fetching tool")
    parser.add_argument(
        "--fetch",
        action="append",
        dest="fetch_list",
        required=True,
        help="Resources to fetch in format <secret>=<output path>",
    )
    parser.add_argument(
        "--vault",
        dest="vault",
        required=True,
        help="Vault server address",
    )
    parser.add_argument(
        "--vault-auth",
        dest="vault_auth",
        required=True,
        help="Vault auth options",
    )
    parser.add_argument(
        "--vault-renew-token",
        dest="vault_renew_token",
        default="true",
        help="Set to renew Vault auth token on usage",
    )
    parser.add_argument(
        "--mount-point",
        dest="mount_point",
        required=False,
        default="ska-ser-infra-machinery",
        help="Target Vault KV engine",
    )

    args = parser.parse_args()
    hvac_client: hvac.Client = get_vault_client(
        args.vault,
        args.vault_auth,
        args.vault_renew_token.lower() == "true",
        log,
    )
    for fetch in args.fetch_list:
        fetch_resources(
            client=hvac_client, resource=fetch, mount_point=args.mount_point
        )

#!/bin/bash

# Default backup directory
BACKUP_DIR="vault_backup_$(date +%Y%m%d%H%M%S)"

# Default ignored engines list
IGNORE_ENGINES=()
IGNORE_PATHS=()

# Default mapping list
declare -A IMPORT_MAPPING

# Default destination engine (empty means restore to original)
DEFAULT_ENGINE=""

# Engines to limit restoration and backup (empty means backup or restore all)
LIMIT_ENGINES=()
LIMIT_PATHS=()

# Engines to strip from the path (used in restore)
declare -A STRIP_ENGINES

# Function to display general help message
main_help() {
  echo "SKA Vault Backup & Restore Tool"
  echo "Usage: $0 <command> [options]"
  echo ""
  echo "Commands:"
  echo "  backup           Backup Vault KV engines"
  echo "  restore          Restore Vault KV engines"
  echo ""
  echo "Options:"
  echo "  --help           Show help for a specific command (e.g., '$0 backup --help' or '$0 restore --help')"
  echo "  --backup-dir     Specify the backup directory for both backup and restore operations"
  echo "  --strip-engine   Specify one or more engines (comma-separated) to strip from the path when restoring"
  echo "  --map            Map source paths to new target paths"
  echo ""
  echo "Run '$0 <command> --help' for more information on a command"
  exit 1
}

# Function to display usage for backup command
backup_usage() {
  echo "Usage: $0 backup [--ignore engines] [--limit engines] [--backup-dir path]"
  echo "  --ignore         Comma-separated list of KV engines to ignore during backup (e.g., --ignore engine1,engine2)"
  echo "  --limit          Comma-separated list of KV engines to include in the backup (e.g., --limit engine1,engine2). Backs up all if not specified"
  echo "  --backup-dir     Specify the backup directory to store backups (default: $BACKUP_DIR)"
  echo "  --help           Display this help message"
  exit 1
}

# Function to display usage for restore command
restore_usage() {
  echo "SKA Vault Restore Tool"
  echo "Usage: $0 restore [--engine path] [--map mappings] [--ignore engines] [--limit engines] [--strip-engine engines] [--backup-dir path]"
  echo "  --engine         Default engine path where all backups will be restored if no mapping is specified (e.g., --engine kv/default)"
  echo "  --map            Mapping of engines from source to target (e.g., --map source:target/path)"
  echo "  --ignore         Comma-separated list of KV engines to ignore (e.g., --ignore engine1,engine2)"
  echo "  --ignore-path    Comma-separated list of paths to ignore (e.g., --ignore-paths engine1/path1,engine2/paths2). Only applies if --strip-engine <engine> has been specified without --engine <new engine>"
  echo "  --limit          Comma-separated list of KV engines to restore (e.g., --limit engine1,engine2)"
  echo "  --limit-path     Comma-separated list of paths to restore (e.g., --limit-paths engine1/path1,engine2/paths2). Only applies if --strip-engine <engine> has been specified without --engine <new engine>"
  echo "  --strip-engine   Comma-separated list of engines to strip from the path when restoring (e.g., --strip-engine engine1,engine2). This will make all first level keys new engines"
  echo "  --backup-dir     Specify the backup directory to use for restoring backups (default: $BACKUP_DIR)"
  echo "  --help           Display this help message"
  exit 1
}

# Function to parse command-line arguments for backup
parse_backup_args() {
  while [[ "$#" -gt 0 ]]; do
    case "$1" in
      --ignore)
        IFS=',' read -r -a IGNORE_ENGINES <<< "$2"
        shift 2
        ;;
      --limit)
        IFS=',' read -r -a LIMIT_ENGINES <<< "$2"
        shift 2
        ;;
      --backup-dir)
        BACKUP_DIR="$2"
        shift 2
        ;;
      --help)
        backup_usage
        ;;
      *)
        echo "Unknown option: $1"
        backup_usage
        ;;
    esac
  done
}

# Function to parse command-line arguments for restore
parse_restore_args() {
  while [[ "$#" -gt 0 ]]; do
    case "$1" in
      --ignore)
        IFS=',' read -r -a IGNORE_ENGINES <<< "$2"
        shift 2
        ;;
      --ignore-path)
        IFS=',' read -r -a IGNORE_PATHS <<< "$2"
        shift 2
        ;;
      --map)
        IFS=',' read -r -a MAPPINGS <<< "$2"
        for MAPPING in "${MAPPINGS[@]}"; do
          SOURCE=$(echo "$MAPPING" | cut -d':' -f1)
          TARGET=$(echo "$MAPPING" | cut -d':' -f2)
          IMPORT_MAPPING["$SOURCE"]="$TARGET"
        done
        shift 2
        ;;
      --engine)
        DEFAULT_ENGINE="$2"
        shift 2
        ;;
      --backup-dir)
        BACKUP_DIR="$2"
        shift 2
        ;;
      --limit)
        IFS=',' read -r -a LIMIT_ENGINES <<< "$2"
        shift 2
        ;;
      --limit-path)
        IFS=',' read -r -a LIMIT_PATHS <<< "$2"
        shift 2
        ;;
      --strip-engine)
        IFS=',' read -r -a _STRIP_ENGINES <<< "$2"
        for STRIP_ENGINE in "${_STRIP_ENGINES[@]}"; do
          STRIP_ENGINES["$STRIP_ENGINE"]="$STRIP_ENGINE"
        done
        shift 2
        ;;
      --help)
        restore_usage
        ;;
      *)
        echo "Unknown option: $1"
        restore_usage
        ;;
    esac
  done
}

# Function to list all KV engines and export them
backup_vault() {
  mkdir -p "$BACKUP_DIR"

  # List all mounts and filter for KV engines
  KV_ENGINES=$(vault secrets list -detailed -format=json | jq -r 'to_entries[] | select(.value.type == "kv") | .key')

  if [[ -z "$KV_ENGINES" ]]; then
    echo "No KV engines found or access denied"
    exit 1
  fi

  # Convert IGNORE_ENGINES array to a set for easier checking
  IGNORED_SET=$(printf "%s\n" "${IGNORE_ENGINES[@]}" | tr -d '/')

  # Convert LIMIT_ENGINES array to a set for easier checking
  LIMIT_SET=$(printf "%s\n" "${LIMIT_ENGINES[@]}" | tr -d '/')

  # Loop through each KV engine and export to YAML
  for ENGINE in $KV_ENGINES; do
    ENGINE_NAME=$(echo "$ENGINE" | tr -d '/') # Strip the trailing slash

    # If a limit is set, skip any engines not in the limit
    if [[ ${#LIMIT_ENGINES[@]} -gt 0 ]]; then
      if ! echo "$LIMIT_SET" | grep -qx "$ENGINE_NAME"; then
        echo "Skipping KV engine: $ENGINE_NAME"
        continue
      fi
    fi

    # Check if the current engine should be ignored
    if echo "$IGNORED_SET" | grep -qx "$ENGINE_NAME"; then
      echo "Ignoring KV engine: $ENGINE_NAME"
      continue
    fi

    echo "Backing up KV engine: $ENGINE"

    # Export the KV engine using vkv
    vkv export -p "$ENGINE" --skip-errors --show-values --format yaml > "$BACKUP_DIR/$ENGINE_NAME.yml"
    if [[ $? -eq 0 ]]; then
      echo "Backup successful for $ENGINE. Saved to $BACKUP_DIR/$ENGINE_NAME.yml"
    else
      echo "Backup failed for $ENGINE"
    fi
  done
}

# Function to restore KV engines from the specified backup directory
restore_vault() {
  echo "Restoring KV engines from backup directory: $BACKUP_DIR"

  # Check if the specified backup directory exists
  if [[ ! -d "$BACKUP_DIR" ]]; then
    echo "Backup directory $BACKUP_DIR does not exist"
    exit 1
  fi

  # Convert LIMIT_ENGINES and IGNORE_ENGINES array to a set for easier checking
  LIMIT_SET=$(printf "%s\n" "${LIMIT_ENGINES[@]}" | tr -d '/')
  IGNORE_SET=$(printf "%s\n" "${IGNORE_ENGINES[@]}" | tr -d '/')
  LIMIT_PATH_SET=$(printf "%s\n" "${LIMIT_PATHS[@]}")
  IGNORE_PATH_SET=$(printf "%s\n" "${IGNORE_PATHS[@]}")

  # Loop through each YAML file in the backup directory and import it
  for FILE in "$BACKUP_DIR"/*.yml; do
    ENGINE_NAME=$(basename "$FILE" .yml)
    # Check if we need to limit or ignore specific engines
    if [[ ${#IGNORE_ENGINES[@]} -gt 0 ]]; then
      if echo "$IGNORE_SET" | grep -qx "$ENGINE_NAME"; then
        echo "Skipping ignored KV engine: $ENGINE_NAME"
        continue
      fi
    fi

    if [[ ${#LIMIT_ENGINES[@]} -gt 0 ]]; then
      if ! echo "$LIMIT_SET" | grep -qx "$ENGINE_NAME"; then
        echo "Skipping non-limited KV engine: $ENGINE_NAME"
        continue
      fi
    fi

    # Determine the target engine and path
    TARGET=${IMPORT_MAPPING[$ENGINE_NAME]}
    STRIP_ENGINE=${STRIP_ENGINES[$ENGINE_NAME]}

    # If no target engine is specified and we are stripping, we need to copy one by one into
    # new engines
    if [[ -z "$DEFAULT_ENGINE" && -n "$STRIP_ENGINE" ]]; then
      KEYS=$(yq eval '.["'"$ENGINE_NAME"/'"] | keys | join("\n")' $FILE)
      IFS=$'\n' read -rd '' -a NEW_ENGINES <<< "$KEYS"
      # Loop over the keys and check if any match the IMPORT_MAPPING
      for NEW_ENGINE in "${NEW_ENGINES[@]}"; do
        SOURCE_PATH="${ENGINE_NAME}/${NEW_ENGINE}"
        SOURCE_PATH=${SOURCE_PATH%/}
        TARGET=$NEW_ENGINE

        # Check if there's an import mapping for this subpath
        if [[ -n "${IMPORT_MAPPING[$SOURCE_PATH]}" ]]; then
          TARGET=${IMPORT_MAPPING[$SOURCE_PATH]}
        fi

        TARGET=${TARGET%/}
        if [[ ${#IGNORE_PATHS[@]} -gt 0 ]]; then
          if echo "$IGNORE_PATH_SET" | grep -qx "$SOURCE_PATH"; then
            echo "Skipping ignored subpath: $SOURCE_PATH"
            continue
          fi
        fi

        if [[ ${#LIMIT_PATHS[@]} -gt 0 ]]; then
          if ! echo "$LIMIT_PATH_SET" | grep -qx "$SOURCE_PATH"; then
            echo "Skipping non-limited sub-path: $SOURCE_PATH"
            continue
          fi
        fi

        yq eval '{"'"$NEW_ENGINE"'": .["'"$ENGINE_NAME"'/"]["'"$NEW_ENGINE"'"]}' $FILE | vkv import --path "$TARGET" --force --silent
      done
    # If no target engine is specified and we are not stripping, we can do a bulk copy
    else
      # If no mapping and there is a default engine, copy it with or without engine stripping
      if [[ -z "$TARGET" && -n "$DEFAULT_ENGINE" ]]; then
        if [[ -n "$STRIP_ENGINE" ]]; then
          TARGET="$DEFAULT_ENGINE"
        else
          TARGET="$DEFAULT_ENGINE/$ENGINE_NAME"
        fi
      # If no mapping is provided and no default engine, restore to the original engine
      elif [[ -z "$TARGET" ]]; then
        TARGET="$ENGINE_NAME"
      fi

      vkv import --path "$TARGET" --file "$FILE" --force --silent
    fi
  done
}

# Main script to handle subcommands
if [[ $# -lt 1 ]]; then
  main_help
fi

COMMAND=$1
shift

case $COMMAND in
  backup)
    parse_backup_args "$@"
    backup_vault
    ;;
  restore)
    parse_restore_args "$@"
    restore_vault
    ;;
  --help)
    main_help
    ;;
  *)
    echo "Unknown command: $COMMAND. Use 'backup' or 'restore'"
    main_help
    ;;
esac

"""
Utility functions used in parsing configurations and accessing HasiCrop Vault
"""

import os
import re
import sys

import hvac
from hvac.api.secrets_engines import KvV2


def secure_opener(path, flags):
    """
    Creates a file opener with user-only permissions -> 0600
    """
    return os.open(path, flags, 0o600)


def get_config(config, config_key, first=True):
    """
    Parses a config from multi config string
    """
    search = re.findall(config_key + r"[ ]*=[ ]*(\S+)", config)
    if search is None or len(search) == 0:
        return None

    return search[0] if first else search


def get_vault_token(vault_auth):
    """
    Extracts the vault token from the vault auth configuration
    """
    return get_config(vault_auth, "token")


def get_vault_client(vault_addr, vault_auth, vault_renew, log):
    """
    Gets the hvac vault client
    """
    if vault_addr is None:
        log.error("HashiCorp Vault address cannot be null")
        sys.exit(1)

    vault_auth_method = get_config(vault_auth, "auth_method")
    if vault_auth_method is None:
        log.error("HashiCorp Vault auth method cannot be null")
        sys.exit(1)

    log.info(
        "Accessing vault at '%s' using '%s' authentication",
        vault_addr,
        vault_auth_method,
    )
    client: hvac.Client = hvac.Client(vault_addr)
    if vault_auth_method == "token":
        vault_token = get_vault_token(vault_auth)
        client.token = vault_token
        if vault_renew:
            client.renew_token(token=vault_token)
    elif vault_auth_method == "jwt":
        raise NotImplementedError("HashiCorp Vault JWT not implemented")
    elif vault_auth_method == "basic":
        raise NotImplementedError(
            "HashiCorp Vault basic authentication not implemented"
        )
    else:
        log.error(
            "HashiCorp Vault auth method not supported: '%s'",
            vault_auth_method,
        )
        sys.exit(1)

    return client


def get_secret_data(
    client: hvac.Client, path, mount_point, log, ignore_failure=False
):
    """
    Gets all the key value pairs from a vault secret
    """
    try:
        api: KvV2
        api = client.secrets.kv.v2
        return (
            api.read_secret_version(path=path, mount_point=mount_point)
            .get("data", {})
            .get("data", {})
        )
    except hvac.exceptions.InvalidPath as exc:
        if not ignore_failure:
            log.error("Failed to read secret: %s", exc)

        return {}


def list_secrets(client: hvac.Client, path, mount_point, log):
    """
    Lists all the sub-paths in a vault path
    """
    try:
        api: KvV2
        api = client.secrets.kv.v2
        return (
            api.list_secrets(path=path, mount_point=mount_point)
            .get("data", {})
            .get("keys", [])
        )

    except hvac.exceptions.InvalidPath as exc:
        log.error("Failed to list secrets: %s", exc)

        return []


def merge(dict1: dict, dict2: dict):
    """
    Merge dictionaries recursively
    """
    result = dict1.copy()
    for key, value in dict2.items():
        if (
            key in result
            and isinstance(result[key], dict)
            and isinstance(value, dict)
        ):
            result[key] = merge(result[key], value)
        else:
            result[key] = value

    return result


def str_presenter(dumper, data):
    """
    Custom presenter to allow for proper formatting of multiline strings
    """
    if "\n" in data:
        return dumper.represent_scalar(
            "tag:yaml.org,2002:str", data, style="|"
        )

    return dumper.represent_scalar("tag:yaml.org,2002:str", data)

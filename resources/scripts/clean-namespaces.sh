#!/bin/bash

DECODED_INPUT=$(echo $1 | base64 -d | xargs)

for RULE in $DECODED_INPUT; do
        AGE=$(echo $RULE | sed 's#.*:\([0-9]\+\)$#\1#')
        REGEX=$(echo $RULE | sed 's#:[0-9]\+$##')
        echo "Terminating all namespaces matching '$REGEX' older than '$AGE' seconds."
        NAMESPACES=$(kubectl get namespaces --field-selector status.phase=Active -o json | jq  --arg age $AGE --arg regex "$REGEX" -r '.items[] | select ((now - (.metadata.creationTimestamp | fromdateiso8601)) > ($age|tonumber)) | select(.metadata.name | test($regex)).metadata.name')
        for NS in $NAMESPACES; do
                kubectl delete namespace $NS
        done
done

FAILING_DS_NS_AGE=${FAILING_DS_NS_AGE:-3600}
FAILING_DS_NS_REGEX=${FAILING_DS_NS_REGEX:-'^integration|^staging-|^ci-'}
echo "Terminating all failing namespaces (that is with device server in waiting or error) older than '$FAILING_DS_NS_AGE' seconds."
NAMESPACES=$(kubectl get deviceservers.tango.tango-controls.org -A -o json | jq --arg age $FAILING_DS_NS_AGE --arg regex "$FAILING_DS_NS_REGEX" -r '.items[] | select ((now - (.metadata.creationTimestamp | fromdateiso8601)) > ($age|tonumber)) | select(.status.state | startswith("Error") or startswith("Waiting")) | select (.metadata.namespace | test($regex)).metadata.namespace' | sort -u)
for NS in $NAMESPACES; do
        kubectl delete namespace $NS
done

FAILING_POD_NS_AGE=${FAILING_POD_NS_AGE:-3600}
FAILING_POD_NS_REGEX=${FAILING_POD_NS_REGEX:-'^integration|^staging-|^ci-'}
echo "Terminating all failing namespaces (that is with pod in pending phase) older than '$FAILING_POD_NS_AGE' seconds."
NAMESPACES=$(kubectl get pods --field-selector=status.phase==Pending --selector='app.kubernetes.io/managed-by notin (DeviceServerController, DatabaseDSController)' -A -o json | jq --arg age $FAILING_POD_NS_AGE --arg regex "$FAILING_POD_NS_REGEX" -r '.items[] | select ((now - (.metadata.creationTimestamp | fromdateiso8601)) > ($age|tonumber)) | select (.metadata.namespace | test($regex)).metadata.namespace' | sort -u)
for NS in $NAMESPACES; do
        kubectl delete namespace $NS
done
echo "Terminating all failing namespaces (that is with pod in failed phase) older than '$FAILING_POD_NS_AGE' seconds."
NAMESPACES=$(kubectl get pods --field-selector=status.phase==Failed --selector='app.kubernetes.io/managed-by notin (DeviceServerController, DatabaseDSController)' -A -o json | jq --arg age $FAILING_POD_NS_AGE --arg regex "$FAILING_POD_NS_REGEX" -r '.items[] | select ((now - (.metadata.creationTimestamp | fromdateiso8601)) > ($age|tonumber)) | select (.metadata.namespace | test($regex)).metadata.namespace' | sort -u)
for NS in $NAMESPACES; do
        kubectl delete namespace $NS
done

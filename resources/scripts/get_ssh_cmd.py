"""
This script builds an SSH command from the variables within the Ansible
environment to facilitate the ac-ssh make target
"""

import json
import re
import sys
from argparse import ArgumentParser


def extract_from_ansible(data, variable, default=None):
    """
    Extracts a variable from Ansible's output. Usually it contains the name
    of the ansible host, followed by the output in JSON
    """
    matches = re.findall(r"{.*}", data.replace("\n", ""), flags=re.MULTILINE)
    if len(matches) == 0 or len(matches) > 1:
        return default

    value = json.loads(matches[0]).get(variable, None)
    if "VARIABLE IS NOT DEFINED" in value:
        return default

    return value


parser = ArgumentParser(description="SSH Args builder tool")
parser.add_argument(
    "--host",
    dest="host",
    required=True,
    help="SSH host",
)
parser.add_argument(
    "--user",
    dest="user",
    required=True,
    help="SSH user",
)
parser.add_argument(
    "--default-args",
    dest="default_args",
    required=True,
    help="SSH default args",
)
parser.add_argument(
    "--ssh-args",
    dest="ssh_args",
    required=True,
    help="SSH primary args",
)
parser.add_argument(
    "--ssh-extra-args",
    dest="ssh_extra_args",
    required=True,
    help="SSH extra args",
)

args = parser.parse_args()

ssh_command = [
    "ssh",
    extract_from_ansible(
        args.ssh_args, "ansible_ssh_args", default=args.default_args
    ),
    extract_from_ansible(
        args.ssh_extra_args, "ansible_ssh_extra_args", default=""
    ),
    f"-l {extract_from_ansible(args.user, 'ansible_user', default='ubuntu')}",
    extract_from_ansible(args.host, "ansible_host", default=""),
]

print(" ".join(ssh_command), file=sys.stdout)

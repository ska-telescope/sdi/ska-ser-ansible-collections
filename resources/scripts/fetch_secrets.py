"""This script creates an ansible-vault protected secrets file, from Vault
data, in the following structure:


vault_config:
    <vault configuration for playbooks that use vault>
shared:
    <shared secrets between datacentres and environments>
<datacentre>:
    <environment>:
        <datacentre/environment specific secrets>
"""

import getpass
import logging
import os
import pathlib
import re
import sys
from argparse import ArgumentParser
from urllib.parse import urljoin

import hvac
import yaml
from ansible.parsing.vault import VaultLib, VaultSecret
from utils import (  # pylint: disable=import-error
    get_secret_data,
    get_vault_client,
    get_vault_token,
    list_secrets,
    merge,
    secure_opener,
    str_presenter,
)

LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "INFO")
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("fetch-secrets")
log.debug("Logging level is: %s", LOGGING_LEVEL)


class SafeDumperWithMultiline(yaml.SafeDumper):
    """
    Safe dumper to allow for multiline strings
    """


SafeDumperWithMultiline.add_representer(str, str_presenter)


def get_secrets(
    client: hvac.Client,
    path,
    mount_point,
    allowed_paths=None,
    ignored_paths=None,
):
    """
    Recursively gets all secrets with allowed nested paths
    """
    secrets = {}
    if path != "/":
        secrets = get_secret_data(
            client, path, mount_point, log, ignore_failure=True
        )
        ignored_keys = []
        for secret in secrets:
            full_secret_path = urljoin(path, secret)
            if ignored_paths is not None and re.match(
                ignored_paths, full_secret_path
            ):
                ignored_keys.append(secret)

        for ignored_key in ignored_keys:
            del secrets[ignored_key]

    if path.endswith("/"):
        nested_secrets = list_secrets(client, path, mount_point, log)
        for nested in nested_secrets:
            full_secret_path = urljoin(path, nested)
            if ignored_paths is not None and re.match(
                ignored_paths, full_secret_path
            ):
                continue

            if allowed_paths is not None:
                matched = False
                for expression in allowed_paths:
                    if re.match(expression, full_secret_path):
                        matched = True

                if not matched:
                    continue

            secrets = {
                **secrets,
                **{
                    nested.rstrip("/"): get_secrets(
                        client,
                        urljoin(path, nested),
                        mount_point,
                        allowed_paths,
                        ignored_paths,
                    ),
                },
            }

    return secrets


def get_secrets_from_vault(
    vault,
    vault_auth,
    vault_renew_token,
    datacentre,
    environment,
    environment_type,
    ignored_paths=None,
):
    """
    Gets secrets from vault in a structured format with shared and
    datacentre-specific secrets
    """
    client: hvac.Client = get_vault_client(
        vault, vault_auth, vault_renew_token, log
    )

    global_shared = get_secrets(
        client, "global/", "shared", None, ignored_paths
    )
    environment_shared = get_secrets(
        client, f"{environment}/", "shared", None, ignored_paths
    )

    environment_type_shared = {}
    if environment_type and environment_type != environment:
        environment_type_shared = get_secrets(
            client, f"{environment_type}/", "shared", None, ignored_paths
        )

    datacentre_shared = get_secrets(
        client, "shared/", datacentre, None, ignored_paths
    )
    datacentre_environment = get_secrets(
        client, f"{environment}/", datacentre, None, ignored_paths
    )

    return {
        datacentre: {
            environment: merge(
                merge(
                    merge(
                        merge(global_shared, environment_type_shared),
                        environment_shared,
                    ),
                    datacentre_shared,
                ),
                datacentre_environment,
            )
        }
    }


if __name__ == "__main__":
    parser = ArgumentParser(description="SKAO Vault secrets fetching tool")
    parser.add_argument(
        "--output",
        dest="output",
        required=True,
        default="secrets.yml",
        help="Output file",
    )
    parser.add_argument(
        "--datacentre",
        dest="datacentre",
        required=True,
        help="Target datacentre",
    )
    parser.add_argument(
        "--environment",
        dest="environment",
        required=True,
        help="Target environment",
    )
    parser.add_argument(
        "--environment-type",
        dest="environment_type",
        required=False,
        default=None,
        help="Target environment type. Defaults to environment",
    )
    parser.add_argument(
        "--vault",
        dest="vault",
        required=True,
        help="Vault server address",
    )
    parser.add_argument(
        "--vault-auth",
        dest="vault_auth",
        required=True,
        help="Vault auth options",
    )
    parser.add_argument(
        "--vault-renew-token",
        dest="vault_renew_token",
        default="true",
        help="Set to renew Vault auth token on usage",
    )
    parser.add_argument(
        "--mount-point",
        dest="mount_point",
        required=False,
        default="ska-ser-infra-machinery",
        help="Vau mount point",
    )
    parser.add_argument(
        "--disable-ansible-vault",
        action="store_true",
        dest="disable_ansible_vault",
        help="Set to disable outputting an ansible-vault \
            encrypted secrets file",
    )
    parser.add_argument(
        "--ask-vault-password",
        action="store_true",
        dest="ask_password",
        help="Set to ask for ansible-vault \
            encryption password",
    )
    parser.add_argument(
        "--vault-password-file",
        dest="vault_password_file",
        required=False,
        default=None,
        help="Set to use the password in the file \
            for ansible-vault encryption",
    )
    parser.add_argument(
        "--ignored-paths",
        dest="ignored_paths",
        required=False,
        default=None,
        help="Set to a regex expression to ignore paths",
    )

    args = parser.parse_args()
    # Create output directories
    output = os.path.abspath(args.output)
    pathlib.Path(os.path.dirname(output)).mkdir(parents=True, exist_ok=True)
    if args.disable_ansible_vault:
        log.warning(
            "Ansible vault output disabled. Secrets will be in plain-text"
        )
    else:
        if args.vault_password_file is not None:
            if not os.path.isfile(args.vault_password_file):
                log.error("'%s' is not a valid file", args.vault_password_file)
                sys.exit(1)

            with open(
                args.vault_password_file,
                "r",
                opener=secure_opener,
                encoding="utf-8",
            ) as password_file:
                vault_password = password_file.readline()
        else:
            vault_password = getpass.getpass(
                "Enter secrets passphrase (Vault password): "
            )

    # Output secrets
    data = yaml.dump(
        {
            "secrets": get_secrets_from_vault(
                args.vault,
                args.vault_auth,
                args.vault_renew_token.lower() == "true",
                args.datacentre,
                args.environment,
                args.environment_type,
                (
                    re.compile(args.ignored_paths)
                    if args.ignored_paths is not None
                    else None
                ),
            ),
            "vault_config": {
                "address": args.vault,
                "token": get_vault_token(args.vault_auth),
                "mount_point": args.datacentre,
            },
        },
        indent=2,
        sort_keys=True,
        Dumper=SafeDumperWithMultiline,
    )

    with open(output, "wb+", opener=secure_opener) as f:
        if args.disable_ansible_vault:
            f.write(data.encode("utf-8"))
        else:
            f.write(
                VaultLib().encrypt(
                    data,
                    secret=VaultSecret(vault_password.encode()),
                )
            )

        log.info("Secrets at %s", output)

.PHONY: check-hosts vars manual-deployment install test help velero-execute-backup velero-restore-backup
.DEFAULT_GOAL := help
ANSIBLE_PLAYBOOK_ARGUMENTS ?=
ANSIBLE_EXTRA_VARS ?=
PLAYBOOKS_DIR ?= ./ansible_collections/ska_collections
ANSIBLE_COLLECTIONS_PATHS ?=
TESTS_DIR ?= ./ansible_collections/ska_collections/k8s/tests

VAULT_CREATE_DEFAULT_ENGINE ?= false

TAGS ?= all,metallb,externaldns,ping,ingress,rookio,standardprovisioner,metrics,binderhub,nvidia,vault,secrets_store,vso,ska_tango_operator,coder,releases_notifier ## Ansible tags to run in post deployment processing

-include $(BASE_PATH)/PrivateRules.mak

check-hosts:
ifndef PLAYBOOKS_HOSTS
	$(error PLAYBOOKS_HOSTS is undefined)
endif

vars:
	@echo "\033[36mclusterapi:\033[0m"
	@echo "INVENTORY=$(INVENTORY)"
	@echo "PLAYBOOKS_HOSTS=$(PLAYBOOKS_HOSTS)"
	@echo "ANSIBLE_PLAYBOOK_ARGUMENTS=$(ANSIBLE_PLAYBOOK_ARGUMENTS)"
	@echo "ANSIBLE_EXTRA_VARS=$(ANSIBLE_EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "ANSIBLE_SSH_ARGS=$(ANSIBLE_SSH_ARGS)"

manual-deployment: check-hosts  ## Manual K8s deployment based on kubeadm
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/k8s.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"

deploy-minikube:  ## Deploy Minikube single node cluster
	ansible-playbook $(PLAYBOOKS_DIR)/minikube/playbooks/minikube.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"

deploy-singlenode:  ## Deploy singlenode single node cluster
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/singlenode.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"

install-base:  ## Install container base for Kubernetes servers
	ansible-playbook $(PLAYBOOKS_DIR)/docker_base/playbooks/containers.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"

device-integration:  ## Deployment device integration to k8s cluster
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_ROLES_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/devices.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--extra-vars "mlnx_ofed_version=23.07-0.5.0.0" \
	--extra-vars "mlnx_ofed_distro=ubuntu22.04" \
	--tags "$(TAGS)" \
	--flush-cache

destroy:

ifneq (,$(findstring vso,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/vault_secrets_operator_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring vault,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/vault_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring mysql_exporter,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/mysql_exporter_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring mariadb_operator,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/mariadb_operator_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring minio_operator,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/minio_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring minio_tenants,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/minio_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring binderhub,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/binderhub_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring coder,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/coder_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring eda,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/eda_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring dind,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/dind_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring secrets_store,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/secrets_store_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cicd_automation,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cicd_automation_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ska_ser_bar_ui,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ska_ser_bar_ui_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cicd_validations,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cicd_validations_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ska_ser_namespace_manager,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ska_ser_namespace_manager_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring velero,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/velero_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring manila_csi,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/manila_csi_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring fluxcd,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/fluxcd_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring fluxcd_config,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/fluxcd_configure_clean.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring kyverno,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/kyverno_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring headlamp,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/headlamp_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring librenms,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/librenms_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring xilinx_device_plugin,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/xilinx_device_plugin_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ska_base_image_exporter,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ska_base_image_exporter_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring sbom_visualiser,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/sbom_visualiser_destroy.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

install: check-hosts  ## Post installations for a kubernetes cluster

ifneq (,$(findstring labels,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/node_labels.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring mysql_exporter,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/mysql_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring mariadb_operator,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/mariadb_operator.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring etcd_operator,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/etcd_operator.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring stackgres_operator,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/stackgres_operator.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring minio_operator,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/minio.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring minio_tenants,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/minio.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cnpg_operator,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cnpg_operator.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif


ifneq (,$(findstring taints,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/node_taints.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cloudprovider,$(TAGS))) # If you want to run the CCM install you must explicitly add 'cloudprovider' to TAGS
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cloudprovider.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ingress,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ingress.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring standardprovisioner,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/standardprovisioner.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring metallb,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/metallb.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring externaldns,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/externaldns.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ping,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ping.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring metrics,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/metrics.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring rookio,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/rookio.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring manila_csi,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/manila_csi.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring rook_ceph_cluster,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/rook_ceph_cluster.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring binderhub,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/binderhub.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring nvidia,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/nvidia.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring taranta,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/taranta.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring eda,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/eda.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring filebeat,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/filebeat.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring vault,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/vault.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring vso,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/vault_secrets_operator.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring dind,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/dind.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring secrets_store,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/secrets_store.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cicd_validations,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cicd_validations.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cicd_automation,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cicd_automation.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ska_ser_bar_ui,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ska_ser_bar_ui.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ska_ser_namespace_manager,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ska_ser_namespace_manager.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring fluxcd,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/fluxcd.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring fluxcd_config,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/fluxcd_configure.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring kyverno,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/kyverno.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring headlamp,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/headlamp.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring headlamp_proxies,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/headlamp_proxies.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring librenms,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/librenms.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring multihoming,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/multihoming.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring spookd_device_plugin,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/spookd_device_plugin.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring generic_device_plugin,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/generic_device_plugin.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring localpvs,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/localpvs.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ska_tango_operator,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ska_tango_operator.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring ska_base_image_exporter,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/ska_base_image_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring coder,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/coder.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring releases_notifier,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/releases_notifier.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring xilinx_device_plugin,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/xilinx_device_plugin.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring coredns_config,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/coredns_config.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring velero,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/velero.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring sbom_visualiser,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/sbom_visualiser.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring gpu_burn,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/gpu_burn.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--extra-vars "k8s_gpu_burn_confluence_token=$(CONFLUENCE_TOKEN)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cilium_cli,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cilium_cli.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cilium_pre_migration,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cilium_pre_migration.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cilium_migration,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cilium_migration.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--extra-vars "drain_node=$(DRAIN_NODE)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cilium_post_migration,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cilium_post_migration.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring cilium_install,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/cilium_install.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

test: check-hosts  # Test service deployments

ifneq (,$(findstring ingress,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-ingress.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring metrics,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-metrics.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring standardprovisioner,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-storageprovisioner.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring metallb,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-metallb.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring nvidia,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-nvidia.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring ping,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-ping.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring vault,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-vault.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring vso,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-vault-operator.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring rookio,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-rookio.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring ska_tango_operator,$(TAGS)))
	@ansible-playbook $(TESTS_DIR)/test-ska-tango-operator.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"
endif

ifneq (,$(findstring dind,$(TAGS)))
	ansible-playbook $(TESTS_DIR)/test-dind.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring kyverno,$(TAGS)))
	ansible-playbook $(TESTS_DIR)/test-kyverno.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

vault-add-teams:
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/vault_configure_teams.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--extra-vars "gitlab_access_token=$(GITLAB_ACCESS_TOKEN)"

vault-update-team:
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/vault_update_teams_config.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--extra-vars "gitlab_access_token=$(GITLAB_ACCESS_TOKEN)" \
	--extra-vars "k8s_vault_group_name=$(VAULT_TEAM_NAME)"

vault-register-external-cluster:
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/vault_register_external_cluster.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--extra-vars "k8s_vault_login_token=$(VAULT_TOKEN)" \
	--extra-vars "k8s_vault_kv_create_default=$(VAULT_CREATE_DEFAULT_ENGINE)" \
	--extra-vars "k8s_vault_external_registration_enabled=false" \
	--extra-vars "k8s_vault_external_registration_b64data=$(VAULT_EXTERNAL_REGISTRATION)"

create-client-kubeconfigs:
	ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/client_kubeconfigs.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "client_kubeconfigs"

VELERO_SCHEDULE_NAME ?= ## velero schedule name to run or restore
VELERO_BACKUP_NAME ?= ## velero backup to restore

velero-execute-backup:
	@if [ -z "$(VELERO_SCHEDULE_NAME)" ]; then \
		echo "ERROR: You must provide 'VELERO_SCHEDULE_NAME'."; \
		exit 1; \
	fi
	@echo "Running backup from schedule '$(VELERO_SCHEDULE_NAME)'"
	@ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/velero-run-schedule.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--extra-vars "k8s_velero_run_schedule=$(VELERO_SCHEDULE_NAME)" \
	--tags "$(TAGS)"

velero-restore-backup:
	@if [ -z "$(VELERO_SCHEDULE_NAME)" ] || [ -z "$(VELERO_BACKUP_NAME)" ]; then \
		echo "ERROR: You must provide 'VELERO_SCHEDULE_NAME' and 'VELERO_BACKUP_NAME'."; \
		exit 1; \
	fi
	@echo "Restoring backup '$(VELERO_BACKUP_NAME)' from schedule '$(VELERO_SCHEDULE_NAME)'"
	@ansible-playbook $(PLAYBOOKS_DIR)/k8s/playbooks/velero-restore-backup.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--extra-vars "k8s_velero_restore_backup_name=$(VELERO_BACKUP_NAME)" \
	--extra-var "k8s_velero_restore_schedule=$(VELERO_SCHEDULE_NAME)" \
	--tags "$(TAGS)"

MINIO_TENANT_K8S_NAMESPACE ?= gitlab
MINIO_OPERATOR_CONSOLE_PORT ?= 9090
MINIO_TENANT_CONSOLE_PORT ?= 9091
MINIO_LOCAL_DOCKER ?= 127.0.0.1

minio_logs: ## Show minio logs
	@for i in `kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) get pods -l app=minio -o=name`; \
	do \
	echo "---------------------------------------------------"; \
	echo "Logs for $${i}"; \
	echo kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) logs $${i}; \
	echo kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"`; do \
	RES=`kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "initContainer: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	echo "Main Pod logs for $${i}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) get $${i} -o jsonpath="{.spec.containers[*].name}"`; do \
	RES=`kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "Container: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	done

list_cache:  ## list the minio cache using in-cluster connection
	@kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) run mc --rm -ti --image=minio/mc --restart=Never --command -- \
	/bin/sh -c "mc alias set cache http://minio $(MINIO_ACCESS_KEY) $(MINIO_SECRET_KEY); mc ls --recursive cache/"

list_cache_remote: ## list the global minio cache using remote connection
	docker run --rm -it --entrypoint=/bin/sh minio/mc -c 'mc alias set cache https://k8s.stfc.skao.int:9443 $(MINIO_ACCESS_KEY) $(MINIO_SECRET_KEY); mc ls --recursive cache/'

get_cache:  ## retrieve the minio cache
	sudo rm -rf $$(pwd)/tmp
	mkdir -p $$(pwd)/tmp
	kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) port-forward service/minio 9002:80 &
	sleep 3
	docker run --rm -it --net=host \
	-e HOME=$${HOME} \
	-v /etc/passwd:/etc/passwd:ro --user=$$(id -u) \
	-v $${HOME}:$${HOME} -w $${HOME} \
	--volume $$(pwd)/tmp:/mnt \
	--entrypoint=/bin/sh minio/mc -c \
	'mc alias set cache http://$(MINIO_LOCAL_DOCKER):9002 $(MINIO_ACCESS_KEY) $(MINIO_SECRET_KEY); mc ls --recursive cache; mc cp --recursive cache/$(MINIO_BUCKET_NAME) /mnt/'
	ls -lR $$(pwd)/tmp
	ps axf | grep 'port-forward service' | grep -v grep | awk '{print $$1}' | xargs kill

get_key: ## get keys from minio environmen variables
	kubectl exec -n $(MINIO_TENANT_K8S_NAMESPACE) -it gitlab-minio-tenant-pool-0 -- printenv | grep KEY

get_operator_jwt: ## get jwt token for operator console
	kubectl -n $(MINIO_OPERATOR_K8S_NAMESPACE) get secret console-sa-secret -o jsonpath="{.data.token}" | base64 --decode

forward_operator_console: ## port forward operator console on 9090
	kubectl -n $(MINIO_OPERATOR_K8S_NAMESPACE) port-forward svc/console $(MINIO_OPERATOR_CONSOLE_PORT):9090

forward_tenant_console: ## port forward tenant console on 9091
	kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) port-forward svc/minio-console $(MINIO_TENANT_CONSOLE_PORT):9090

# https://docs.min.io/minio/baremetal/reference/minio-cli/minio-mc/mc-rm.html#mc-rm-older-than
prune_cache:  ## prune the minio cache
	@kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) run mc --rm -ti --image=minio/mc --restart=Never --command -- \
	/bin/sh -c "mc alias set cache http://minio $(MINIO_ACCESS_KEY) $(MINIO_SECRET_KEY); mc rm --older-than 15d --recursive --dangerous --force cache/"

delete_ilm:
	@kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) run mc --rm -ti --image=minio/mc --restart=Never --command -- \
	/bin/sh -c "mc alias set cache http://minio $(MINIO_ACCESS_KEY) $(MINIO_SECRET_KEY); mc ilm rule rm --id $(MINIO_ILM_LIFECYCLE_ID) cache/cache"

add_ilm:
	@kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) run mc --rm -ti --image=minio/mc --restart=Never --command -- \
	/bin/sh -c "mc alias set cache http://minio $(MINIO_ACCESS_KEY) $(MINIO_SECRET_KEY); mc ilm rule add --expire-days 30 cache/cache"

list_ilms:
	@kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) run mc --rm -ti --image=minio/mc --restart=Never --command -- \
	/bin/sh -c "mc alias set cache http://minio $(MINIO_ACCESS_KEY) $(MINIO_SECRET_KEY); mc ilm rule ls cache/cache"

create_cache_bucket: ## create the minio bucket named cache for gitlab manually
	@kubectl -n $(MINIO_TENANT_K8S_NAMESPACE) run mc --rm -ti --image=minio/mc --restart=Never --command -- \
	/bin/sh -c "mc alias set cache http://minio $(MINIO_ACCESS_KEY) $(MINIO_SECRET_KEY); \
	mc mb --ignore-existing cache/$(MINIO_BUCKET_NAME)"

help:  ## Show Help
	@echo "K8s targets - make playbooks k8s <target>:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

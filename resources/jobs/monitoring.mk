.DEFAULT_GOAL := help
ANSIBLE_PLAYBOOK_ARGUMENTS ?=
INVENTORY ?= $(PLAYBOOKS_ROOT_DIR)
PLAYBOOKS_DIR ?= ./ansible_collections/ska_collections/monitoring/playbooks
TESTS_DIR ?= ./ansible_collections/ska_collections/monitoring/tests
ANSIBLE_EXTRA_VARS ?=
PROMETHEUS_FILES_DIR ?= ./ansible_collections/ska_collections/monitoring/roles/prometheus/files
PROMETHEUS_HELPER_DIR ?= $(PROMETHEUS_FILES_DIR)/helper

## EXECUTION VARIABLES
NODES ?= all
COLLECTIONS_PATHS ?= ./collections
PROM_CONFIGS_PATH ?= .

SNMP_DELL_IDRAC_AUTH_COMMUNITY ?= "" ## mandatory field for configuring the authentication of idrac snmp exporter
SNMP_UPS_AUTH_COMMUNITY ?= "" ## mandatory field for configuring the authentication of ups snmp exporter

-include $(BASE_PATH)/PrivateRules.mak

check_hosts:
ifndef PLAYBOOKS_HOSTS
	$(error PLAYBOOKS_HOSTS is undefined)
endif

vars:  ## Variables
	@echo "\033[36mMonitoring:\033[0m"
	@echo "INVENTORY=$(INVENTORY)"
	@echo "PLAYBOOKS_HOSTS=$(PLAYBOOKS_HOSTS)"
	@echo "NODES=$(NODES)"
	@echo "SNMP_DELL_IDRAC_AUTH_COMMUNITY=$(SNMP_DELL_IDRAC_AUTH_COMMUNITY)"
	@echo "SNMP_UPS_AUTH_COMMUNITY=$(SNMP_UPS_AUTH_COMMUNITY)"

prometheus: check_hosts ## Install Prometheus Server
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_prometheus.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'"

grafana: check_hosts ## Install Grafana Server
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_grafana.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'"

alertmanager: check_hosts ## Install Prometheus Server
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_alertmanager.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'"

thanos_query: check_hosts ## Install Thanos query and query front-end
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_thanos.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)' mode='thanos_query'"

thanos_store: check_hosts ## Install Thanos store
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_thanos.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)' mode='thanos_store'"

thanos_compactor: check_hosts ## Install Thanos compactor
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_thanos.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)' mode='thanos_compactor'"

thanos_receive: check_hosts ## Install Thanos receive
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_thanos.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)' mode='thanos_receive'"

thanos_sidecar: check_hosts ## Install Thanos compactor
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_thanos.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)' mode='thanos_sidecar'"

thanos_ruler: check_hosts ## Install Thanos ruler
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_thanos.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)' mode='thanos_ruler'"

node-exporter: check_hosts ## Install Prometheus node exporter
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_node_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	-e "target_hosts='$(PLAYBOOKS_HOSTS)'"

generate-targets: check_hosts ## Update json file for prometheus targets definition
	@ansible-playbook $(PLAYBOOKS_DIR)/generate_targets.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	-e "target_hosts='$(PLAYBOOKS_HOSTS)'"

gitlab-ci-pipelines-exporter: check_hosts ## Install Prometheus gitlab ci pipelines exporter - pass INVENTORY and NODES
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_gitlab_ci_pipelines_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
	--limit $(NODES)


ironic-exporter: check_hosts ## Install Prometheus ironic exporter - pass INVENTORY and NODES
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_ironic_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
	--limit $(NODES)

postgres-exporter: check_hosts ## Install Prometheus postgres exporter - pass INVENTORY and NODES
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_postgres_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
	--limit $(NODES)

sql-exporter: check_hosts ## Install Prometheus SQL exporter - pass INVENTORY and NODES
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_sql_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
	--limit $(NODES)

mariadb-sql-exporter: check_hosts ## Install Prometheus MariaDB SQL exporter - pass INVENTORY and NODES
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_mariadb_sql_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
	--limit $(NODES)

slurm-exporter: check_hosts ## Install Prometheus SLURM exporter
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_slurm_exporter.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	-e "target_hosts='$(PLAYBOOKS_HOSTS)'"

snmp_exporter: check_hosts ## Install snmp_exporter
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_snmp_exporter.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
		-e 'ansible_python_interpreter=/usr/bin/python3'

snmp_install: check_hosts ## Install snmp_install
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_snmp.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
		-e 'ansible_python_interpreter=/usr/bin/python3'

snmp_destroy: check_hosts ## Destroy snmp services
	@ansible-playbook $(PLAYBOOKS_DIR)/destroy_snmp.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
		-e 'ansible_python_interpreter=/usr/bin/python3'

apt-exporter: check_hosts ## Install snmp_install
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_apt_exporter.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
		-e 'ansible_python_interpreter=/usr/bin/python3'

apt-exporter-destroy: check_hosts ## Destroy snmp services
	@ansible-playbook $(PLAYBOOKS_DIR)/destroy_apt_exporter.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
		-e 'ansible_python_interpreter=/usr/bin/python3'

k8s-compute-exporter: check_hosts ## Install k8s_compute_exporter
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_k8s_compute_exporter.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
		-e 'ansible_python_interpreter=/usr/bin/python3'

k8s-gpu-exporter: check_hosts ## Install k8s_gpu_exporter
	@ansible-playbook $(PLAYBOOKS_DIR)/deploy_k8s_gpu_exporter.yml \
		-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
		-e "target_hosts='$(PLAYBOOKS_HOSTS)'" \
		-e 'ansible_python_interpreter=/usr/bin/python3'

test-prometheus: check_hosts ## Test prometheus
	@ansible-playbook $(TESTS_DIR)/prometheus_test.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"

test-thanos: check_hosts ## Test thanos
	@ansible-playbook $(TESTS_DIR)/thanos_test.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"

help: ## Show Help
	@echo "Monitoring targets - make playbooks monitoring <target>:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

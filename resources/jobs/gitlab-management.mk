.PHONY: help
.DEFAULT_GOAL := help
ANSIBLE_PLAYBOOK_ARGUMENTS ?=
ANSIBLE_EXTRA_VARS ?=
INVENTORY ?= $(PLAYBOOKS_ROOT_DIR)
PLAYBOOKS_DIR ?= ./ansible_collections/ska_collections/gitlab_management/playbooks
#Default to SKAO Group
GITLAB_NAMESPACE_ID ?= 3180705
GITHUB_ADMIN_HANDLE ?= "marvin-skao"

-include $(BASE_PATH)/PrivateRules.mak

check_inputs_user:
ifndef GITLAB_HANDLE
	$(error GITLAB_HANDLE is undefined)
endif
ifndef GITLAB_ACCESS_TOKEN
	$(error GITLAB_ACCESS_TOKEN is undefined)
endif

check_inputs_repo:
ifndef GITLAB_REPO_NAME
	$(error GITLAB_REPO_NAME is undefined)
endif
ifndef GITLAB_ACCESS_TOKEN
	$(error GITLAB_ACCESS_TOKEN is undefined)
endif
ifndef JIRA_ISSUE_ID
	$(error JIRA_ISSUE_ID is undefined)
endif
ifndef GITLAB_REPO_MAINTAINERS
	$(error GITLAB_REPO_MAINTAINERS is undefined)
endif
ifndef GITHUB_MIRROR_TOKEN
	$(error GITHUB_MIRROR_TOKEN is undefined)
endif

.validate_repo_name:
#If it's an SRC repo, set the subgroup ID and allow name to start with src-
	@if [ "true" == "$(SRC_REPO)" ]; then \
		if [ $$(echo $(GITLAB_REPO_NAME) |  awk -e '$$1 ~ /^(ska-|src-)[a-z]+[-a-z0-9]*[a-z0-9]$$/ {print $$0}') ]; then\
			GITLAB_NAMESPACE_ID = 10957126 ;\
			exit 0;\
		fi; \
	else \
		if [ $$(echo $(GITLAB_REPO_NAME) |  awk -e '$$1 ~ /^(ska-)[a-z]+[-a-z0-9]*[a-z0-9]$$/ {print $$0}') ]; then\
			exit 0;\
		fi; \
	fi; \
	echo "❌❌❌ Repo's name is not according to the naming conventions -> Exiting now! ❌❌❌";\
	echo "";\
	exit 1
	
	
.validate_src_repo_name:

add_gitlab_user: check_inputs_user ## Add user to SKAO default groups
	ansible-playbook $(PLAYBOOKS_DIR)/add_user.yml \
	$(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "gitlab_handle=$(GITLAB_HANDLE)" \
	--extra-vars "gitlab_access_token=$(GITLAB_ACCESS_TOKEN)"

remove_gitlab_user: check_inputs_user ## Add user to SKAO default groups
	ansible-playbook $(PLAYBOOKS_DIR)/remove_user.yml \
	$(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "gitlab_handle=$(GITLAB_HANDLE)" \
	--extra-vars "gitlab_access_token=$(GITLAB_ACCESS_TOKEN)"

add_gitlab_repo: check_inputs_repo .validate_repo_name ## Add repo to SKAO group and mirror it to Github
	ansible-playbook $(PLAYBOOKS_DIR)/create_repo.yml \
	$(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "gitlab_repo_name=$(GITLAB_REPO_NAME)" \
	--extra-vars "gitlab_namespace_id=$(GITLAB_NAMESPACE_ID)" \
	--extra-vars "gitlab_access_token=$(GITLAB_ACCESS_TOKEN)" \
	--extra-vars "jira_issue_id=$(JIRA_ISSUE_ID)" \
	--extra-vars "gitlab_repo_maintainers=$(GITLAB_REPO_MAINTAINERS)" \
	--extra-vars "github_admin_handle=$(GITHUB_ADMIN_HANDLE)" \
	--extra-vars "github_mirror_token=$(GITHUB_MIRROR_TOKEN)"


help: ## Show Help
	@echo "Gitlab_management targets - make playbooks gitlab_management <target>:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

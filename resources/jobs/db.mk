.PHONY: check-hosts vars manual-deployment launch test velero-backups-install help
.DEFAULT_GOAL := help
ANSIBLE_PLAYBOOK_ARGUMENTS ?=
ANSIBLE_EXTRA_VARS ?=
PLAYBOOKS_DIR ?= ./ansible_collections/ska_collections
ANSIBLE_COLLECTIONS_PATHS ?=
TESTS_DIR ?= ./ansible_collections/ska_collections/db/tests

TAGS ?= none ## Ansible tags to run in post deployment processing

-include $(BASE_PATH)/PrivateRules.mak

check-hosts:
ifndef PLAYBOOKS_HOSTS
	$(error PLAYBOOKS_HOSTS is undefined)
endif

vars:
	@echo "\033[36mclusterapi:\033[0m"
	@echo "INVENTORY=$(INVENTORY)"
	@echo "PLAYBOOKS_HOSTS=$(PLAYBOOKS_HOSTS)"
	@echo "ANSIBLE_PLAYBOOK_ARGUMENTS=$(ANSIBLE_PLAYBOOK_ARGUMENTS)"
	@echo "ANSIBLE_EXTRA_VARS=$(ANSIBLE_EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "ANSIBLE_SSH_ARGS=$(ANSIBLE_SSH_ARGS)"

launch:
ifneq (,$(findstring mariadb_mkmon,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/db/playbooks/mariadb_mkmon.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring mariadb_singlebackup,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/db/playbooks/mariadb_singlebackup.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring mariadb_s3backup,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/db/playbooks/mariadb_s3backup.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

ifneq (,$(findstring mariadb_s3cronbackup,$(TAGS)))
	ansible-playbook $(PLAYBOOKS_DIR)/db/playbooks/mariadb_s3cronbackup.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	--tags "$(TAGS)"
endif

help:  ## Show Help
	@echo "DB targets - make playbooks db <target>:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

none:
	@echo "Please define a TAG"

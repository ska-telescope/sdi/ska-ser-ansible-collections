.PHONY: check_hosts check_gitlab_runner_secrets vars install destroy help
.DEFAULT_GOAL := help
ANSIBLE_PLAYBOOK_ARGUMENTS ?=
ANSIBLE_EXTRA_VARS ?=
INVENTORY ?= $(PLAYBOOKS_ROOT_DIR)
PLAYBOOKS_DIR ?= ./ansible_collections/ska_collections/gitlab_runner/playbooks
TESTS_DIR ?= ./ansible_collections/ska_collections/gitlab_runner/tests
V ?= ## ansible-playbook debug options, i.e. -vvv
GITLAB_RUNNER_K8S_NAMESPACE ?= gitlab

-include $(BASE_PATH)/PrivateRules.mak


check_hosts:
ifndef PLAYBOOKS_HOSTS
	$(error PLAYBOOKS_HOSTS is undefined)
endif

vars:
	@echo "\033[36mGitlab_runner:\033[0m"
	@echo "INVENTORY=$(INVENTORY)"
	@echo "PLAYBOOKS_HOSTS=$(PLAYBOOKS_HOSTS)"

deploy_docker_runner: check_hosts ## Deploy gitlab_runner
	ansible-playbook $(PLAYBOOKS_DIR)/install_runner_docker_executor.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"

destroy_docker_runner: check_hosts ## Destroy gitlab_runner
	ansible-playbook $(PLAYBOOKS_DIR)/destroy_runner_docker_executor.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"

tidy:  ## Clean up patch files
	@rm -rf ./runner_kustomize/cache-secret.yaml \
	./playbooks/files/minio_tenant.yml

label_nodes:  ## Label worker nodes for CI
	ansible-playbook $(PLAYBOOKS_DIR)/label_nodes.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	  $(V)

deploy_k8s_runner: tidy ## Deploy runners
	ansible-playbook $(PLAYBOOKS_DIR)/install_runner_k8s_executor.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	$(GITLAB_RUNNER_TAG_LIST_ARG)

destroy_k8s_runner: tidy  ## Destroy runners
	ansible-playbook $(PLAYBOOKS_DIR)/destroy_runner_k8s_executor.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
	$(GITLAB_RUNNER_TAG_LIST_ARG)

test_k8s_runner:
	ansible-playbook $(TESTS_DIR)/test_k8s_runner.yml \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"

runner_logs: ## Show runner logs
	@for i in `kubectl -n $(GITLAB_RUNNER_K8S_NAMESPACE) get pods -l release=$(GITLAB_RUNNER_K8S_RELEASE_NAME) -o=name`; \
	do \
	echo "---------------------------------------------------"; \
	echo "Logs for $${i}"; \
	echo kubectl -n $(GITLAB_RUNNER_K8S_NAMESPACE) logs $${i}; \
	echo kubectl -n $(GITLAB_RUNNER_K8S_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(GITLAB_RUNNER_K8S_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"`; do \
	RES=`kubectl -n $(GITLAB_RUNNER_K8S_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "initContainer: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	echo "Main Pod logs for $${i}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(GITLAB_RUNNER_K8S_NAMESPACE) get $${i} -o jsonpath="{.spec.containers[*].name}"`; do \
	RES=`kubectl -n $(GITLAB_RUNNER_K8S_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "Container: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	echo "config.toml for: $${j}"; \
	rm -f /tmp/config.toml; \
	kubectl -n $(GITLAB_RUNNER_K8S_NAMESPACE) cp $$(echo "$${i}" | cut -d/ -f 2):/home/gitlab-runner/.gitlab-runner /tmp/; \
	echo "---------------------------------------------------";\
	cat /tmp/config.toml; \
	done; \
	done

help: ## Show Help
	@echo "Gitlab_runner targets - make playbooks gitlab_runner <target>:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# SKA Logging Collection

This collection includes a variety of Ansible roles to help automate the installation and configuration of [Elasticsearch Stack](https://www.elastic.co/elastic-stack/).
This collection is currently maintained by [SKAO](https://www.skao.int/).

## Ansible

Tested with the current Ansible 6.5.x releases.

## Ansible Roles
| Name | Description | Version | OS Requirements | Dependencies |
| ---- | ----------- | ------- | --- | ---|
| [logging.stack](./logging/roles/stack) | Install Elasticsearch cluster, Kibana and HA | 8.4.2 | Ubuntu 18+ (LTS) | [common.certs](./common/roles/certs) |
| [logging.haproxy](./logging/roles/haproxy) | Install and configure SSL certificates | 2.6 | Ubuntu 18+ (LTS) | |
| [logging.beats](./logging/roles/beats) | Install and configure Filebeat | 7.17.0 | Ubuntu 18+ (LTS) | |
| [k8s.beats](../k8s/roles/filebeat) | Install and configure Filebeat as a Daemonset for k8s cluster| 8.11.4 | 1.25.5+ |
| [k8s.filebeat](../k8s/roles/filebeat) | Install and configure Filebeat as a Daemonset| 8.11.4 | Kubernetes 1.25.5+ | |
| [logging.custom_metrics](./logging/roles/custom_metrics) | Install and configure some custom metrics for prometheus monitoring of Filebeat | 0.1.0 | Ubuntu 18+ (LTS) | |

## Installation



Before using the collection, you need to install the collection with the `ansible-galaxy` CLI:

    ansible-galaxy collection install ska_collections.logging

You can also include it in a `requirements.yml` file and install it via ansible-galaxy collection install -r requirements.yml` using the format:

```yaml
collections:
- name: ska_collections.logging
```

## Usage

Playbooks can be found in the [playbooks/](./playbooks) folder in the following files:

| Name | Description |
| ---- | ----------- |
| [install.yml](./playbooks/install.yml) | Install Elasticsearch cluster, Kibana and HA  |
| [destroy.yml](./playbooks/destroy.yml) | Destroys Elastic Stack |
| [destroy-logging.yml](.playbooks/destroy-logging.yml) | Destroys Filebeat |
| [list-api-keys.yml](.playbooks/list-api-keys.yml) | Lists Elasticsearch API keys |
| [logging.yml](.playbooks/logging.yml) | Installs Filebeat|
| [update-api-keys.yml](.playbooks/logging.yml) | Updates the API keys |

In order to run these playbooks, it's needed to specify the Ansible Inventory location and the respective group/hosts ***target_hosts*** variable.

Run **install** playbook as an example:
```
ansible-playbook <playbooks-folder-path>/install.yml \
	-i $(INVENTORY) \
	--extra-vars "target_hosts=<target-hosts>"
```

### Prior to each new/upgraded filebeat deployment

Each time filebeat is installed or upgraded, the configuration in Elasticsearch must be updated.  This includes setting/correcting the index template, ILM policy, and the Datastream configuration.  To do this, run the following:
```
podman run --rm -ti \
       --name filebeat_setup \
       -v $(pwd)/filebeat.yaml:/etc/filebeat.yaml:ro \
       -v $(pwd)/filebeat-ilm-policy.json:/etc/filebeat-ilm-policy.json:ro \
       -v $(pwd)/ca-certificate.crt:/etc/ca-certificate.crt:ro \
       -v $(pwd)/filebeat.pem:/etc/filebeat.crt:ro \
       -v $(pwd)/filebeat.key:/etc/filebeat.key:ro \
       docker.elastic.co/beats/filebeat:8.4.3 \
       setup --index-management -e -strict.perms=false -c /etc/filebeat.yaml
```

Where files:
 * filebeat.yaml
 * filebeat-ilm-policy.json
 * ca-certificate.crt
 * filebeat.pem
 * filebeat.key

Are the correctly configured files for the runtime deployment of filebeat.

### Required variables

| Name | Ansible variable | Obs |
| ---- | ----------- | ----- |
| Elasticsearch cluster DNS Name | elasticsearch_dns_name | Same dns name used on the certificates |
| Target Elasticsearch Cluster address | logging_filebeat_elasticsearch_address | Domain or ip of the target elasticsearch cluster (preferably, the loadbalancer) |

### Required secrets

| Name | Ansible variable | ENV variable | Obs |
| ---- | ----------- | ------------ | ----- |
| Certificate Authority Password | ca_cert_password | CA_CERT_PASSWORD | |
| Elasticsearch Admin Password | elasticsearch_password | ELASTICSEARCH_PASSWORD | |
| Kibana User Password | kibana_viewer_password | KIBANA_VIEWER_PASSWORD | |
| Elasticsearch HA Stats password | elastic_haproxy_stats_password | ELASTIC_HAPROXY_STATS_PASSWORD | |
| Filebeat authentication password | logging_filebeat_elasticsearch_password | LOGGING_FILEBEAT_API_KEY | logging_filebeat_elasticsearch_auth_method: 'basic' -> Plain password <br><br> logging_filebeat_elasticsearch_auth_method: 'api-key' -> base64-decoded issued by `elasticsearch_api_keys`|

### Client Authentication

By default, clients are required to present to the server valid certificates signed by a private CA (mentioned previously as ca-certificate.crt). To ease the configuration of clients, we can configure elasticsearch to consider this PKI **optional**. To do so, we need to set:

```
elasticsearch_http_authentication: "optional"
```


This option should be used for user accounts or user facing tools. Services still should follow mTLS. As we are using a private CA, the clients - including **filebeat** - need to trust the server TLS certificate. Although it's advised against it, to simplify this procedure, we can set:


## How to Contribute

### Adding a new role
A new role can be added to the [roles](./roles/) folder and then included into a new and/or existing playbook.

### Updating an existing role
The existing roles can be found in the [roles](./roles/) folder. To update a role, the role's tasks can be simply modified.

### External dependencies
Go to [requirements.yml](../../../requirements.yml) and [galaxy.yml](./galaxy.yml) files to add or update any external dependency.

### Add/Update new variables
Ansible variables that are datacentre specific should be added to the `group_vars` folder of the inventory.

To modify non-secret variable role defaults, go to the defaults folder of the respective role and update them. As an [example](./roles/stack/defaults/main.yml).

Finally, the secret variables are defined in the respective [Makefile](../../../resources/jobs/logging.mk) and can be modified there. To assign proper values to these variables, please use a `PrivateRules.mak` file.

## More information

- [Ansible Using collections](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html)
- [Ansible Collection overview](https://github.com/ansible-collections/overview)
- [Ansible User guide](https://docs.ansible.com/ansible/latest/user_guide/index.html)
- [Ansible Developer guide](https://docs.ansible.com/ansible/latest/dev_guide/index.html)
- [Ansible Community code of conduct](https://docs.ansible.com/ansible/latest/community/code_of_conduct.html)

## License

BSD-3.
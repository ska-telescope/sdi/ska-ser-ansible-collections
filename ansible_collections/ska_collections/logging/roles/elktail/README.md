# Elktail

Elktail is a command line utility to query and tail Elasticsearch logs.

This version of Elktail comes from here https://gitlab.com/piersharding/elktail/

Run ``elktail -h`` to learn how to run elktail and the readme in the link above.

# Elktail Checker

The elktail checker is a shell script that queries the cluster associated elasticsearch using Elktail. It queries for logs in the configured namepace and checks if there is logs for all the Kubernetes nodes of the cluster in a defined date range. It will generate metrics for each node stating if has logs or not.

This script will run as a cron job.

By default: 

- elktail will be installed in ``/usr/local/bin/elktail``

- the cron job will run every 10 min.

- the configuration file path will be ``/home/ubuntu/.elktail/default.json``.

- the script file path will be ``/home/ubuntu/elktail/elktail_check.sh``.

- logs for the script can be seen in `` /var/log/elktail_check.log``.

The cron job needs to run with root because we need to move the metrics generated to the node-exporter path.
The files/paths are owned by the non sudo user for more easy debuging.

# Debugin
The app can be run easily to repoduce the same behavior has the cron job. Just run:

```bash
cd elktail
sudo ./elktail_check.sh
```

# Pre requirements

- Kubectl should be installed and connected to the cluster.

- A token must be generated, with the proper permissions, so elktail can connect to elasticsearch and should be placed in the **elktail_search_target_api_key** variable. The token can be generated in kibana or iwith a directly via the api:

```bash
# ex how to create a token (may need more permissions).
sudo curl -u USER --cacert PATH_TO_CERTIFICATE -X POST "https://localhost:9200/_security/api_key" -H "Content-Type: application/json" -d '{
  "name": "elktail-token",
  "role_descriptors": {
    "elktail_role": {
      "cluster": ["monitor", "manage_own_api_key"],
      "index": [
        {
          "names": ["*"],
          "privileges": ["read", "view_index_metadata"]
        }
      ]
    }
  }
}'

```
|Variable|Description|
|---|---|
|elktail_project_api_url| project where the release is|
|elktail_release_tag| elktail release tag|
|elktail_initial_entries| default retrived entries|
|elktail_search_target_insecure| true if no authentication is needed to connect to elastic|
|elktail_search_target_api_key| token generated in elasticsearch for elktail to use|
|elktail_search_target_index_pattern| index patern to tail logs|
|elktail_search_target_url| elasticsearch url (ex: https://localhost:9200)|
|elktail_user|user to run elktail|
|elktail_config_path| path to store elktail default configuration|
|elktail_check_path|path to store the elktail check script|
|elktail_check_namespace| namespace to look in elktail check script|
|elktail_check_time_interval_minutes| interval in minutes, that apply to the query range and cron job interval|
|elktail_check_command| elktail querie in elktail check script|
|elktail_check_command_fiter| elktail querie filter in elktail check script|
|elktail_check_cron_minutes| cron job frequency in minutes and date range for the elktail query|
|elktail_node_exporter_metrics_path| node-exporter metris path|
|elktail_node_exporter_metrics_file| elktail metrics file|

## Install

There is playbooks will install/remove Elktail and the a cron job to run the Elktail Checker.
If you want to only install/remove one of roles please add ``TAGS=elktail`` or ``TAGS=elktail_check``


```bash
make playbooks logging install-elktail PLAYBOOKS_HOSTS=HOST_SERVER

```

## Destroy

There is playbooks to destroy Elktail and the Elktail Checker cron job

```bash
make playbooks logging destroy-elktail PLAYBOOKS_HOSTS=HOST_SERVER

```

#!/bin/bash

# Retrieve the current kernel version
echo "Retrieving the current kernel version..."
kernel_version=$(uname -r)
echo "The current kernel version is $kernel_version"

# Construct the package name
tools_package_name="linux-tools-$kernel_version"

# Check if perf is already installed
if ! command -v perf >/dev/null 2>&1; then
    echo "perf is not installed. Installing perf package: $tools_package_name"
    sudo apt-get update
    sudo apt-get install -y "$tools_package_name"
else
    echo "perf is already installed."
fi

# Create perf_users group
if ! getent group perf_users >/dev/null; then
    echo "Creating perf_users group..."
    sudo groupadd -g 1500 perf_users
else
    echo "perf_users group already exists."
fi

# Change group ownership of the perf executable
if [ -e /usr/bin/perf ]; then
    echo "Changing group ownership of /usr/bin/perf to perf_users..."
    sudo chgrp perf_users /usr/bin/perf
else
    echo "/usr/bin/perf not found."
fi

# Change from 4 to -1 to allow perf tools for users with CAP_PERFMON capabilities
echo "kernel.perf_event_paranoid=-1" | sudo tee -a /etc/sysctl.conf

# Disable kptr restrictions
echo "kernel.kptr_restrict=0" | sudo tee -a /etc/sysctl.conf

# Apply the sysctl settings
sudo sysctl -p

# Restrict access to perf executable
echo "Restricting access to /usr/bin/perf..."
sudo chmod 0750 /usr/bin/perf
sudo chown root:perf_users /usr/bin/perf

# Set required capabilities on the perf executable
echo "Setting capabilities on /usr/bin/perf..."
sudo setcap "cap_perfmon,cap_sys_ptrace,cap_syslog=ep" /usr/bin/perf

# Verify capabilities on perf executable
echo "Verifying capabilities on /usr/bin/perf..."
perf_capabilities=$(getcap /usr/bin/perf)
echo "Capabilities: $perf_capabilities"

# Create privileged shell script
echo "Creating /usr/local/bin/perf.shell..."
sudo bash -c 'cat > /usr/local/bin/perf.shell <<EOF
#!/bin/bash
exec /usr/sbin/capsh --iab=^cap_perfmon,^cap_sys_ptrace --secbits=239 --user=\$SUDO_USER -- -l
EOF'
sudo chmod 0755 /usr/local/bin/perf.shell

# Path to the user list file
userlistfile="/opt/parallelcluster/shared/userlistfile"

# Check if the user list file exists
if [ ! -f "$userlistfile" ]; then
    echo "User list file not found: $userlistfile"
    exit 1
fi

# Read the user list and add each user to the perf_users group
while IFS=',' read -r username userid; do
    echo "Adding user $username (UID: $userid) to the perf_users group..."
    sudo usermod -aG perf_users "$username"
done < "$userlistfile"

# Backup sudoers file
sudo cp /etc/sudoers /etc/sudoers.bak

# Function to safely update the sudoers file
update_sudoers() {
    local regexp="$1"
    local line="$2"
    local sudoers_file="/etc/sudoers"

    # Check if the line exists in the sudoers file
    if ! grep -q "$regexp" "$sudoers_file"; then
        echo "Updating sudoers file with: $line"
        # Append the line to the sudoers file
        echo "$line" | sudo tee -a "$sudoers_file" > /dev/null

        # Validate the sudoers file
        sudo visudo -cf "$sudoers_file"
        if [[ $? -ne 0 ]]; then
            echo "Validation failed for the sudoers file. Restoring the original."
            sudo cp "$sudoers_file.bak" "$sudoers_file"
            exit 1
        fi
    else
        echo "Line matching '$regexp' already exists in sudoers file."
    fi
}

# Update sudoers file to preserve home for perf_users group
update_sudoers '^Defaults:%perf_users' 'Defaults:%perf_users env_keep += "HOME"'

# Update sudoers file with permissions for perf_users group
update_sudoers '^%perf_users' '%perf_users    ALL=(ALL) NOPASSWD: /usr/local/bin/perf.shell'


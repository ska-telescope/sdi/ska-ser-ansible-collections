#!/bin/bash

# Packages to install
PACKAGES=("s3fs" "xvfb") 

# Update package list
echo "Updating package list..."
sudo apt update

for PACKAGE in "${PACKAGES[@]}"; do
    echo "Installing $PACKAGE..."
    sudo apt install -y "$PACKAGE"
    if [ $? -eq 0 ]; then
        echo "$PACKAGE installed successfully."
    else
        echo "Failed to install $PACKAGE. Exiting script."
        exit 1
    fi
done

echo "All packages installed successfully."
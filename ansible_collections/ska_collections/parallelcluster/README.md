# SKA ParallelCluster Ansible Collection

This collection provisions ParallelCluster hardware, namely the Head Node and configures users, monitoring and sets debugging privileges

## Ansible

Tested with the current Ansible 9.4.x releases.

# Collection Usage

| Targets | Description | Usage |
| ------- | ----------- | ----- |
| provision_headnode | Provision the cluster Head Node | make playbooks parallelcluster provision_headnode PLAYBOOKS_HOSTS=aws-eu-west-2-dp-hpc-parallelcluster-headnode |

# Cluster information

See the [DP HPC Cluster Documentation](https://confluence.skatelescope.org/display/SE/DP+HPC+Cluster+%28Pilot%29+Information) for information on the cluster setup, specifications and how to gain access to the Head Node after provisioning.
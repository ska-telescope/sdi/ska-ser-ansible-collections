# Ansible Collection - `ska_collections.low_cbf`

This directory contains the `ska_collections.low_cbf` Ansible Collection.
The collection contains roles and playbooks to provision LOW-CBF hardware.

This collection is currently maintained by [SKAO](https://www.skao.int/).

## Ansible Versions

Tested with Ansible 6.7.x.

## Ansible Roles

| Name                                             | Description                                               |
| ------------------------------------------------ | --------------------------------------------------------- |
| [`ska_collections.low_cbf.alveo`](./roles/alveo) | Sets up Xilinx Alveo FPGAs.                               |
| [`ska_collections.low_cbf.p4`](./roles/p4)       | Compiles and installs the LOW-CBF P4 code on P4 switches. |

## Ansible Playbooks

| Name                                                 | Description                                                                                                             |
| ---------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| [`install-alveo.yml`](./playbooks/install-alveo.yml) | Sets up Xilinx Alveo FPGAs on the target hosts.                                                                         |
| [`install-p4.yml`](./playbooks/install-p4.yml)       | Does a full install of the LOW-CBF P4 code, including the installation of the background service and the SDE if needed. |
| [`uninstall-p4.yml`](./playbooks/uninstall-p4.yml)   | Does a full uninstall of the LOW-CBF P4 code, including the removal of the background service and the SDE.              |
| [`update-p4.yml`](./playbooks/update-p4.yml)         | Updates the LOW-CBF P4 code on an already provisioned P4 switch.                                                        |

## Installation

To install, add the following to your `requirements.yml` and then run `ansible-galaxy install -r requirements.yml`.

```yaml
collections:
  - name: https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections.git#/ansible_collections/ska_collections/low_cbf/
    type: git
    version: main
```

# Ansible Role - `ska_collections.low_cbf.alveo`

This directory contains the `ska_collections.low_cbf.alveo` Ansible Role.
The role is used to set up the Xilinx Alveo FPGAs used by LOW-CBF by performing the following on every target host:

- Install the Xilinx XRT and platform packages;
- Detect all installed FPGAs and flash them if needed;
- Reboot the host if any FPGA was flashed.

**Note: The role is currently hard-coded to support the Xilinx Alveo U55C, and the Xilinx platform version 2022.2.**
Support for other FPGA models or platform versions may be added in the future.

## Prerequisites

### Remote Host

The remote host is assumed to be running Ubuntu 22.04 LTS.
The `{{ ansible_user }}` should have passwordless sudo permissions.

### Alveos

The alveos should be installed in the host prior to running this role.

## Ansible Variables

The variables used in this role are not meant to be changed, unless you really know what you are doing.

## Authors

- [@deneys.maartens](https://gitlab.com/deneys.maartens)
- [@sanderploegsma](https://gitlab.com/sanderploegsma)

## Notes

This role is an adaptation of the [`accelize.xilinx_xrt`][accelize-repo] role, which has been deprecated and does not support Ubuntu versions later than 20.04.

[accelize-repo]: https://github.com/Accelize/ansible-role-xilinx_xrt

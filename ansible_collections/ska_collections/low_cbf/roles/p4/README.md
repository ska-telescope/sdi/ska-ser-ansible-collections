# Ansible Role - `ska_collections.low_cbf.p4`

This directory contains the `ska_collections.low_cbf.p4` Ansible Role.
The role is used to install the LOW-CBF switch code onto Edge-Core DCS810 P4 switches by performing the following on every target host:

- Install required packages;
- Copy, extract and compile the Intel SDE if it doesn't exist yet (unless overridden);
- Install the compiled kernel modules and reboot the host if needed;
- Install the `systemd` service used to run the switch daemon;
- Checkout the LOW-CBF P4 code and compile it with the Intel SDE;
- Start the switch daemon.

Please note that compiling the SDE from scratch takes a _very_ long time: 3 to 5 hours is not uncommon.
Subsequent compilations are faster - usually under an hour.

## Prerequisites

### Remote Host

The remote host is assumed to be an Edge-Core DCS810 P4 switch running Ubuntu 22.04 LTS.
The `{{ ansible_user }}` should have passwordless sudo permissions.

### Intel SDE

Before using this role, you have to obtain a copy of the Intel SDE.
A license should already be procured as part of [SST-790].
Use the corresponding account details to download the SDE from the [P4 Studio download page].

The download should contain the following tarballs:

- `bf-sde-<version>.tgz`
- `bf-reference-bsp-<version>.tgz`

Place these files somewhere in your local Ansible repository.
**UNDER NO CIRCUMSTANCES SHOULD THESE FILES BE PUBLISHED ANYWHERE PUBLIC**, so make sure to add them to your `.gitignore`.

## Ansible Variables

This role can be configured using the following variables:

| Name                             | Has default value | Description                                                                                                                                 |
| -------------------------------- | ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| `low_cbf_p4_code_repo`           | Yes               | The repository containing the LOW-CBF P4 code.                                                                                              |
| `low_cbf_p4_code_ref`            | Yes               | The version of the LOW-CBF P4 code to check out before compiling. This can be any valid Git reference such as a tag, branch name or commit. |
| `low_cbf_p4_code_array_assembly` | Yes               | The LOW-CBF P4 code is archived per Array Assembly. For valid options, see the `p4_code` directory in the LOW-CBF P4 repository.            |
| `low_cbf_p4_code_dst_path`       | Yes               | The path on the remote host where the `low_cbf_p4_code_repo` should be cloned.                                                              |
| `low_cbf_p4_sde_version`         | Yes               | The version of the SDE to use to compile and run the P4 code.                                                                               |
| `low_cbf_p4_repository_url`      | Yes                | The URL to the Nexus RAW repository containing the SDE tarballs for the configured.                                         |
| `low_cbf_p4_repository_user`     | Yes                | The username for accessing the repository.                                         |
| `low_cbf_p4_repository_password` | No                | The password for accessing the repository.                                         |
| `low_cbf_p4_sde_dst_path`        | Yes               | The path on the remote host where the SDE should be unpacked and stored.                                                                    |
| `low_cbf_p4_sde_force_recompile` | Yes               | Forces re-compilation of the SDE in case it already exists on the remote host. Use this when running into problems.                         |

## Authors

- [@bernardo.bacic](https://gitlab.com/bernardo.bacic)
- [@deneys.maartens](https://gitlab.com/deneys.maartens)
- [@gjourjon](https://gitlab.com/gjourjon)
- [@sanderploegsma](https://gitlab.com/sanderploegsma)

[P4 Studio download page]: https://www.intel.com/content/www/us/en/secure/confidential/collections/programmable-ethernet-switch-products/p4-suite/p4-studio.html
[SST-790]: https://jira.skatelescope.org/browse/SST-790

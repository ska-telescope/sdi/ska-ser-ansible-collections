# SKA Gitlab Runner Ansible Collection

This collection registers and runs both a single [gitlab-runner](https://docs.gitlab.com/runner/) using docker as well as multiple gitlab-runners in a Kubernetes cluster, along with the installation of [MinIO](https://min.io/) for S3-compatible caching.
This collection is currently maintained by [SKAO](https://www.skao.int/).

## Ansible

Tested with the current Ansible 7.7.0 release.

## Ansible Roles
| Name | Description | Version | OS Requirements | Dependencies |
| ---- | ----------- | ------- | --- | ---|
| [gitlab_runner.docker](./roles/docker) | Install and configure a Gitlab Docker runner | 17.0.0 | Ubuntu 18+ (LTS) | |
| [gitlab_runner.k8s](./roles/k8s) | Install and configure a Gitlab K8s Runner | 17.0.0 | | Kubernetes |
| [gitlab_runner.minio](./roles/minio) | Deploy MinIO to Kubernetes cluster | 4.3.7 |  | Kubernetes |

## Usage

Installation/Undeployment playbooks for each engine can be found in the [playbooks/](./playbooks) folder in the following files:

| Name | Description |
| ---- | ----------- |
| [install_runner_docker_executor.yml](./playbooks/install_runner_docker_executor.yml) | Install a docker-based Gitlab runner |
| [destroy_runner_docker_executor.yml](./playbooks/destroy_runner_docker_executor.yml) | Undeploy a docker-based Gitlab runner |
| [install_runner_k8s_executor.yml](./playbooks/install_runner_k8s_executor.yml) | Install a kubernetes-based Gitlab runner |
| [destroy_runner_k8s_executor.yml](./playbooks/destroy_runner_k8s_executor.yml) | Undeploy a kubernetes-based Gitlab runner |
| [deploy_minio.yml](./playbooks/deploy_minio.yml) | Deploy MinIO to Kubernetes |
| [destroy_minio.yml](./playbooks/destroy_minio.yml) | Undeploy MinIO to Kubernetes |


The playbooks should be run through the use of make targets, described in [gitlab-runner.mk](../../../resources/jobs/gitlab-runner.mk), following the pattern:
`` make playbooks gitlab-runner <operation> PLAYBOOKS_HOSTS=<host>``

Before executing the playbooks the `` set-env`` script should be run so that, adding to the variables required by default to execute any playbook, the required gitlab runner-specific secrets can be retrieved from Vault.

### Kubernetes Runner (k8s role)

By default, the Kubernetes runners will use the underlying host Docker engine - by mounting docker.sock - to build images. For systems that do not have Docker installed, we can use DIND (docker-in-docker) to fulfil this role.

To enable DIND as the Docker engine set `gitlab_runner_k8s_dind_enabled` to `true`.

In this role, we provide two modes of operation using DIND:

* DIND as service (set `gitlab_runner_k8s_dind_as_service` to `true`):
  * This mode deploys DIND to the Kubernetes cluster with a Service with `sticky-sessions` fronting all the instances
  * This improves layer caching as it is long lived
  * Shared between all the builds running in all runners
  * Set the appropriate endpoint for the DIND service using `gitlab_runner_k8s_dind_host`
* DIND as sidecar (set `gitlab_runner_k8s_dind_as_service` to `false`):
  * This mode creates a DIND container as a sidecar to the runner
  * No layer caching between builds


## How to Contribute

### Adding a new role
A new role can be added to the [roles](./roles/) folder and then included into a new and/or existing playbook.

### Updating an existing role
The existing roles can be found in the [roles](./roles/) folder. To update a role, the role's tasks can be simply modified.

### External dependencies
Go to [requirements.yml](../../../requirements.yml) and [galaxy.yml](./galaxy.yml) files to add or update any external dependency.

### Add/Update new variables
Ansible variables that are datacentre specific should be added to the `group_vars` folder of the inventory.

To modify non-secret variable role defaults, go to the defaults folder of the respective role and update them. As an [example](./roles/runner/defaults/main.yml).

Finally, the secret variables are defined in the respective [Makefile](../../../resources/jobs/gitlab-runner.mk) and can be modified there. To assign proper values to these variables, please use a `PrivateRules.mak` file.

## More information

- [Ansible Using collections](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html)
- [Ansible Collection overview](https://github.com/ansible-collections/overview)
- [Ansible User guide](https://docs.ansible.com/ansible/latest/user_guide/index.html)
- [Ansible Developer guide](https://docs.ansible.com/ansible/latest/dev_guide/index.html)
- [Ansible Community code of conduct](https://docs.ansible.com/ansible/latest/community/code_of_conduct.html)
## License

BSD-3.
# Gitlab Runner Collection - Kubernetes role
This repo is meant to work with the [ska-ser-infra-machinery](https://gitlab.com/ska-telescope/sdi/ska-ser-infra-machinery) repository.

All the operations should, therefore, be run from the ska-ser-infra-machinery repo, instead of directly from the Ansible Collections one or there will be several variables that will need to be defined from the level where the playbook is being run.

### Important Notes
To use this collection to deploy or undeploy a runner it should be noticed that:
* If a runner token is created manually, from the Gitlab UI, it should be added to the path defined by the vars ``{{ gitlab_runner_k8s_credentials_secret_path }}/{{ gitlab_runner_k8s_runner_name }}`` in Vault.
* If a runner token isn't created manually the playbook will create a one and push it to Vault under the same path described in the previous point
* The list of runners - and their corresponding configs - to be deployed is defined under the variable ``{{ gitlab_runner_k8s_runners }}``
* When a runner is undeployed its runner token will also be deleted from Vault

## How to use
The available operations are:
* **Deploy** a Gitlab runner to a kubernetes cluster
    ```console
    make playbooks gitlab-runner deploy_k8s_runner PLAYBOOKS_HOSTS=<host>
    ```
* **Undeploy** a Gitlab runner from a kubernetes cluster
    ```console
    make playbooks gitlab-runner destroy_k8s_runner PLAYBOOKS_HOSTS=<host>
    ```
* **Test** a Gitlab runner is running as expected in a kubernetes cluster
    ```console
    make playbooks gitlab-runner test_k8s_runner PLAYBOOKS_HOSTS=<host>
    ```

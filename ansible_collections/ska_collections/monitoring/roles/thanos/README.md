# THANOS

Thanos is a set of components that can be composed into a highly available metric system with unlimited storage capacity, which can be added seamlessly on top of existing Prometheus deployments. [link](https://github.com/thanos-io/thanos)

[Thanos releases](https://github.com/thanos-io/thanos/releases)

## AWS

If you want to install in aws you can do the following commands

```shell
aws-vault list

aws-vault login CREDENTIAL

asw-vault exec CREDENTIAL
```

## Pre-install

Make sure you have the secrets before start the deployments. Set secrets in env vars or get it from vault:

```shell
make im-fetch-secrets
```

Set the env vars:

```shell
# example

ENVIRONMENT=production
DATACENTRE=aws-eu-west-2
SERVICE=monitoring

```

## Thanos deployment

The deployment of the various components of Thanos via make targets

```shell
# example
PLAYBOOKS_HOSTS=Target_HOST make playbooks monitoring thanos_receive
'''

The thanos monitoring targets are:
- thanos_query (Thanos query and query front-end)
  - Aggregates and queries metrics from multiple Prometheus instances, with a query front-end.
- thanos_store
  - Stores and retrieves historical metric data.
- thanos_compactor
  - Compacts and downsamples data blocks to reduce storage usage.
- thanos_receive
  - Receives and stores metrics from Prometheus remote write.
- thanos_sidecar (if needed is used next to the Prometheus)
  - Runs alongside Prometheus to upload data to Thanos storage.
- thanos_ruler
  - Executes alerting rules over several datacentres using the `thanos_ruler_monitoring_datacentres` list variable.


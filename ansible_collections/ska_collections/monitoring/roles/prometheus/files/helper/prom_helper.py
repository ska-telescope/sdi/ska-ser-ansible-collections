"""
This script generates a series of prometheus configuration files
(json files) with targets for each exporter, as well as a set of
relabel instructions for the collected metrics
"""

import argparse
import datetime
import glob
import json
import logging
import os
import re
import socket
import sys
from collections import defaultdict

import yaml
from ansible.inventory.manager import InventoryManager
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager

EXPORTERS = {
    "node_exporter": {"name": "node", "port": 9100, "warn": True},
    "apt_exporter": {"name": "apt_exporter", "port": 9097, "warn": True},
    "elasticsearch_exporter": {"name": "elasticsearch", "port": 9114},
    "ceph-mgr": {"name": "ceph_cluster", "port": 9283},
    "docker_exporter": {"name": "docker", "port": 9323, "warn": True},
    "docker_cadvisor": {"name": "docker_cadvisor", "port": 9324},
    "kube-proxy": {"name": "kube-proxy", "port": 10249},
}
RELABEL_KEY = "prometheus_node_metric_relabel_configs"
IGNORED_HOSTS = ["localhost"]

targets = defaultdict(list)
hostrelabelling = {RELABEL_KEY: []}


def check_port(address, port):
    """
    Checks if a port on a host is opened
    """
    location = (address, port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(0.3)
    return sock.connect_ex(location)


def generate_targets_from_inventory(
    inventory,
):  # pylint: disable=too-many-locals
    """
    This method parses the provided inventory file and generate targets
    for prometheus by iterating over the EXPORTERS list and finding
    the matching ports in the machines.
    If the port is open, the target is generated.
    """
    sources = []
    if os.path.isdir(inventory):
        for filename in os.listdir(inventory):
            file_complete_path = os.path.join(inventory, filename)
            if (
                os.path.isfile(file_complete_path)
                and filename.endswith("yml")
                and not os.path.islink(file_complete_path)
                and not filename == "groups.inventory.yml"
            ):
                sources.append(file_complete_path)
    else:
        sources.append(inventory)

    for source in sources:
        loader = DataLoader()
        inventory_manager = InventoryManager(loader=loader, sources=source)
        variable_manager = VariableManager(
            loader=loader, inventory=inventory_manager
        )

        for host in inventory_manager.get_hosts():
            host_vars = variable_manager.get_vars(host=host)
            hostname = host_vars["inventory_hostname"]
            host_ip = host_vars.get("ip", host_vars.get("ansible_host", None))

            if host_ip is None:
                raise ValueError(f"Could not find IP for host {hostname}")

            print(f"\n** Checking {hostname} [{host_ip}]")
            for exporter_name, details in EXPORTERS.items():
                port = details["port"]
                result_of_check = check_port(host_ip, port)
                if result_of_check == 0:
                    print(f"\t{exporter_name} {hostname}:{port}")
                    targets[exporter_name].append(f"{host_ip}:{str(port)}")
                    hostrelabelling[RELABEL_KEY].append(
                        {
                            "source_labels": ["instance"],
                            "regex": re.escape(host_ip) + ":" + str(port),
                            "action": "replace",
                            "target_label": "instance",
                            "replacement": hostname + ":" + str(port),
                        }
                    )
                elif details.get("warn", False):
                    print(f"WARNING: Port {port} not open at {hostname}")


def write_config_files(output_dir):
    """
    Writes the exporter config files and the metric relabel
    configurations
    """
    print(f"\nOutputing files to: {output_dir}")
    for exporter, exporter_config in EXPORTERS.items():
        exporter_targets = targets[exporter]
        job = [
            {
                "labels": {"job": exporter_config["name"]},
                "targets": exporter_targets,
            }
        ]

        json_file = os.path.join(output_dir, f"{exporter}.json")
        print(f"Generating {json_file}")
        if len(exporter_targets) == 0:
            print(f"WARNING: Exporter '{exporter}' has no targets")
        with open(json_file, "w", encoding="utf-8") as outfile:
            json.dump(job, outfile, indent=2)

    yaml_file = os.path.join(output_dir, f"{RELABEL_KEY}.yaml")
    print(f"Generating {yaml_file}")
    with open(yaml_file, "w", encoding="utf-8") as outfile:
        yaml.dump(hostrelabelling, outfile, indent=2)


start_time = datetime.datetime.now()

logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger(__name__)

if os.environ.get("http_proxy") or os.environ.get("https_proxy"):
    LOG.WARN("Proxy env vars set")

parser = argparse.ArgumentParser()
parser.add_argument(
    "-i",
    "--inventory",
    help="Inventory file or folder",
)
parser.add_argument(
    "-g",
    "--glob",
    help="Inventory file or folder with glob helper for multiple directories",
)
parser.add_argument(
    "-o",
    "--output",
    default=".",
    help="Output directory where to place the json files",
)

if __name__ == "__main__":
    args = parser.parse_args()
    if args.inventory is None and args.glob is None:
        print(
            (
                "Please provide an inventory file",
                "or folder with the --inventory option",
            )
        )
        parser.print_help(sys.stderr)
        sys.exit(1)

    if args.inventory:
        generate_targets_from_inventory(args.inventory)

    if args.glob:
        inventories = glob.glob(args.glob)
        for inv in inventories:
            generate_targets_from_inventory(inv)

    write_config_files(args.output)

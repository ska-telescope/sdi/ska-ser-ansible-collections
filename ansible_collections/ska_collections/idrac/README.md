# SKA Ansible iDRAC Base collection

This collection includes a variety of Ansible roles to help automate HPE server lifecycle management using iDRAC.
This collection is currently maintained by [SKAO](https://www.skao.int/).

## Usage

Installation playbooks for each engine can be found in the [playbooks/](./playbooks) folder in the following files:

| Name | Description |
| ---- | ----------- |
| [idrac-firmware-update.yml](./playbooks/idrac-firmware-update.yml) | Update iDRAC firmware version |
| [idrac-firmware-version-check.yml](./playbooks/idrac-firmware-version-check.yml) | Check iDRAC firmware version |
| [idrac-info.yml](./playbooks/idrac-info.yml) | Display iDRAC info: iDRAC IP, Hardware Model, BIOS Version and iDRAC Firmware Version |

In order to run these playbooks, it's needed to specify the Ansible Inventory location and the respective group/hosts ***target_hosts*** variable.

Install **Docker** as an example:
```
ansible-playbook <playbooks-folder-path>/docker.yml \
	-i $(INVENTORY) \
	--extra-vars "target_hosts=<target-hosts>"
```

> To run the playbook on every host in the inventory select **all** as *target_hosts*

## How to Contribute

### Adding a new role
A new role can be added to the [roles](./roles/) folder and then included into a new and/or existing playbook.

### Updating an existing role
The existing roles can be found in the [roles](./roles/) folder. To update a role, the role's tasks can be simply modified.

### External dependencies
Go to [requirements.yml](../../../requirements.yml) and [galaxy.yml](./galaxy.yml) files to add or update any external dependency.

### Add/Update new variables
Ansible variables that are datacentre specific should be added to the `group_vars` folder of the inventory.

To modify non-secret variable role defaults, go to the defaults folder of the respective role and update them. As an [example](./roles/idrac/defaults/main.yml).

Finally, the secret variables are defined in the respective [Makefile](../../../resources/jobs/idrac.mk) and can be modified there. To assign proper values to these variables, please use a `PrivateRules.mak` file.

## More information

- [Ansible Using collections](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html)
- [Ansible Collection overview](https://github.com/ansible-collections/overview)
- [Ansible User guide](https://docs.ansible.com/ansible/latest/user_guide/index.html)
- [Ansible Developer guide](https://docs.ansible.com/ansible/latest/dev_guide/index.html)
- [Ansible Community code of conduct](https://docs.ansible.com/ansible/latest/community/code_of_conduct.html)
- [Ansible Modules for Dell iDRAC](https://docs.ansible.com/ansible/latest/collections/dellemc/openmanage/index.html)

## License

BSD-3.

---
- name: Get group hierarchy
  ansible.builtin.uri:
    url: "{{ gitlab_api_url }}/groups/{{ gitlab_namespace_id }}"
    method: GET
    headers:
      PRIVATE-TOKEN: "{{ gitlab_access_token }}"
  register: group_hierarchy

- name: Check if the namespace is under SKA Developers
  ansible.builtin.fail:
    msg: "A repository cannot be created under SKA Developers or any of its subgroups."
  when: group_hierarchy.json.full_path is search('ska-telescope/ska-dev')

- name: Get SKAO Gitlab repos
  ansible.builtin.uri:
    url: "{{ gitlab_api_url }}/groups/{{ gitlab_skao_group_id }}/projects?search={{ gitlab_repo_name }}"
    method: GET
    force: true
    return_content: true
    headers:
      PRIVATE-TOKEN: "{{ gitlab_access_token }}"
    body_format: "json"
    body:
      include_subgroups: true
  register: repos_list

- name: Check if repo exists
  when: '( repos_list.json | length == 0 )'
  ansible.builtin.debug:
    msg: "Repo doesn't exist yet, you are good to go!"

- name: Create and Configure Gitlab repo
  when: '( repos_list.json | length == 0 )'
  block:
    - name: Create Gitlab repo
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/"
        method: POST
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "json"
        body:
          name: "{{ gitlab_repo_name }}"
          namespace_id: "{{ gitlab_namespace_id }}"
          initialize_with_readme: true
          visibility: public # always keep this public. If a private repo is requested, talk with PO first to clarify the reason
          only_allow_merge_if_all_discussions_are_resolved: true
          only_allow_merge_if_pipeline_succeeds: true
          default_branch: "main"
          squash_option: "never"
          request_access_enabled: false
          security_and_compliance_access_level: "private"
          model_experiments_access_level: "disabled"
          build_timeout: 7200
        status_code: 201
      register: repo_out

    - name: Check repo created
      ansible.builtin.debug:
        msg: "{{ repo_out.json }}"

    - name: Enable MR results pipelines and MR trains
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}"
        method: PUT
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "json"
        body:
          merge_pipelines_enabled: true
          merge_trains_enabled: true

    - name: Get SKAO Gitlab group details
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}//groups/{{ gitlab_skao_group_id }}"
        method: GET
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
      register: repo_licenses

    - name: Get SKAO License Template
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/templates/licenses/{{ gitlab_skao_license_filename }}"
        method: GET
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "json"
        body:
          name: "{{ gitlab_skao_license_filename }}"
          source_template_project_id: "{{ repo_licenses.json.file_template_project_id }}"
      register: skao_license_template

    - name: Push SKAO License Template
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/repository/files/{{ gitlab_push_license_file_path }}"
        method: POST
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "raw"
        body:
          branch=main&file_path={{ gitlab_push_license_file_path }}&commit_message={{ gitlab_license_commit_message }}&content={{ skao_license_template.json.content }}
        status_code: 201

    - name: Get protected branch settings
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/protected_branches/main"
        method: GET
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
      register: protected_branch

    - name: Define protected branches settings
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/protected_branches/main"
        method: PATCH
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "json"
        body:
          allowed_to_merge:
            - id: "{{ protected_branch.json.merge_access_levels[0].id }}"
              _destroy: 1
              access_level: 40 # maintainer access
            - access_level: 30 # developer access
          allowed_to_push:
            - id: "{{ protected_branch.json.push_access_levels[0].id }}"
              _destroy: 1
            - access_level: 0 # no access
          allow_force_push: false
          code_owner_approval_required: false

    - name: Define protected environment
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/protected_environments"
        method: POST
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "json"
        body:
          name: "production"
          deploy_access_levels:
            - access_level: 40 # maintainer access
        status_code: 201

    - name: Get merge request approval rules
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/approval_rules"
        method: GET
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
      register: mr_settings

    - name: Create merge request approval rule
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/approval_rules"
        method: POST
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "json"
        body:
          name: "All Members"
          approvals_required: 1
        status_code: 201

    - name: Get merge request approval rules
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/approval_rules"
        method: GET
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
      register: mr_settings

    - name: Share project with SKAO Developers group
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out.json.id }}/share"
        method: POST
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "json"
        body:
          group_access: 30 # developer access
          group_id: "{{ gitlab_skao_developers_group_id }}"
        status_code: 201

- name: Set fact about repo in case it already exists
  when: '( repos_list.json | length > 0 )'
  ansible.builtin.set_fact:
    repo_out_id: "{{ repos_list.json[0].id }}"
    repo_out_ssh_url_to_repo: "{{ repos_list.json[0].ssh_url_to_repo }}"

- name: Set fact about repo in case it was just created
  when: '( repos_list.json | length == 0 )'
  ansible.builtin.set_fact:
    repo_out_id: "{{ repo_out.json.id }}"
    repo_out_ssh_url_to_repo: "{{ repo_out.json.ssh_url_to_repo }}"

- name: Set fact about repo maintainers
  ansible.builtin.set_fact:
    repo_maintainers: "{{ gitlab_repo_maintainers | split(',') }}"

- name: Add Maintainers to repo
  when: 'repo_out_id is defined'
  block:
    - name: Check if Gitlab user exists
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/users"
        method: GET
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "raw"
        body:
          username={{ item }}
      register: user_check
      with_items: "{{ repo_maintainers }}"

    - name: Check if Gitlab user is a member of the SKAO group
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/groups/{{ gitlab_skao_group_id }}/members/{{ item.json[0].id }}"
        method: GET
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        status_code: 200
      with_items: "{{ user_check.results }}"
      loop_control:
        label: "{{ item.json[0].name }}"
      register: skao_members

    - name: Add user as maintainer
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out_id }}/members"
        method: POST
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
        body_format: "json"
        body:
          user_id: "{{ item.json[0].id }}"
          access_level: 40 # maintainer access
        status_code: [200, 201, 409]
      with_items: "{{ user_check.results }}"
      loop_control:
        label: "{{ item.json[0].name }}"

- name: Configure Github Repo and Mirroring
  block:
    - name: Github repo setup
      ansible.builtin.include_tasks:
        file: configure_github_repo.yml

    - name: Set fact about repo in case it already exists
      when: 'repo_search_result.json.id is defined'
      ansible.builtin.set_fact:
        github_repo_fullname: "{{ repo_search_result.json.full_name }}"

    - name: Set fact about repo in case was just created
      when: 'github_repo.json.id is defined'
      ansible.builtin.set_fact:
        github_repo_fullname: "{{ github_repo.json.full_name }}"

    - name: Mirror Gitlab repo to Github
      when: 'github_repo_fullname is defined and (( repos_list.json | length == 0 ) or github_mirror_refresh )'
      block:
        - name: Create mirror
          ansible.builtin.uri:
            url: "{{ gitlab_api_url }}/projects/{{ repo_out_id }}/remote_mirrors"
            method: POST
            force: true
            return_content: true
            headers:
              PRIVATE-TOKEN: "{{ gitlab_access_token }}"
            body_format: "json"
            body:
              url: "https://{{ github_admin_handle }}:{{ github_mirror_token }}@github.com/{{ github_repo_fullname }}"
              enabled: true
              auth_method: "password"
            status_code: 201
          register: github_mirror

        - name: Check mirror created
          ansible.builtin.debug:
            msg: "{{ github_mirror.json }}"

    - name: Get mirror status
      ansible.builtin.uri:
        url: "{{ gitlab_api_url }}/projects/{{ repo_out_id }}/remote_mirrors"
        method: GET
        force: true
        return_content: true
        headers:
          PRIVATE-TOKEN: "{{ gitlab_access_token }}"
      register: mirror_status

    - name: Print mirror status
      ansible.builtin.debug:
        msg: "{{ mirror_status.json }}"

    - name: Fail if mirror is in error state
      ansible.builtin.fail:
        msg: "Mirror is in error state"
      when: mirror_status.json.last_error is defined

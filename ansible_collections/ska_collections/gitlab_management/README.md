# Gitlab Management Collection - ska_collections.gitlab_management

The purpose of this collection is to automate some of the Gitlab Management operations performed by the System Team. 

Currently, the automated operations available are:
- Creating Gitlab Repos with all the required configs including Github Mirroring
- Adding and/or Removing Users to/from the SKAO Group and SKAO Developers subgroup

## Required and Optional Variables

| Name | Description | Required for User Management Role | Required for Repo Management Role |
| ---- | ----------- | --------------------------------- | ------------------- |
| GITLAB_ACCESS_TOKEN | a Gitlab personal access token with permissions for user and repo management | Yes | Yes |
| GITLAB_HANDLE | gitlab username of the user to be added to the SKAO repos | Yes | N/A |
| GITLAB_NAMESPACE_ID | gitlab group where the new repo will be created <br>[**NOTE:** defaults to SKAO group] | N/A | No |
| GITLAB_REPO_NAME | name of the repo to be created <br>[**WARNING:** needs to start with the prefix *ska-* and use lower case with hyphens] | N/A | Yes |
| GITLAB_REPO_MAINTAINERS | a list of gitlab usernames to be added as maintainers <br>[**WARNING:** list must be separated by commas] | N/A | Yes |
| JIRA_ISSUE_ID | the support ticket ID requesting the new repo to be added to the initial repo's license commit message | N/A | Yes |
| GITHUB_MIRROR_TOKEN | a Github personal access token **for the marvin-skao user** with permissions for repo management | No | Yes |

## Available Targets
### Repository Management

- **Create a new repo** under the <ins>SKAO group</ins> called <ins>ska-new-repo-name</ins> attached to the support ticket <ins>STS-XYZ</ins> and with the maintainers whose gitlab handlers are <ins>user1, user2 and user3</ins>

    ```bash
    make playbooks gitlab-management add_gitlab_repo GITLAB_REPO_NAME=ska-new-repo-name JIRA_ISSUE_ID=STS-XYZ GITLAB_REPO_MAINTAINERS=user1,user2,user3 GITHUB_MIRROR_TOKEN=marvin-skao-pat-value PLAYBOOKS_HOSTS=localhost
    ```

- **Create a new repo** under the <ins>SKAO subgroup XPTO (id:12345)</ins> called <ins>ska-new-repo-name</ins> attached to the support ticket <ins>STS-XYZ</ins> and with the maintainers whose gitlab handlers are <ins>user1, user2 and user3</ins>

    ```bash
    make playbooks gitlab-management add_gitlab_repo GITLAB_REPO_NAME=ska-new-repo-name GITLAB_NAMESPACE_ID=12345 JIRA_ISSUE_ID=STS-XYZ GITLAB_REPO_MAINTAINERS=user1,user2,user3 GITHUB_MIRROR_TOKEN=marvin-skao-pat-value PLAYBOOKS_HOSTS=localhost
    ```

### User Management

- **Add a new user** to the <ins>SKAO group</ins> and <ins>SKAO Developers</ins> subgroup with the Gitlab handler <ins>fred-the-dev</ins>

    ```bash
    make playbooks gitlab-management add_gitlab_user GITLAB_HANDLE=fred-the-dev PLAYBOOKS_HOSTS=localhost
    ```
- **Remove an existing user** from the <ins>SKAO group</ins> and <ins>SKAO Developers</ins> subgroup with the Gitlab handler <ins>fred-the-dev</ins>

    ```bash
    make playbooks gitlab-management remove_gitlab_user GITLAB_HANDLE=fred-the-dev PLAYBOOKS_HOSTS=localhost
    ```
**IMPORTANT:** Before adding or removing a user, mind our [User Management Guidelines](https://confluence.skatelescope.org/display/SE/Adding+Members+and+Groups+in+GitLab)
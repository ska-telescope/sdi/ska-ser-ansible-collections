#!/bin/bash
set -x
ENDPOINT={{ cron_etcd_endpoint }}:2379
BACKUPDIR=/var/lib/backup
DTE=`date +"%Y.%m.%d.%H%M%S"`
DAILY_AGE=30
HOURLY_AGE=2

SNAPSHOT=${BACKUPDIR}/k8s-etcd-snapshot-${DTE}.db

sudo mkdir -p ${BACKUPDIR}

# backup etcd
sudo ETCDCTL_API=3 /usr/bin/etcdctl --endpoints ${ENDPOINT} \
     --cacert=/etc/kubernetes/pki/etcd/ca.crt  \
     --cert=/etc/kubernetes/pki/etcd/healthcheck-client.crt \
     --key=/etc/kubernetes/pki/etcd/healthcheck-client.key  \
     snapshot save ${SNAPSHOT}

# print the status of the backup
sudo ETCDCTL_API=3 etcdctl --write-out=table snapshot status ${SNAPSHOT}

# compress backup
gzip -9 ${SNAPSHOT}

# prune old backups
echo "Pruning files older than ${DAILY_AGE} days"
find ${BACKUPDIR} -type f -mtime +${DAILY_AGE} -print
find ${BACKUPDIR} -type f -mtime +${DAILY_AGE} -exec rm -rf {} \;

echo "Pruning files older than ${HOURLY_AGE} days, keeping 1 per day"
TEMP_DAY=""
# for each backup file older than ${HOURLY_AGE} days, sorted by newest to oldest
for BACKUP_FILE in `find ${BACKUPDIR} -type f -mtime +${HOURLY_AGE} -print | sort -r` ; do
     # save the day it was last modified
     DAY=`date -d @$(stat ${BACKUP_FILE} -c %Y) +"%d"`
     # if it matches the current day being processed
     if [[ ${DAY} == ${TEMP_DAY} ]] ; then
          # print and delete it
          echo ${BACKUP_FILE}
          rm -f ${BACKUP_FILE}
     else
          # otherwise, keep the file and update the day being processed
          TEMP_DAY=${DAY}
     fi
done

echo "Backup files:"
ls -latr ${BACKUPDIR}

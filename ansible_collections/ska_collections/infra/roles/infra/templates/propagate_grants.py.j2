#! /usr/bin/python3
import requests
import json
import re
import os

from urllib.parse import urljoin
from datetime import datetime
from pathlib import Path

INFRA_API_URL = "{{ infra_propagator_server_url }}"
INFRA_HEADERS = {
    "Content-Type": "application/json",
    "Infra-Version": "{{ infra_server_version }}",
    "Authorization": "Bearer {{ infra_propagator_access_key }}",
}

k8s_destinations_link= {
    "lowaa" : "au-aa-k8s-master02-k8s",
    "midaa" : "za-aa-k8s-master01-k8s",
    "lowitf": "au-itf-k8s-master01-k8s",
    "miditf" : "za-itf-k8s-master01-k8s",
    "dishitf" : "za-itf-dish-lmc-k8s",
    "aavs3itf" : "au-itf-aavs302-k8s",
    "mccsaa": "au-aa-mccs-cloud01-k8s",
    "dish063aa": "za-aa-ska063-k8s"
}


def get_all_destinations() -> dict:
    """Requests the list of destinations registered with infra.

    Raises:
        Exception: When the infra server does not return status code 200.

    Returns:
        dict: An array with the infra destinations.
    """
    return infra_api_get("destinations")

def sync_user_grants(managed_grants: dict):
    """Synchronizes the user grants with the group grants

    Raises:
        Exception: When the infra server does not return status code 200.
    """
    # Limit the sync to SKAOInfra Azure Provider users
    users = [u for u in get_all_users() if "SKAOInfra" in u["providerNames"]]

    # For each user
    for user in users:
        # Ensure the list of managed grants for the user exists
        managed_grants[user["id"]] = managed_grants.get(user["id"], [])

        # Get the user and associated group grants
        user_grants = format_grants(user, get_user_grants(user["id"]))

        group_grants = []
        for group in get_user_groups(user["id"]):
            group_grants.extend(format_grants(user, get_group_grants(group["id"])))

        # Process only SSH grants
        user_grants = [g for g in user_grants if g["resource"].endswith("-ssh")]
        group_grants = [g for g in group_grants if g["resource"].endswith("-ssh")]

        # Generate the list of grants to add and remove
        grantsToAdd = gen_grants_to_add(user_grants, group_grants)
        grantsToRemove = gen_grants_to_remove(
            user_grants, group_grants, managed_grants[user["id"]]
        )

        # Patch the user grants
        patch_user_grants(grantsToAdd, grantsToRemove)

        # Keep the managed grants up to date
        managed_grants[user["id"]] = [
            g
            for g in managed_grants[user["id"]]
            if g not in grantsToRemove and g in user_grants
        ]
        managed_grants[user["id"]].extend(
            [g for g in grantsToAdd if g not in managed_grants[user["id"]]]
        )


def get_all_users():
    """Requests the list of users registered with infra.

    Raises:
        Exception: When the infra server does not return status code 200.

    Returns:
        dict: An array with the infra users.
    """
    return infra_api_get("users")


def get_user_groups(userID: str) -> dict:
    """Requests the list of groups a given infra user belongs to.

    Args:
        userID (str): The ID of the infra user.

    Raises:
        Exception: When the infra server does not return status code 200.

    Returns:
        dict: An array with the user's groups.
    """
    return infra_api_get("groups", {"userID": userID})


def get_user_grants(userID: str) -> dict:
    """Requests the list of grants a given infra user has.

    Args:
        userID (str): The ID of the infra user.

    Raises:
        Exception: When the infra server does not return status code 200.

    Returns:
        dict: An array with the user's grants.
    """
    return infra_api_get("grants", {"user": userID})


def get_group_grants(groupID: str) -> dict:
    """Requests the list of grants a given infra group has.

    Args:
        groupID (str): The ID of the infra group.

    Raises:
        Exception: When the infra server does not return status code 200.

    Returns:
        dict: An array with the group's grants.
    """
    return infra_api_get("grants", {"group": groupID})


def infra_api_get(endpoint: str, params: dict = {}) -> dict:
    """Makes a GET request to the infra server.

    Args:
        endpoint (str): The infra API endpoint to send the request to.
        params (dict): The query parameters to send with the request.

    Raises:
        Exception: When the infra server does not return status code 200.

    Returns:
        dict: An array with the data items.
    """
    # Set the first page of the request to 1
    params["page"] = 1

    # Handle request pagination
    data = []
    while True:
        # Request a page of data
        response = requests.get(
            url=urljoin(INFRA_API_URL, endpoint),
            headers=INFRA_HEADERS,
            params=params,
        )

        # Ensure API status code is 200
        if response.status_code != 200:
            raise Exception("Could not obtain data.")

        # Convert the content to json
        content = response.json()

        # Append the received data
        data.extend(content["items"])

        # Break the loop after handling the last page
        if params["page"] >= content["totalPages"]:
            break

        # Update the params to request the next page
        params["page"] = params["page"] + 1

    # Return the data
    return data


def patch_user_grants(grantsToAdd: list, grantsToRemove: list):
    """Makes a PATCH request to update the user grants in bulk.

    Args:
        grantsToAdd (list): The list of grants to add to the user.
        grantsToRemove (list): The list of grants to remove from the user.

    Raises:
        Exception: When the infra server does not return status code 200.
    """
    response = requests.patch(
        url=urljoin(INFRA_API_URL, "grants"),
        headers=INFRA_HEADERS,
        data=json.dumps(
            {
                "grantsToAdd": grantsToAdd,
                "grantsToRemove": grantsToRemove,
            }
        ),
    )
    print("Grants to Add ", grantsToAdd, "Grants to Remove ", grantsToRemove)
    # Ensure API status code is 200
    if response.status_code != 200:
        raise Exception("Could not patch grants for user.")

def create_group_grants(privilege: str, resource: str, groupName: str):
    """Makes a POST request to create the group grants.

    Args:
        privilege (str): A role or permission
        resource (str): A resource name in Infra's Universal Resource Notation
        group (str): Name of the group granted access

    Raises:
        Exception: When the infra server does not return status code 201.
    """   

    privileges = privilege.split(',')

    for priv in privileges:
        response = requests.post(
            url=urljoin(INFRA_API_URL, "grants"),
            headers=INFRA_HEADERS,
            data=json.dumps(
                {
                    "privilege": priv,
                    "resource": resource,
                    "groupName" : groupName
                }
            ),
        )

        if response.status_code != 201:
            print("Privilege: ", priv, "Resource: ", resource, "Group Name: ", groupName)
        # Ensure API status code is 201
        if response.status_code != 201 and response.status_code != 200:
            raise Exception(f"Could not create grants for group with privilege '{priv}'.")


def format_grants(user: dict, grants: list) -> list:
    """Formats a list of grants returned by the infra API for a PATCH request

    Args:
        user (dict): The user's infra username.
        grants (list): The list of grants to format.

    Returns:
        list: The formatted list of grants.
    """
    return [
        {
            "userName": user["name"],
            "privilege": grant["privilege"],
            "resource": grant["resource"],
        }
        for grant in grants
    ]


def gen_grants_to_add(user_grants: list, group_grants: list) -> list:
    """Generates the list of grants to add to the user.

    Args:
        user_grants (list): The current list of user grants.
        group_grants (list): The list of grants assigned to the user groups.

    Returns:
        list: The list of missing grants.
    """
    return [grant for grant in group_grants if grant not in user_grants]


def gen_grants_to_remove(
    user_grants: list, group_grants: list, managed_grants: list
) -> list:
    """Generates the list of managed grants to remove from the user.

    Args:
        user_grants (list): The current list of user grants.
        group_grants (list): The list of grants assigned to the user groups.

    Returns:
        list: The list of grants to remove.
    """
    return [
        grant
        for grant in user_grants
        if grant in managed_grants and grant not in group_grants
    ]

def parse_group_access(group_name : str):

    group_name_split = group_name.split('-',maxsplit=3)

    # Grant K8s Access
    if group_name_split[0] == "k8s":
        resource = k8s_destinations_link[group_name_split[1]]
        role = group_name_split[2]
        if role == "admin":
            role = "cluster-admin"
        if role == "aivadmin":
            role = "aivadmin,admin"
        # If number of fields is 3 than is cluster wide access
        if len(group_name_split) > 3:
            namespace = group_name_split[3]
            resource = resource + "." + namespace

    # Grant SSH Access
    if group_name_split[0] == "ssh":
        role = "connect"
        # For SSH access max number of fields is 3
        if len(group_name_split) == 4:
            group_name_split[2] = group_name_split[2] + "-" + group_name_split[3]
            del group_name_split[3]
        
        resource =  group_name_split[2] + "-ssh"

    create_group_grants(role,resource,group_name)

def manage_wildcard_groups( wildcard_groups : list):

    for group in wildcard_groups:
        group_name_split = group["name"].split('-',maxsplit=3)

        group_grants = [item["resource"] for item in get_group_grants(group["id"])]
        # Grant K8s Access
        if group_name_split[0] == "k8s":
            resource = k8s_destinations_link[group_name_split[1]]
            role = group_name_split[2]
            if role == "admin":
                role = "cluster-admin"
            if role == "aivadmin":
                role = "aivadmin,admin"
            # If number of fields is 3 than is cluster wide access
            if len(group_name_split) > 3:
                namespace_regex = group_name_split[3]

        destinations = get_all_destinations()
        namespaces = next((item['resources'] for item in destinations if item['name'] == resource), None)

        # Use regex to match names starting with namespace_regex
        pattern = re.compile(f'^{namespace_regex}.*')

        # Filter the list using the regex pattern
        filtered_namespaces = [name for name in namespaces if pattern.match(name)]

        # Extract the part after the dot in each element of group_grants_namespace
        group_grants_namespace = [item.split('.', 1)[1] for item in group_grants]

        # Find items in group_grants_namespace that are not in filtered_namespaces
        grants_delete = [item for item in group_grants_namespace if item not in filtered_namespaces]

        # Find items in filtered_namespaces that are not in group_grants_namespace
        grants_create = [item for item in filtered_namespaces if item not in group_grants_namespace]

        grantsToAdd=[]
        grantsToRemove=[]
        for namespace in grants_create:
            if namespace not in group_grants:
                resource_namespace = resource + "." + namespace
                grantsToAdd.append({
                    "groupName": group["name"],
                    "privilege": role,
                    "resource": resource_namespace
                })
        
        for namespace in grants_delete:
            resource_namespace = resource + "." + namespace
            grantsToRemove.append({
                    "groupName": group["name"],
                    "privilege": role,
                    "resource": resource_namespace
                })

        patch_user_grants(grantsToAdd,grantsToRemove)

def main():
    """Main Function"""
    # Define the path to the managed user ssh grants file
    managed_grants_path = f"{os.getenv('HOME')}/.propagate-grants.json"

    # Ensure the managed grants file exists
    Path(managed_grants_path).touch(exist_ok=True)

    # Get the saved managed grants
    with open(managed_grants_path, mode="r", encoding="utf-8") as fd:
        managed_grants = {}
        try:
            managed_grants = json.load(fd)
        except:
            pass

    # Propagate the grants
    sync_user_grants(managed_grants)

    # Save the managed grants
    with open(managed_grants_path, mode="w", encoding="utf-8") as fd:
        json.dump(managed_grants, fd)


    # Define the path to the managed group grants file
    managed_groups_grants_path = f"{os.getenv('HOME')}/.azure-group-grants.json"

     # Ensure the managed group grants file exists
    Path(managed_groups_grants_path).touch(exist_ok=True)
    
    # Get the saved managed grants
    with open(managed_groups_grants_path, mode="r", encoding="utf-8") as fd:
        verified_groups = {}
        try:
            verified_groups = json.load(fd)
        except:
            pass

    all_groups = infra_api_get("groups")

    filtered_groups = []

    # Regex pattern to match the new azureAD group schema
    pattern = re.compile(r'-(?:' + '|'.join(re.escape(key) for key in k8s_destinations_link.keys()) + r')\b', re.IGNORECASE)


    # Filtering groups
    for group in all_groups:
        if pattern.search(group['name']):
            filtered_groups.append(group)

    wildcard_groups = [item for item in filtered_groups if '*' in item['name']]
    manage_wildcard_groups(wildcard_groups)
    
    # remove wildcard groups
    filtered_groups = [item for item in filtered_groups if '*' not in item['name']]

    for group in filtered_groups:
        created_group_time = group["created"]
        updated_group_time=datetime.fromisoformat(created_group_time.replace('Z', '+00:00'))
        if group["name"] in verified_groups: 
            last_verified = datetime.fromisoformat(verified_groups[group["name"]]["verified_date"].replace('Z', '+00:00'))
            if updated_group_time > last_verified:
                parse_group_access(group["name"])
                verified_groups[group["name"]]= {
                    "verified_date" : created_group_time
                }
        else:
            parse_group_access(group["name"])  
            verified_groups[group["name"]]= {
                    "verified_date" : created_group_time
                }
            
    # Save the managed grants
    with open(managed_groups_grants_path, mode="w", encoding="utf-8") as fd:
        json.dump(verified_groups, fd)


if __name__ == "__main__":
    main()

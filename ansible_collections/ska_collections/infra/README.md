
# Infra

Infra brings authentication and access control to servers, clusters, and databases. [link](https://infrahq.com/)

## AWS

If you want to install in aws you can do the following commands

```shell
aws-vault list

aws-vault login CREDENTIAL

asw-vault exec CREDENTIAL
```

## Pre-install

Make sure you to look the properties before deploying.

The secrets should only be created **once**.
If you generate the secrests using an existing infra database, the infra-server won't be able to connect to that database.
Only set it to true if you have a new database.

```yaml
infra_server_generate_secrets: false
```

Make sure you fill this parameters:

```yaml
infra_server_db_host: "{{ _ | default_to_env('INFRA_SERVER_DB_HOST') }}"
infra_server_db_password: "{{ _ | default_to_env('INFRA_SERVER_DB_PASSWORD') }}"
```

## Install

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks infra install
```

## Destroy

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks infra destroy
```

The /etc/infrahq folder does not get destroyed. If need to deleted it you must do it manualy.

## Local DB

For testing you can deploy a local database.

Change the **infra_local_db_install** to **true** and make sure the **infra_server_db_host** is pointing to the local db.


# Infra CLI Client
To run the make targets for the clients you need to install localy the infra client:

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks infra client-install
```

# Infra SSH Client

To deploy the SSH client agent on a host to make it a possible destination in the infra server, make sure the following parameters are defined:

```yaml
infra_client_access_key: "{{ _ | default_to_env('INFRA_CLIENT_ACCESS_KEY') }}"
```

And then deploy the SSH client agent using the following make target:

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks infra client-ssh-install
```

To destroy the SSH client agent, the following make target can be used:

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks infra client-ssh-destroy
```

# Infra K8s Client

To connect a k8s cluster run:

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks infra client-k8s-install
```

To destroy a k8s cluster run:

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks infra client-k8s-destroy
```

# Infra Propagator

The infra propagator is a pythons script that is executed periodically to propagate the ground grants down to each user.
This is necessary as the SSH implementation is not complete and the client is not aware of the group grants.

It automatically adds missing grants to each user by checking the groups they are in and their grants.
If a user is removed from a group, the script will automatically remove the grants it added (NOTE: any grants that were added to the user outside of the script are not removed and must be manually removed if necessary!)

To deploy the script, run:

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks infra propagator
```

This script only needs to be executed from a single machine, preferably the machine running the infra server.

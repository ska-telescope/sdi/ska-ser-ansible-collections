#!/bin/bash
function log(){
    echo "$(date -u +"%Y-%m-%dT%H:%M:%SZ") | $@"
}

function dataplaneapi(){
    curl -s -u${DATAPLANE_API_AUTH} -H "Content-Type: application/json" $@
}

function add(){ # args: node_name node_ip
    log "Adding server '${1}' in backend '${HAPROXY_BACKEND}' with IP '${2}'"
    dataplaneapi \
    --fail \
    -X POST \
    "${DATAPLANE_API_URL}/services/haproxy/configuration/backends/${HAPROXY_BACKEND}/servers?transaction_id=${TRANSACTION_ID}" \
    --data-raw "{\"name\":\"${1}\",\"address\":\"${2}\",\"port\":6443,\"check\":\"enabled\",\"verify\":\"none\"}" > /dev/null
}

function remove(){ # args: node_name
    log "Removing server '${1}' in backend '${HAPROXY_BACKEND}'"
    dataplaneapi \
    --fail \
    -X DELETE \
    "${DATAPLANE_API_URL}/services/haproxy/configuration/backends/${HAPROXY_BACKEND}/servers/${1}?transaction_id=${TRANSACTION_ID}" > /dev/null
}

CONFIG_VERSION=$(dataplaneapi --fail "${DATAPLANE_API_URL}/services/haproxy/configuration/version")
log "Current HAProxy config version: ${CONFIG_VERSION}"

# Checking status.nodeRef as that allows for failed instances (ie, after restore velero backup) to be considered, as nodeRef
# is only present in instances that actually joined the cluster
CONTROL_PLANES=$( \
    (kubectl get machines -n ${CAPI_NAMESPACE} -l cluster.x-k8s.io/control-plane-name=${CAPI_CLUSTER_NAME}-control-plane -o json 2>/dev/null || echo '{"items":[]}') | \
    jq -c '[.items[] | select(.status.infrastructureReady==true)]' \
)
NUM_CONTROL_PLANES=$(echo "${CONTROL_PLANES}" | jq '. | length')
log "Found ${NUM_CONTROL_PLANES} ControlPlane instances"

CONFIGURED_CONTROL_PLANES=$( \
    dataplaneapi "${DATAPLANE_API_URL}/services/haproxy/configuration/backends/${HAPROXY_BACKEND}/servers" | \
    jq -c '.' \
)
NUM_CONFIGURED_CONTROL_PLANES=$(echo "${CONFIGURED_CONTROL_PLANES}" | jq '. | length')
log "Currently ${NUM_CONFIGURED_CONTROL_PLANES} backend servers are configured"
if [ $NUM_CONTROL_PLANES -eq 0 ] && [ $NUM_CONFIGURED_CONTROL_PLANES -eq 0 ]; then
    log "Nothing to update"
    exit 0
fi

COMMIT_TRANSACTION=1 # false
TRANSACTION_ID=$(dataplaneapi --fail -X POST "${DATAPLANE_API_URL}/services/haproxy/transactions?version=$CONFIG_VERSION" | jq -r .id)
log "Opening configuration transaction: ${TRANSACTION_ID}"

for CP in $(echo $CONTROL_PLANES | jq -r '.[].spec.infrastructureRef.name'); do
    if [ $(echo "$CONFIGURED_CONTROL_PLANES" | grep ${CP} | wc -l) -eq 0 ]; then
        add ${CP} $(echo $CONTROL_PLANES | jq -r --arg cp "${CP}" '.[] | select(.spec.infrastructureRef.name == $cp) | .status.addresses[] | select(.type == "InternalIP") | .address')
        COMMIT_TRANSACTION=0
    else
        log "Server ${CP} already configured"
    fi
done

for CP in $(echo $CONFIGURED_CONTROL_PLANES | jq -r '.[].name'); do
    if [ $(echo "$CONTROL_PLANES" | grep ${CP} | wc -l) -eq 0 ]; then
        remove ${CP}
        COMMIT_TRANSACTION=0
    fi
done

if [ $COMMIT_TRANSACTION -eq 0 ]; then
    log "Comitting transaction: ${TRANSACTION_ID}"
    TRANSACTION_STATUS=$(dataplaneapi -X PUT "${DATAPLANE_API_URL}/services/haproxy/transactions/${TRANSACTION_ID}" | jq -r .status)
    if [ "$TRANSACTION_STATUS" = "success" ]; then
        log "Transaction '${TRANSACTION_ID}' successful"
    else
        log "Transaction '${TRANSACTION_ID}' failed"
        exit 1
    fi
else
    log "Discarding transaction: ${TRANSACTION_ID}"
    TRANSACTION_STATUS=$(dataplaneapi -X DELETE "${DATAPLANE_API_URL}/services/haproxy/transactions/${TRANSACTION_ID}")
fi

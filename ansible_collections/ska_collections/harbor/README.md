# SKA Harbor Ansible Collection

This collection deploys and configures Harbor - an open-source container image registry - to have an administration account as well as a robot account (to be used for every automation task).

This collection also configures a production project in Harbor where a vulnerability scan is automatically executed every time an OCI image is pushed to it. Adding to this, it is also configured to not allow an image to be pulled if it is not signed or contains any critical vulnerability identified by the vulnerability scan executed upon its push.

# Collection Usage

| Targets | Description | Usage |
| ------- | ----------- | ----- |
| install | Deploy and configure Harbor | make playbooks harbor install PLAYBOOKS_HOSTS=harbor |
| destroy | Uninstall Harbor | make playbooks harbor destroy PLAYBOOKS_HOSTS=harbor |
| test    | Run tests to verify a successful Harbor deployment | make playbooks harbor test PLAYBOOKS_HOSTS=harbor |

Note: If you are deploying Harbor on a clean AWS instance, docker and pip need to be installed *before running the deployment playbook* by running:
```
make playbooks oci docker PLAYBOOKS_HOSTS=harbor
```

# Harbor Usage Examples

Assuming you've installed and configured Harbor and that you have a Dockerfile in the current folder, here's a quick example of how to
* Push an image to Harbor:
    ```
    docker login -u <username> -p <password> harbor.skao.int
    docker build -t harbor.skao.int/production/<image>:<tag> .
    docker push harbor.skao.int/production/<image>:<tag>
    ```
* Pulling an image from Harbor:
    ```
    docker pull harbor.skao.int/production/<image>:<tag>
    ```

The username and password for Harbor can be found in [Vault](https://vault.skao.int/ui/vault/secrets/ska-ser-infra-machinery/kv/aws-eu-west-2%2Fproduction%2Fharbor)
# Kyverno: Policy Agent

[Kyverno](https://kyverno.io/docs/) is an open-source policy agent that helps you enforce Kubernetes best-practices and implement custom behaviors via Mutating and Validating webhooks, as well as the capability of generating new resources and manage their lifecycle.

# Features

- Validation: Validate resources submitted to the Kubernetes API and block unwanted configurations.
- Mutation: Murate resources submitted to the API to enforce best-practice configurations (i.e., resource requests) or implement custom behaviors
- Generation: Generate resources when other resources are submitted to the API.
- Image Validation: Validate image signatures and vulnerability scannings
- External Data Sources: Get data from external data sources when ruling submitted resources.

## What's included

This role installs Kyverno to the Kubernetes cluster and also deploys policies. The policies are searched in the list of directories provided by `k8s_kyverno_policy_dirs` (concatenation of `k8s_kyverno_default_policy_dirs` and `k8s_kyverno_extra_policy_dirs`). If you want to provide other directories, simply set `k8s_kyverno_extra_policy_dirs`. Also, by default, all policies in the directories are applied.

To **exclude** some policies from being applied, set `k8s_kyverno_exclude_policies`. This supports both substring matching and regex. A few examples:

```
k8s_kyverno_exclude_policies:
  - "some-rule.yml" # It will ignore all files named some-rule.yml
  - ".*/image-validation/[0-9]+.*" # It will ignore all rules in directory image-validation that start with a number
```

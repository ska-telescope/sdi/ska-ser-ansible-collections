---
- name: Install Vault Operator Helm chart
  kubernetes.core.helm:
    wait: true
    timeout: "{{ k8s_vault_timeout }}"
    chart_repo_url: https://helm.releases.hashicorp.com
    chart_ref: vault-secrets-operator
    chart_version: "{{ k8s_vault_operator_chart_version }}"
    create_namespace: true
    namespace: "{{ k8s_vault_operator_namespace }}"
    name: "{{ k8s_vault_operator_release_name }}"
    values: "{{ k8s_vault_operator_config }}"

- name: Create Vault auths
  kubernetes.core.k8s:
    state: present
    definition: "{{ lookup('template', 'vault_auth.yml.j2') | from_yaml }}"
  with_items: "{{ k8s_vault_operator_extra_auths }}"
  loop_control:
    label: "{{ item.name }}: {{ item.role }}"

- name: Gather all VaultAuth objects
  kubernetes.core.k8s_info:
    api_version: secrets.hashicorp.com/v1beta1
    kind: VaultAuth
  register: vault_auth_objects

- name: Set vault auths to create
  ansible.builtin.set_fact:
    vault_auths_to_create: "{{ [(item.namespace | default(k8s_vault_operator_namespace)) + '/' + item.name] + (vault_auths_to_create | default([])) }}"
  with_items: "{{ k8s_vault_operator_extra_auths }}"
  loop_control:
    label: "{{ item.namespace | default(k8s_vault_operator_namespace) }}/{{ item.name }}"

- name: Delete undefined managed VaultAuths
  when: >-
    (
      (
        (item.metadata.labels["app.kubernetes.io/managed-by"] == "Ansible") and
        (item.metadata.namespace + "/" + item.metadata.name) not in vault_auths_to_create
      ) |
      default(false)
    )
  kubernetes.core.k8s:
    api_version: secrets.hashicorp.com/v1beta1
    kind: VaultAuth
    name: "{{ item.metadata.name }}"
    namespace: "{{ item.metadata.namespace }}"
    state: absent
  with_items: "{{ vault_auth_objects.resources }}"
  loop_control:
    label: "{{ item.metadata.namespace }}/{{ item.metadata.name }}"

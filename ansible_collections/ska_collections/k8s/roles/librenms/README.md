# Librenms: Network Monitoring System

[Librenms](https://docs.librenms.org/) is an open-source Network Management System.

## What's included

This role installs LibreNMS to the Kubernetes cluster.

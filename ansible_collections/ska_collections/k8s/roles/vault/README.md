# K8s Collection - Vault Role - ska_collections.k8s.vault

The purpose of this role is to automate the installation and configuration options for Vault.

Currently, the automated operations available are:
- Deploying Vault to a K8s cluster
- Fetching all Gitlab Groups under the SKA Developers subgroup and creating those as teams in Vault
- Adding extra / Updating policies for a specific team in Vault

## Required and Optional Variables

| Name | Description | Required for Vault Deployment Role | Required for creating Vault Teams Role | Required for updating a Vault Team Role |
| ---- | ----------- | ---------------------------------- | -------------------------------------- | ----------------------------------------|
| GITLAB_ACCESS_TOKEN | a personal access token with permissions for user and repo management | N/A | Yes | Yes |
| VAULT_TEAM_NAME | name of the team to be updated <br>[**WARNING:** needs to match the team's short path in Gitlab] | N/A | No | Yes |

## Available Targets
### Vault Management

- **Deploy Vault** in the k8s cluster

    ```bash
    make playbooks k8s install TAGS=vault PLAYBOOKS_HOSTS=<host>
    ```

- **Add teams to Vault** based on all the SKA Developers groups under the SKAO's Gitlab group. 

    ```bash
    make playbooks k8s vault-add-teams PLAYBOOKS_HOSTS=<host>
    ```

- **Update the Vault policies for a specific team** 

    ```bash
    make playbooks k8s vault-update-team VAULT_TEAM_NAME=<team name> PLAYBOOKS_HOSTS=<host>
    ```

Each of the existing Vault teams will be under ``dev/<team name>`` and these have permissions (set by the variable ``k8s_vault_auth_default_group_policy_permissions``) to manage all the secrets within their team as they see fit. To add extra policies to a team, the variable ``k8s_vault_auth_extra_group_policy_permissions`` should be used.

**IMPORTANT:** All the previous actions require the execution of the set-env script to fetch the required secrets
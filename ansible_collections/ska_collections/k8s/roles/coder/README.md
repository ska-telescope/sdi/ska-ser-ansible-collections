# Coder

This role installs the Coder application which is composed by an instance of PostgreSql and the Coder itself. The coder service, available as k8s load balancer, is an http application where user can create their own development environment which in this role is a k8s pod with one persistant volume.

## Installation

Make sure to have a password for postgresql available in the `PrivateRules.mak` as `K8S_CODER_POSTGRESQL_PASSWORD`.

Install Coder from the ska-infra-machinery repository with:

```bash
export KUBECONFIG=/path-to-kube/config
PLAYBOOKS_HOSTS=localhost make playbooks k8s install TAGS=coder
```

## Configuration

Once code is installed, you need to configure a template. Go to the service load balancer http endpoint, i.e. `http://coder.coder.svc.cluster.local/cli-auth`, and paste the token value in the `PrivateRules.mak` as `K8S_CODER_CLI_AUTH`.
Configure coder with:

```bash
export KUBECONFIG=/path-to-kube/config
PLAYBOOKS_HOSTS=localhost make playbooks k8s install TAGS=coder
```

### Templates

Coder uses templates to define workspace configurations. Templates are written in Terraform and specify the infrastructure and environment setup for developer workspaces.

The role includes two default templates:

- `kubernetes.j2` - A basic template that provisions a Kubernetes pod with code-server and JupyterLab
- `cicd_workshop.tf.j2` - An extended template with additional tools for CI/CD training

Key aspects of templates:

1. **Template Configuration**
   - Templates are configured via the `configure_job.yaml.j2` job
   - The job creates/updates templates in Coder using the template files
   - Each template gets a unique version based on timestamp
   - TODO: Templates can be customized with display name, description and icon

To create a new template:

1. Create a new .tf.j2 file in `templates/coder-templates/`
2. Define the Terraform configuration for workspace resources
3. Run the playbook to deploy the template as above:

```bash
export KUBECONFIG=/path-to-kube/config
PLAYBOOKS_HOSTS=localhost make playbooks k8s install TAGS=coder
```

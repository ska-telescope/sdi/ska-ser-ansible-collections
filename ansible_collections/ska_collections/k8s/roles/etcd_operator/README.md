# ETCD aenix operator role

This will install the etcd Aenix operator on a cluster. It will install:
* The operator if the operator namespace is *not* available.
* An etcd deployment with one or more nodes

The variables file shoud have at least:
```
in_etcd_namespace: the namespace where to install the etcd
in_etcd_storagesize: the size of the storage
in_etcd_storageclass: the storage class (bds1, ceph-cephfs, ...)
```
Variables with defaults:
```
in_etcd_name: name of the deployment, default is etcd
in_etcd_instances: number of etcd nodes, default is 1
in_etcd_operator_namespace: the namespace where to install the etcd operator, default is etcd-operator-system
```
Other options to be aware of:
```
in_etcd_dcoverride: override the datacenter name, EG: stfc instead of stfc-techops
```

# Documentation of the operator

You can find it on the operator's [homepage](https://etcd.aenix.io/).

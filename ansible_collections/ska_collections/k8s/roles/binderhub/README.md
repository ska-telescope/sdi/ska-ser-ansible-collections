# BinderHub

This role allows you to install Binderhub and Jupyterhub in Kubernetes. Binderhub acts as an image-builder service, that simplifies building images to run as Jupyterhub sessions, to run Jupyter notebooks. The images deployed by this role are SKAO custom built images and charts tailored to the currently identified project's needs.

- Binderhub - https://gitlab.com/ska-telescope/sdi/ska-binderhub
- Jupyterhub - https://gitlab.com/ska-telescope/sdi/ska-binderhub

## Requirements

- Kubernetes cluster
- Management machine with the cluster credentials (path specified by `k8s_kubeconfig`)

## Role Variables

The following variables can be customized to configure the BinderHub deployment:

### Chart, Release and Namespace

- `k8s_binderhub_release`: Name of the release
- `k8s_binderhub_namespace`: Namespace to install the release
- `k8s_binderhub_helm_chart`: Chart to install
- `k8s_binderhub_helm_chart_repo`: URL of the chart repository
- `k8s_binderhub_helm_chart_version`: Chart version to install
- `k8s_binderhub_image`: Configure Binderhub's image name, tag and pullPolicy. Defaults to `k8s_binderhub_default_image`
- `k8s_binderhub_jupyterhub_images`: Configure Jupyterhub's images' name, tag and pullPolicy. Defaults to `k8s_binderhub_jupyterhub_default_images`

### OCI Image Cache

- `k8s_binderhub_transport`: Transport (http or https) to use for Binderhub
- `k8s_binderhub_oci_registry_host`: OCI registry hostname (with port if applicable)
- `k8s_binderhub_oci_registry_host_full_url`: OCI registry url
- `k8s_binderhub_oci_registry_username`: OCI registry username
- `k8s_binderhub_oci_registry_password`: OCI registry password

### Authentication

- `k8s_binderhub_singleuser`: Configure Jupyterhub as single user (no OAuth integration)
- `k8s_binderhub_azuread_enabled`: If not single user, if using AzureAD over GitlabAD
- `k8s_binderhub_callback_url`: Binderhub OAuth callback url
- `k8s_binderhub_jupyterhub_callback_url`: Jupyterhub OAuth callback url
- `k8s_binderhub_azuread_client_id`: AzureAD Client ID
- `k8s_binderhub_azuread_client_secret`: AzureAD Client Secret
- `k8s_binderhub_azuread_tenant_id`: AzureAD Tenant ID
- `k8s_binderhub_azuread_auth_roles`: AzureAD list of allowed to authenticate roles
- `k8s_binderhub_gitlab_client_id`: GitlabAD Client ID
- `k8s_binderhub_gitlab_client_secret`: GitlabAD Client Secret
- `k8s_binderhub_gitlab_allowed_groups`: Gitlab group slugs allowed to login

### Configuration and Profiles

- `k8s_binderhub_jupyterhub_named_server_limit_per_user`: Number of named servers per user
- `k8s_binderhub_jupyterhub_volumes`: List of volumes to be attached by default to all Jupyterhub session pods. Example:

```
k8s_binderhub_jupyterhub_volumes:
  - name: some-volume
    persistentVolumeClaim:
      claimName: some-claim
    mountPath: /some-path
```

- `k8s_binderhub_singleuser_resources`: Jupyterhub "session" pod's configurations, defaults to `k8s_binderhub_singleuser_default_resources`
- `k8s_binderhub_jupyterhub_profiles`: List of configured profiles

> Note: Users cannot create profiles, those need to be configured and deployed by a cluster admin to be available

Jupyterhub allows you to specify profiles that can be selected to create a particular session, making it possible to use different computational requests (e.g., GPU) and configurations. Looking into the [official documentation](https://z2jh.jupyter.org/en/stable/resources/reference.html#singleuser-profilelist) it should be noted that these configurations are passthrough to [Kubespawner](https://jupyterhub-kubespawner.readthedocs.io/en/latest/spawner.html#kubespawner.KubeSpawner), being this where we can find which settings can be set per profile. For settings that are not mentioned in the documentation we can still use the `extra_pod_config` field to set arbitrary pod spec fields.

As an example, to allow GPU nodes, we must set the pod's `runtimeClassName` and that is only achievable by adding that to `extra_pod_config`:

```
k8s_binderhub_jupyterhub_profiles:
  - display_name: GPU
    description: GPU profile
    kubespawner_override:
      ...
      node_selector:
        "nvidia.com/gpu": "true"
      tolerations:
        - key: "nvidia.com/gpu"
          value: "true"
          effect: "NoSchedule"
      extra_resource_guarantees:
        "nvidia.com/gpu": 1
      extra_resource_limits:
        "nvidia.com/gpu": 1
      extra_pod_config:
        runtimeClassName: "nvidia"
```

## Dependencies

This role has no external dependencies.

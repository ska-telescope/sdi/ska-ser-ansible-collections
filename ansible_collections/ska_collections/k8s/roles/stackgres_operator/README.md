# Stackgres operator role

This will install the Stackgres operator on a cluster. It will install:
* A PostgreSQL cluster (a server, not a multiple servers)
If you install more than 1 replica you will have a primary and a secondary on two containers.

The variables file shoud have at least:
```
in_stgop_namespace: the namespace where to install the MariaDB
in_stgop_pgpassword: the postgres password
in_stgop_secret: the secret that will store that password
in_stgop_storagesize: the size of the storage
in_stgop_storageclass: the storage class (bds1, ceph-cephfs, ...)
in_stgop_dbname: name of the database
```
Passwords should be read from vault.
Other options to be aware of:
```
in_stgop_service_account: service account to be use by the CSI driver
in_stgop_vault_role: role to be use with vault
in_stgop_dcoverride: override the datacenter name, EG: stfc instead of stfc-techops
```
By default there will be no wal stream to an S3 bucket. You should backup your database manually or enable a backup policy.

# Storage

You could manually create the storage with this format for a database called eda and with one node:
```
eda-data-eda-0: [DB]-data-[DB]-[node]
```
If a volume is not created before installing this, it will make one from the k8s_stgop_storageclass storageClass.

# Documentation of the operator

You can find it on the operator's [homepage](https://stackgres.io/).

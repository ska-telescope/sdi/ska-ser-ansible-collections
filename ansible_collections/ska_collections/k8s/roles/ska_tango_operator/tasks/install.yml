---

- name: Create certificates for client communication
  become: true
  ansible.builtin.import_role:
    name: ska_collections.instance_common.certs
    tasks_from: sign-cert
  vars:
    certificate_name: "{{ k8s_operator_ping_svc_name }}"
    certificate_dns_alt_name: "{{ k8s_operator_ping_svc_name }}.{{ k8s_operator_namespace }}.svc"
    certificate_recreate: true

- name: Read certificates tls.key # noqa: command-instead-of-module
  become: true
  ansible.builtin.shell: |
    set -o pipefail && \
    cat {{ certificates_dir }}/{{ k8s_operator_ping_svc_name }}.key
  args:
    executable: /usr/bin/bash
  changed_when: false
  register: ska_tango_ping_key

- name: Read certificates tls.crt # noqa: command-instead-of-module
  become: true
  ansible.builtin.shell: |
    set -o pipefail && \
    cat {{ certificates_dir }}/{{ k8s_operator_ping_svc_name }}.crt
  args:
    executable: /usr/bin/bash
  changed_when: false
  register: ska_tango_ping_crt

- name: Create operator namesapce
  kubernetes.core.k8s:
    kind: Namespace
    name: "{{ k8s_operator_namespace }}"

- name: Add secrets for server certificates in cluster
  kubernetes.core.k8s:
    state: present
    template: 'server-cert-secrets.yml.j2'

- name: Install ska-tango-operator dependency cert-manager
  kubernetes.core.helm:
    atomic: true
    chart_repo_url: "{{ k8s_operator_cert_manager_chart_url }}"
    chart_ref: "{{ k8s_operator_cert_manager_chart_reference }}"
    chart_version: "{{ k8s_operator_cert_manager_chart_version }}"
    create_namespace: true
    namespace: "{{ k8s_operator_namespace }}"
    name: "{{ k8s_operator_cert_manager_release_name }}"
    wait: true
    values:
      crds:
        enabled: "{{ k8s_operator_cert_manager_enable_crds }}"

- name: Install the SKA Tango Operator
  kubernetes.core.helm:
    atomic: true
    chart_repo_url: "{{ k8s_operator_chart_url }}"
    chart_ref: "{{ k8s_operator_chart_reference }}"
    chart_version: "{{ k8s_operator_chart_version }}"
    create_namespace: true
    namespace: "{{ k8s_operator_namespace }}"
    name: "{{ k8s_operator_release_name }}"
    wait: true
    values:
      cert-manager:
        enabled: false
      ska-tango-ping:
        replicaCount: "{{ k8s_operator_ping_replicas }}"
        apiKeySecret:
          secretProviderEnabled: "{{ k8s_operator_ping_secret_provider_enabled }}"
          secretPath: "{{ k8s_operator_ping_secret_path }}"
          secretKey: "{{ k8s_operator_ping_secret_key }}"
          secretMount: "{{ k8s_operator_ping_secret_mount }}"
          value: "{{ k8s_operator_ping_secret }}"
        service:
          tls:
            secretName: "{{ k8s_operator_server_cert_secret_name }}"
      kubernetesClusterDomain: "{{ k8s_cluster_domain }}"
      controllerManager:
        replicas: "{{ k8s_operator_replicas }}"
        manager:
          image:
            repository: "{{ k8s_operator_manager_image_repository }}"
            tag: "{{ k8s_operator_manager_image_tag }}"
          resources:
            limits:
              cpu: "{{ k8s_operator_resources_limits_cpu }}"
              memory: "{{ k8s_operator_resources_limits_memory }}"
            requests:
              cpu: "{{ k8s_operator_resources_requests_cpu }}"
              memory: "{{ k8s_operator_resources_requests_memory }}"
      operatorConfig:
        pingSvcUrl: "{{ k8s_operator_release_name }}-ska-tango-ping:8000"

# CNPG operator role

This will install the MariaDB operator on a cluster. It will install:
* A PostgreSQL cluster (a server, not a multiple servers)
If you install more than 1 replica you will have a primary and a secondary on two containers.

The variables file shoud have at least:
```
in_cnpg_namespace: the namespace where to install the MariaDB
in_cnpg_pgpassword: the postgres password
in_cnpg_secret: the secret that will store that password
in_cnpg_storagesize: the size of the storage
in_cnpg_storageclass: the storage class (bds1, ceph-cephfs, ...)
in_cnpg_dbname: name of the database
```
Passwords should be read from vault.
Other options to be aware of:
```
in_cnpg_service_account: service account to be use by the CSI driver
in_cnpg_vault_role: role to be use with vault
in_cnpg_dcoverride: override the datacenter name, EG: stfc instead of stfc-techops
```
By default there will be no wal stream to an S3 bucket. You should backup your database manually or enable a backup policy.

# Documentation of the operator

You can find it on the operator's [homepage](https://cloudnative-pg.io/).

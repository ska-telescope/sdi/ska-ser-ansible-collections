# Configuration

This playbook mainly uses two files to configure rook to work with a ceph cluster:

* k8s_rook_ceph_conf_ini_path
* k8s_rook_ceph_conf_key_ring_path


* k8s_rook_ceph_conf_ini
* k8s_rook_ceph_conf_key_ring

From these files, we draw most of the configurations. To alter default behavior, or not use the files at all, we can set the following variables:

* k8s_rook_ceph_fsid
* k8s_rook_ceph_admin_user_name
* k8s_rook_ceph_admin_secret
* k8s_rook_ceph_mon_data
* k8s_rook_ceph_monitors
* k8s_rook_rbd_pool_name

If defined, they take precedence over what is in the configuration and keyring files

## Rook Ceph Cluster Deployment

Ceph can also be deployed in Kubernetes through rook instead of using an existing external cluster. To do this, the storage nodes need to be joined to the Kubernetes cluster.

The ceph_cluster [tasks](tasks/ceph_cluster.yml) deploys the rook operation and cluster. To ensure no other workloads are scheduled on the storage nodes, a task that labels and taints the nodes is included. For it to work, the list of node names should be passed to the playbook using the following variable:

* k8s_rook_cluster_storage_nodes

If left empty, the ceph cluster will not use any nodes due to the default placement configuration, which requires the nodes to have the `role=storage-node` label (as defined in the `k8s_rook_cluster_placement` variable). The playbook will label the nodes accordingly by default.

For networking, the cluster is deployed using the host network by default. To configure the public and cluster networks to be used by the ceph cluster, the following variables should be defined:

* k8s_rook_cluster_public_cidrs
* k8s_rook_cluster_cluster_cidrs

In terms of pools, the cluster is created with a `volumes` block storage pool and a `cephfs-data` ceph filesystem by default, both with replication set to 3. These are configured using the following variables and can be optionally changed:

* k8s_rook_cluster_ceph_block_pools
* k8s_rook_cluster_ceph_filesystems

Finally, the following variables change what is deployed with the ceph cluster and can be optionally changed:

* k8s_rook_cluster_version  # The version of the rook-ceph-cluster chart
* k8s_rook_cluster_taint_storage_nodes  # Whether to taint the nodes to prevent other workloads to run on the storage nodes or not.
* k8s_rook_cluster_dashboard_enabled  # Enables the ceph dashboard using ceph modules
* k8s_rook_cluster_monitoring_enabled  # Enabled the monitoring stack using ceph modules
* k8s_rook_cluster_toolbox_enabled  # Deploys a toolbox pod that allows to run ceph commands against the cluster

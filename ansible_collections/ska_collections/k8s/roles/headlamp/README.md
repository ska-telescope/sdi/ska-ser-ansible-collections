## Headlamp

This role deploys [Headlamp](https://headlamp.dev/docs/latest) to a Kubernetes cluster. Headlamp allows to load multiple configs and essentially proxies requests to the various clusters' API to let users view and manage cluster resources.

### RBAC

Aside from the 'main' cluster, which Headlamp doesn't seem to allow to remove or make it an authless login, the users can add multiple clusters to it - authless or not.

This playbook provides *native* authless login to the cluster it is deployed to, by creating an **user** instead of just a service account. That allows us to make it the subject of multiple `RoleBinding` resources, giving us the ability to only give permissions to certain namespaces on the cluster.

Currently, we allow just one authless login for the main cluster with configurable permissions:

* `k8s_headlamp_local_cluster_config.cluster_wide_permissions` to set permissions given to all namespaces (defaults to listing and watching of most resources)
* `k8s_headlamp_local_cluster_config.namespaced_permissions` to set permissions given to certain namespaces (defaults to get/edit of most resources)

The cluster administrator now has to bind the "namespaced" `ClusterRole` to the user created by the playbook, named after `k8s_headlamp_local_cluster_config.name`.

That can be automatically done by using a [Kyverno](https://kyverno.io/docs/) policy like this:
```
apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: generate-ci-namespace-rolebindings
spec:
  background: true
  rules:
    - name: generate-ci-namespace-rolebindings
      match:
        resources:
          kinds:
            - Namespace
          operations:
          - CREATE
          - UPDATE
          namespaces:
            <list of namespaces>
      generate:
        apiVersion: rbac.authorization.k8s.io/v1
        kind: RoleBinding
        name: headlamp-authless-namespaced-role-binding
        namespace: "{{request.object.metadata.name}}"
        synchronize: true
        data:
          subjects:
            - kind: User
              name: <k8s_headlamp_local_cluster_config.name>
              apiGroup: rbac.authorization.k8s.io
          roleRef:
            kind: ClusterRole
            name: headlamp-authless-namespaced-role
            apiGroup: rbac.authorization.k8s.io
```

> **_NOTE:_**  To be able to do this, you need to give Kyverno's Background Controller the same permissions you mean to bind to the user, otherwise the API will not let you create the `RoleBinding`

### Configuring Authless Clusters

Other clusters can be added in an authless maner, by providing a Kubeconfig where the user credentials are properly defined and setting `strip_auth: false`:

```
k8s_headlamp_remote_clusters:
    - strip_auth: false
      kubeconfig: |
        apiVersion: v1
        clusters:
        - cluster:
            certificate-authority-data: LS0tLS1CRUdJ...
        ...
```

### Configuring Token-based Clusters

In the case were the user needs to provide proper authentication - normally by the means of a ServiceAccount token - we simply need to provide a kubeconfig without **users** defined or by not setting `strip_auth: false`, which will strip the users definition from the kubeconfig:

```
k8s_headlamp_remote_clusters:
    - kubeconfig: |
        apiVersion: v1
        clusters:
        - cluster:
            certificate-authority-data: LS0tLS1CRUdJ...
        ...
```

# MariaDB operator role

This will install the MariaDB operator on a cluster. It will install:
* A MariaDB server
* Create a tango user
* Create a tango database
* Optionally start a databaseds pod on that namespace 

The variables file shoud have at least:
```
in_mariadbop_namespace: the namespace where to install the MariaDB
in_mariadbop_rootpassword: the root password
in_mariadbop_password: the mariadb password
in_mariadbop_tangopassword: the tango password
in_mariadbop_databaseds: true/false enable or not the databaseds
```
And other options:
```
in_mariadbop_galera: "true"         # enables galera
in_mariadbop_dbname: "adb"          # name of the mariadb kubernetes object
in_mariadbop_name: "dbtobecreated"  # name of a new database to be created
```
Passwords should be read from vault.
Other options to be aware of:
```
in_mariadbop_service_account: service account to be use by the CSI driver
in_mariadbop_vault_role: role to be use with vault
in_mariadbop_kubeconfig: override default kubeconfig location
in_mariadbop_dcoverride: override the datacenter name, EG: stfc instead of stfc-techops
```

# Pre requisites
You have to create the new namespace.

The helm chart will be installed on its own mariadboperator namespace by default.

You could pre define those volumes:
* mariabackup: an nfss1 storage volume that will be shared
* storage-mariadb-X: a local storage for each MariaDB server starting from 0

If they don't exist, they will be created during the installation.

# Documentation of the operator

You can find it on their [github repository](https://github.com/mariadb-operator/mariadb-operator).

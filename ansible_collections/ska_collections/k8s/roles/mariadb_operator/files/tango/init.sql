-- MariaDB dump 10.19-11.2.3-MariaDB, for Linux (x86_64)
--
-- Host: 172.17.0.3    Database: tango
-- ------------------------------------------------------
-- Server version	11.0.2-MariaDB-1:11.0.2+maria~ubu2204

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_address`
--

DROP TABLE IF EXISTS `access_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_address` (
  `user` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `netmask` varchar(255) DEFAULT 'FF.FF.FF.FF',
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_address`
--

LOCK TABLES `access_address` WRITE;
/*!40000 ALTER TABLE `access_address` DISABLE KEYS */;
INSERT INTO `access_address` VALUES
('*','*.*.*.*','FF.FF.FF.FF','2006-08-24 13:12:21','2006-08-24 13:12:21');
/*!40000 ALTER TABLE `access_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `access_device`
--

DROP TABLE IF EXISTS `access_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_device` (
  `user` varchar(255) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `rights` varchar(255) DEFAULT NULL,
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_device`
--

LOCK TABLES `access_device` WRITE;
/*!40000 ALTER TABLE `access_device` DISABLE KEYS */;
INSERT INTO `access_device` VALUES
('*','*/*/*','write','2006-08-24 13:12:21','2006-08-24 13:12:21');
/*!40000 ALTER TABLE `access_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_alias`
--

DROP TABLE IF EXISTS `attribute_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_alias` (
  `alias` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `device` varchar(255) NOT NULL DEFAULT '',
  `attribute` varchar(255) NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_attribute_alias` (`alias`(64),`name`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_alias`
--

LOCK TABLES `attribute_alias` WRITE;
/*!40000 ALTER TABLE `attribute_alias` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_class`
--

DROP TABLE IF EXISTS `attribute_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_class` (
  `class` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_attribute_class` (`class`(64),`name`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_class`
--

LOCK TABLES `attribute_class` WRITE;
/*!40000 ALTER TABLE `attribute_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_attribute_history_id`
--

DROP TABLE IF EXISTS `class_attribute_history_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_attribute_history_id` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_attribute_history_id`
--

LOCK TABLES `class_attribute_history_id` WRITE;
/*!40000 ALTER TABLE `class_attribute_history_id` DISABLE KEYS */;
INSERT INTO `class_attribute_history_id` VALUES
(0);
/*!40000 ALTER TABLE `class_attribute_history_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_history_id`
--

DROP TABLE IF EXISTS `class_history_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_history_id` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_history_id`
--

LOCK TABLES `class_history_id` WRITE;
/*!40000 ALTER TABLE `class_history_id` DISABLE KEYS */;
INSERT INTO `class_history_id` VALUES
(0);
/*!40000 ALTER TABLE `class_history_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_pipe_history_id`
--

DROP TABLE IF EXISTS `class_pipe_history_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_pipe_history_id` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_pipe_history_id`
--

LOCK TABLES `class_pipe_history_id` WRITE;
/*!40000 ALTER TABLE `class_pipe_history_id` DISABLE KEYS */;
INSERT INTO `class_pipe_history_id` VALUES
(0);
/*!40000 ALTER TABLE `class_pipe_history_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `name` varchar(255) NOT NULL DEFAULT 'nada',
  `alias` varchar(255) DEFAULT NULL,
  `domain` varchar(85) NOT NULL DEFAULT 'nada',
  `family` varchar(85) NOT NULL DEFAULT 'nada',
  `member` varchar(85) NOT NULL DEFAULT 'nada',
  `exported` int(11) DEFAULT 0,
  `ior` text DEFAULT NULL,
  `host` varchar(255) NOT NULL DEFAULT 'nada',
  `server` varchar(255) NOT NULL DEFAULT 'nada',
  `pid` int(11) DEFAULT 0,
  `class` varchar(255) NOT NULL DEFAULT 'nada',
  `version` varchar(8) NOT NULL DEFAULT 'nada',
  `started` datetime DEFAULT NULL,
  `stopped` datetime DEFAULT NULL,
  `comment` text DEFAULT NULL,
  KEY `name` (`name`(64),`alias`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` VALUES
('sys/database/2',NULL,'sys','database','2',0,'nada','nada','DataBaseds/2',0,'DataBase','nada',NULL,NULL,'nada'),
('dserver/DataBaseds/2',NULL,'dserver','DataBaseds','2',0,'nada','nada','DataBaseds/2',0,'DServer','nada',NULL,NULL,'nada'),
('sys/rest/0',NULL,'sys','rest','0',0,'nada','nada','TangoRestServer/rest',0,'TangoRestServer','nada',NULL,NULL,'nada'),
('dserver/TangoRestServer/rest',NULL,'dserver','TangoRestServer','rest',0,'nada','nada','TangoRestServer/rest',0,'DServer','nada',NULL,NULL,'nada'),
('sys/tg_test/1',NULL,'sys','tg_test','1',0,'nada','nada','TangoTest/test',0,'TangoTest','nada',NULL,NULL,'nada'),
('dserver/TangoTest/test',NULL,'dserver','TangoTest','test',0,'nada','nada','TangoTest/test',0,'DServer','nada',NULL,NULL,'nada'),
('sys/access_control/1',NULL,'sys','access_control','1',0,'nada','nada','TangoAccessControl/1',0,'TangoAccessControl','nada',NULL,NULL,'nada'),
('dserver/TangoAccessControl/1',NULL,'dserver','TangoAccessControl','1',0,'nada','nada','TangoAccessControl/1',0,'DServer','nada',NULL,NULL,'nada');
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_attribute_history_id`
--

DROP TABLE IF EXISTS `device_attribute_history_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_attribute_history_id` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_attribute_history_id`
--

LOCK TABLES `device_attribute_history_id` WRITE;
/*!40000 ALTER TABLE `device_attribute_history_id` DISABLE KEYS */;
INSERT INTO `device_attribute_history_id` VALUES
(0);
/*!40000 ALTER TABLE `device_attribute_history_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_history_id`
--

DROP TABLE IF EXISTS `device_history_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_history_id` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_history_id`
--

LOCK TABLES `device_history_id` WRITE;
/*!40000 ALTER TABLE `device_history_id` DISABLE KEYS */;
INSERT INTO `device_history_id` VALUES
(0);
/*!40000 ALTER TABLE `device_history_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_pipe_history_id`
--

DROP TABLE IF EXISTS `device_pipe_history_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_pipe_history_id` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_pipe_history_id`
--

LOCK TABLES `device_pipe_history_id` WRITE;
/*!40000 ALTER TABLE `device_pipe_history_id` DISABLE KEYS */;
INSERT INTO `device_pipe_history_id` VALUES
(0);
/*!40000 ALTER TABLE `device_pipe_history_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `name` varchar(255) DEFAULT NULL,
  `exported` int(11) DEFAULT NULL,
  `ior` text DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `server` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `version` varchar(8) DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `stopped` datetime DEFAULT NULL,
  KEY `index_name` (`name`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_history_id`
--

DROP TABLE IF EXISTS `object_history_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_history_id` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_history_id`
--

LOCK TABLES `object_history_id` WRITE;
/*!40000 ALTER TABLE `object_history_id` DISABLE KEYS */;
INSERT INTO `object_history_id` VALUES
(0);
/*!40000 ALTER TABLE `object_history_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property` (
  `object` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `value` text DEFAULT NULL,
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_name` (`object`(64),`name`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property`
--

LOCK TABLES `property` WRITE;
/*!40000 ALTER TABLE `property` DISABLE KEYS */;
/*!40000 ALTER TABLE `property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_attribute_class`
--

DROP TABLE IF EXISTS `property_attribute_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_attribute_class` (
  `class` varchar(255) NOT NULL DEFAULT '',
  `attribute` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_property_attribute_class` (`class`(64),`attribute`(64),`name`(64),`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_attribute_class`
--

LOCK TABLES `property_attribute_class` WRITE;
/*!40000 ALTER TABLE `property_attribute_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_attribute_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_attribute_class_hist`
--

DROP TABLE IF EXISTS `property_attribute_class_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_attribute_class_hist` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL,
  `class` varchar(255) NOT NULL DEFAULT '',
  `attribute` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`,`count`),
  KEY `class_attribute_name_date` (`class`,`attribute`,`name`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_attribute_class_hist`
--

LOCK TABLES `property_attribute_class_hist` WRITE;
/*!40000 ALTER TABLE `property_attribute_class_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_attribute_class_hist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_attribute_device`
--

DROP TABLE IF EXISTS `property_attribute_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_attribute_device` (
  `device` varchar(255) NOT NULL DEFAULT '',
  `attribute` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_property_attribute_device` (`device`(64),`attribute`(64),`name`(64),`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_attribute_device`
--

LOCK TABLES `property_attribute_device` WRITE;
/*!40000 ALTER TABLE `property_attribute_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_attribute_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_attribute_device_hist`
--

DROP TABLE IF EXISTS `property_attribute_device_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_attribute_device_hist` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL,
  `device` varchar(255) NOT NULL DEFAULT '',
  `attribute` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`,`count`),
  KEY `device_attribute_name_date` (`device`,`attribute`,`name`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_attribute_device_hist`
--

LOCK TABLES `property_attribute_device_hist` WRITE;
/*!40000 ALTER TABLE `property_attribute_device_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_attribute_device_hist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_class`
--

DROP TABLE IF EXISTS `property_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_class` (
  `class` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_property` (`class`(64),`name`(64),`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_class`
--

LOCK TABLES `property_class` WRITE;
/*!40000 ALTER TABLE `property_class` DISABLE KEYS */;
INSERT INTO `property_class` VALUES
('Database','AllowedAccessCmd',1,'DbGetServerInfo','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',2,'DbGetServerNameList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',3,'DbGetInstanceNameList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',4,'DbGetDeviceServerClassList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',5,'DbGetDeviceList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',6,'DbGetDeviceDomainList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',7,'DbGetDeviceFamilyList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',8,'DbGetDeviceMemberList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',9,'DbGetClassList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',10,'DbGetDeviceAliasList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',11,'DbGetObjectList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',12,'DbGetPropertyList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',13,'DbGetProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',14,'DbGetClassPropertyList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',15,'DbGetClassProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',16,'DbGetDevicePropertyList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',17,'DbGetDeviceProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',18,'DbGetClassAttributeList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',19,'DbGetDeviceAttributeProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',20,'DbGetDeviceAttributeProperty2','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',21,'DbGetLoggingLevel','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',22,'DbGetAliasDevice','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',23,'DbGetClassForDevice','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',24,'DbGetClassInheritanceForDevice','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',25,'DbGetDataForServerCache','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',26,'DbInfo','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',27,'DbGetClassAttributeProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',28,'DbGetClassAttributeProperty2','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',29,'DbMysqlSelect','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',30,'DbGetDeviceInfo','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',31,'DbGetDeviceWideList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',32,'DbImportEvent','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',33,'DbGetDeviceAlias','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',34,'DbGetCSDbServerList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',35,'DbGetDeviceClassList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',36,'DbGetDeviceExportedList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',37,'DbGetHostServerList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',38,'DbGetAttributeAlias2','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',39,'DbGetAliasAttribute','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',40,'DbGetClassPipeProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',41,'DbGetDevicePipeProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',42,'DbGetClassPipeList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',43,'DbGetDevicePipeList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',44,'DbGetAttributeAliasList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Database','AllowedAccessCmd',45,'DbGetForwardedAttributeListForDevice','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',1,'QueryClass','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',2,'QueryDevice','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',3,'EventSubscriptionChange','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',4,'DevPollStatus','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',5,'GetLoggingLevel','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',6,'GetLoggingTarget','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',7,'QueryWizardDevProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',8,'QueryWizardClassProperty','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',9,'QuerySubDevice','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',10,'ZMQEventSubscriptionChange','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('DServer','AllowedAccessCmd',11,'EventConfirmSubscription','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Starter','AllowedAccessCmd',1,'DevReadLog','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Starter','AllowedAccessCmd',2,'DevStart','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Starter','AllowedAccessCmd',3,'DevGetRunningServers','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Starter','AllowedAccessCmd',4,'DevGetStopServers','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('Starter','AllowedAccessCmd',5,'UpdateServersInfo','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('TangoAccessControl','AllowedAccessCmd',1,'GetUsers','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('TangoAccessControl','AllowedAccessCmd',2,'GetAddressByUser','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('TangoAccessControl','AllowedAccessCmd',3,'GetDeviceByUser','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('TangoAccessControl','AllowedAccessCmd',4,'GetAccess','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('TangoAccessControl','AllowedAccessCmd',5,'GetAllowedCommands','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL),
('TangoAccessControl','AllowedAccessCmd',6,'GetAllowedCommandClassList','2024-02-19 14:13:30','2024-02-19 14:13:30',NULL);
/*!40000 ALTER TABLE `property_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_class_hist`
--

DROP TABLE IF EXISTS `property_class_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_class_hist` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL,
  `class` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`,`count`),
  KEY `class_name_date` (`class`,`name`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_class_hist`
--

LOCK TABLES `property_class_hist` WRITE;
/*!40000 ALTER TABLE `property_class_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_class_hist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_device`
--

DROP TABLE IF EXISTS `property_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_device` (
  `device` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `domain` varchar(255) NOT NULL DEFAULT '',
  `family` varchar(255) NOT NULL DEFAULT '',
  `member` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_resource` (`device`(64),`name`(64),`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_device`
--

LOCK TABLES `property_device` WRITE;
/*!40000 ALTER TABLE `property_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_device_hist`
--

DROP TABLE IF EXISTS `property_device_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_device_hist` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL,
  `device` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`,`count`),
  KEY `device_name_date` (`device`,`name`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_device_hist`
--

LOCK TABLES `property_device_hist` WRITE;
/*!40000 ALTER TABLE `property_device_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_device_hist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_hist`
--

DROP TABLE IF EXISTS `property_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_hist` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL,
  `object` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`,`count`),
  KEY `object_name_date` (`object`,`name`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_hist`
--

LOCK TABLES `property_hist` WRITE;
/*!40000 ALTER TABLE `property_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_hist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_pipe_class`
--

DROP TABLE IF EXISTS `property_pipe_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_pipe_class` (
  `class` varchar(255) NOT NULL DEFAULT '',
  `pipe` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_property_pipe_class` (`class`(64),`pipe`(64),`name`(64),`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_pipe_class`
--

LOCK TABLES `property_pipe_class` WRITE;
/*!40000 ALTER TABLE `property_pipe_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_pipe_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_pipe_class_hist`
--

DROP TABLE IF EXISTS `property_pipe_class_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_pipe_class_hist` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL,
  `class` varchar(255) NOT NULL DEFAULT '',
  `pipe` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`,`count`),
  KEY `class_pipe_name_date` (`class`,`pipe`,`name`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_pipe_class_hist`
--

LOCK TABLES `property_pipe_class_hist` WRITE;
/*!40000 ALTER TABLE `property_pipe_class_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_pipe_class_hist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_pipe_device`
--

DROP TABLE IF EXISTS `property_pipe_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_pipe_device` (
  `device` varchar(255) NOT NULL DEFAULT '',
  `pipe` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  `updated` timestamp NOT NULL,
  `accessed` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  `comment` text DEFAULT NULL,
  KEY `index_property_pipe_device` (`device`(64),`pipe`(64),`name`(64),`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_pipe_device`
--

LOCK TABLES `property_pipe_device` WRITE;
/*!40000 ALTER TABLE `property_pipe_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_pipe_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_pipe_device_hist`
--

DROP TABLE IF EXISTS `property_pipe_device_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_pipe_device_hist` (
  `id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL,
  `device` varchar(255) NOT NULL DEFAULT '',
  `pipe` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`,`count`),
  KEY `device_pipe_name_date` (`device`,`pipe`,`name`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_pipe_device_hist`
--

LOCK TABLES `property_pipe_device_hist` WRITE;
/*!40000 ALTER TABLE `property_pipe_device_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_pipe_device_hist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `server`
--

DROP TABLE IF EXISTS `server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `server` (
  `name` varchar(255) NOT NULL DEFAULT '',
  `host` varchar(255) NOT NULL DEFAULT '',
  `mode` int(11) DEFAULT 0,
  `level` int(11) DEFAULT 0,
  KEY `index_name` (`name`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `server`
--

LOCK TABLES `server` WRITE;
/*!40000 ALTER TABLE `server` DISABLE KEYS */;
INSERT INTO `server` VALUES
('tangoaccesscontrol/1','',0,0);
/*!40000 ALTER TABLE `server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tango'
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `class_att_prop` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `class_att_prop`(IN class_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_name,tmp_attribute VARCHAR(255);
	DECLARE known_att VARCHAR(255) DEFAULT '';
	DECLARE tmp_value TEXT;
	DECLARE tmp_count INT;
	DECLARE done,prop_nb,att_nb,prop_elt_nb INT DEFAULT 0;
	DECLARE class_name_pos,att_name_pos,prop_name_pos INT;
	
	DECLARE cur_class_att_prop CURSOR FOR 
	SELECT attribute,name,count,value 
	FROM tango.property_attribute_class 
	WHERE class = class_name ORDER BY attribute,name,count;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET res_str = CONCAT_WS(CHAR(0),res_str,class_name);
	SET class_name_pos = LENGTH(res_str);

	OPEN cur_class_att_prop;

	REPEAT
		FETCH cur_class_att_prop INTO tmp_attribute,tmp_name,tmp_count,tmp_value;
		IF NOT done THEN
		
			IF tmp_attribute != known_att THEN
				IF prop_nb != 0 THEN
					SET res_str = INSERT(res_str,att_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
					IF prop_nb < 10 THEN
						SET prop_name_pos = prop_name_pos + 2;
					ELSEIF prop_nb < 100 THEN
						SET prop_name_pos = prop_name_pos + 3;
					ELSE
						SET prop_name_pos = prop_name_pos + 4;
					END IF;
				END IF;
				SET known_att = tmp_attribute;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_attribute);
				SET att_name_pos = LENGTH(res_str);
				SET att_nb = att_nb + 1;
				SET prop_nb = 0;
			END IF;
			
			IF tmp_count = 1 THEN
				IF prop_elt_nb != 0 THEN
					SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
					IF prop_nb = 0 THEN
						IF prop_elt_nb < 10 THEN
							SET att_name_pos = att_name_pos + 2;
						ELSEIF prop_elt_nb < 100 THEN
							SET att_name_pos = att_name_pos + 3;
						ELSE
							SET att_name_pos = att_name_pos + 4;
						END IF;
					END IF;
				END IF;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_name);
				SET prop_name_pos = LENGTH(res_str);
				SET prop_nb = prop_nb + 1;
				SET prop_elt_nb = 0;
			END IF;
						
			SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_value);
			SET prop_elt_nb = prop_elt_nb + 1;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_class_att_prop;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,att_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
		IF prop_nb < 10 THEN
			SET prop_name_pos = prop_name_pos + 2;
		ELSEIF prop_nb < 100 THEN
			SET prop_name_pos = prop_name_pos + 3;
		ELSE
			SET prop_name_pos = prop_name_pos + 4;
		END IF;	
		IF prop_elt_nb != 0 THEN
			SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
		END IF;
	END IF;

	IF att_nb != 0 THEN	
		SET res_str = INSERT(res_str,class_name_pos+1,1,CONCAT_WS(CONCAT(att_nb),CHAR(0),CHAR(0)));
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,0);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `class_pipe_prop` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `class_pipe_prop`(IN class_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_name,tmp_pipe VARCHAR(255);
	DECLARE known_pipe VARCHAR(255) DEFAULT '';
	DECLARE tmp_value TEXT;
	DECLARE tmp_count INT;
	DECLARE done,prop_nb,pipe_nb,prop_elt_nb INT DEFAULT 0;
	DECLARE class_name_pos,pipe_name_pos,prop_name_pos INT;
	
	DECLARE cur_class_pipe_prop CURSOR FOR 
	SELECT pipe,name,count,value 
	FROM tango.property_pipe_class 
	WHERE class = class_name ORDER BY pipe,name,count;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET res_str = CONCAT_WS(CHAR(0),res_str,class_name);
	SET class_name_pos = LENGTH(res_str);

	OPEN cur_class_pipe_prop;

	REPEAT
		FETCH cur_class_pipe_prop INTO tmp_pipe,tmp_name,tmp_count,tmp_value;
		IF NOT done THEN
		
			IF tmp_pipe != known_pipe THEN
				IF prop_nb != 0 THEN
					SET res_str = INSERT(res_str,pipe_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
					IF prop_nb < 10 THEN
						SET prop_name_pos = prop_name_pos + 2;
					ELSEIF prop_nb < 100 THEN
						SET prop_name_pos = prop_name_pos + 3;
					ELSE
						SET prop_name_pos = prop_name_pos + 4;
					END IF;
				END IF;
				SET known_pipe = tmp_pipe;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_pipe);
				SET pipe_name_pos = LENGTH(res_str);
				SET pipe_nb = pipe_nb + 1;
				SET prop_nb = 0;
			END IF;
			
			IF tmp_count = 1 THEN
				IF prop_elt_nb != 0 THEN
					SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
					IF prop_nb = 0 THEN
						IF prop_elt_nb < 10 THEN
							SET pipe_name_pos = pipe_name_pos + 2;
						ELSEIF prop_elt_nb < 100 THEN
							SET pipe_name_pos = pipe_name_pos + 3;
						ELSE
							SET pipe_name_pos = pipe_name_pos + 4;
						END IF;
					END IF;
				END IF;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_name);
				SET prop_name_pos = LENGTH(res_str);
				SET prop_nb = prop_nb + 1;
				SET prop_elt_nb = 0;
			END IF;
						
			SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_value);
			SET prop_elt_nb = prop_elt_nb + 1;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_class_pipe_prop;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,pipe_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
		IF prop_nb < 10 THEN
			SET prop_name_pos = prop_name_pos + 2;
		ELSEIF prop_nb < 100 THEN
			SET prop_name_pos = prop_name_pos + 3;
		ELSE
			SET prop_name_pos = prop_name_pos + 4;
		END IF;	
		IF prop_elt_nb != 0 THEN
			SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
		END IF;
	END IF;

	IF pipe_nb != 0 THEN	
		SET res_str = INSERT(res_str,class_name_pos+1,1,CONCAT_WS(CONCAT(pipe_nb),CHAR(0),CHAR(0)));
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,0);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `class_prop` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `class_prop`(IN class_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_name VARCHAR(255);
	DECLARE tmp_value TEXT;
	DECLARE tmp_count INT;
	DECLARE done,prop_nb,prop_elt_nb INT DEFAULT 0;
	DECLARE class_name_pos,prop_name_pos INT;
	
	DECLARE cur_class CURSOR FOR 
	SELECT name,count,value 
	FROM tango.property_class 
	WHERE class = class_name ORDER BY name,count;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET res_str = CONCAT_WS(CHAR(0),res_str,class_name);
	SET class_name_pos = LENGTH(res_str);
	OPEN cur_class;

	REPEAT
		FETCH cur_class INTO tmp_name,tmp_count,tmp_value;
		IF NOT done THEN
		
			IF tmp_count = 1 THEN
				IF prop_elt_nb != 0 THEN
					SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
				END IF;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_name);
				SET prop_name_pos = LENGTH(res_str);
				SET prop_nb = prop_nb + 1;
				SET prop_elt_nb = 0;
			END IF;
						
			SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_value);
			SET prop_elt_nb = prop_elt_nb + 1;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_class;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
	END IF;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,class_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,0);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dev_att_prop` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `dev_att_prop`(IN dev_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_name,tmp_attribute VARCHAR(255);
	DECLARE known_att VARCHAR(255) DEFAULT '';
	DECLARE tmp_value TEXT;
	DECLARE tmp_count INT;
	DECLARE done,prop_nb,att_nb,prop_elt_nb INT DEFAULT 0;
	DECLARE dev_name_pos,att_name_pos,prop_name_pos INT;
	
	DECLARE cur_dev_att_prop CURSOR FOR 
	SELECT attribute,name,count,value 
	FROM tango.property_attribute_device 
	WHERE device = dev_name ORDER BY attribute,name,count;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET res_str = CONCAT_WS(CHAR(0),res_str,dev_name);
	SET dev_name_pos = LENGTH(res_str);

	OPEN cur_dev_att_prop;

	REPEAT
		FETCH cur_dev_att_prop INTO tmp_attribute,tmp_name,tmp_count,tmp_value;
		IF NOT done THEN
		
			IF tmp_attribute != known_att THEN
				IF prop_nb != 0 THEN
					SET res_str = INSERT(res_str,att_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
					IF prop_nb < 10 THEN
						SET prop_name_pos = prop_name_pos + 2;
					ELSEIF prop_nb < 100 THEN
						SET prop_name_pos = prop_name_pos + 3;
					ELSE
						SET prop_name_pos = prop_name_pos + 4;
					END IF;
				END IF;
				SET known_att = tmp_attribute;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_attribute);
				SET att_name_pos = LENGTH(res_str);
				SET att_nb = att_nb + 1;
				SET prop_nb = 0;
			END IF;
			
			IF tmp_count = 1 THEN
				IF prop_elt_nb != 0 THEN
					SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
					IF prop_nb = 0 THEN
						IF prop_elt_nb < 10 THEN
							SET att_name_pos = att_name_pos + 2;
						ELSEIF prop_elt_nb < 100 THEN
							SET att_name_pos = att_name_pos + 3;
						ELSE
							SET att_name_pos = att_name_pos + 4;
						END IF;
					END IF;
				END IF;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_name);
				SET prop_name_pos = LENGTH(res_str);
				SET prop_nb = prop_nb + 1;
				SET prop_elt_nb = 0;
			END IF;
						
			SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_value);
			SET prop_elt_nb = prop_elt_nb + 1;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_dev_att_prop;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,att_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
		IF prop_nb < 10 THEN
			SET prop_name_pos = prop_name_pos + 2;
		ELSEIF prop_nb < 100 THEN
			SET prop_name_pos = prop_name_pos + 3;
		ELSE
			SET prop_name_pos = prop_name_pos + 4;
		END IF;	
		IF prop_elt_nb != 0 THEN
			SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
		END IF;
	END IF;

	IF att_nb != 0 THEN	
		SET res_str = INSERT(res_str,dev_name_pos+1,1,CONCAT_WS(CONCAT(att_nb),CHAR(0),CHAR(0)));
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,0);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dev_pipe_prop` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `dev_pipe_prop`(IN dev_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_name,tmp_pipe VARCHAR(255);
	DECLARE known_pipe VARCHAR(255) DEFAULT '';
	DECLARE tmp_value TEXT;
	DECLARE tmp_count INT;
	DECLARE done,prop_nb,pipe_nb,prop_elt_nb INT DEFAULT 0;
	DECLARE dev_name_pos,pipe_name_pos,prop_name_pos INT;
	
	DECLARE cur_dev_pipe_prop CURSOR FOR 
	SELECT pipe,name,count,value 
	FROM tango.property_pipe_device 
	WHERE device = dev_name ORDER BY pipe,name,count;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET res_str = CONCAT_WS(CHAR(0),res_str,dev_name);
	SET dev_name_pos = LENGTH(res_str);

	OPEN cur_dev_pipe_prop;

	REPEAT
		FETCH cur_dev_pipe_prop INTO tmp_pipe,tmp_name,tmp_count,tmp_value;
		IF NOT done THEN
		
			IF tmp_pipe != known_pipe THEN
				IF prop_nb != 0 THEN
					SET res_str = INSERT(res_str,pipe_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
					IF prop_nb < 10 THEN
						SET prop_name_pos = prop_name_pos + 2;
					ELSEIF prop_nb < 100 THEN
						SET prop_name_pos = prop_name_pos + 3;
					ELSE
						SET prop_name_pos = prop_name_pos + 4;
					END IF;
				END IF;
				SET known_pipe = tmp_pipe;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_pipe);
				SET pipe_name_pos = LENGTH(res_str);
				SET pipe_nb = pipe_nb + 1;
				SET prop_nb = 0;
			END IF;
			
			IF tmp_count = 1 THEN
				IF prop_elt_nb != 0 THEN
					SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
					IF prop_nb = 0 THEN
						IF prop_elt_nb < 10 THEN
							SET pipe_name_pos = pipe_name_pos + 2;
						ELSEIF prop_elt_nb < 100 THEN
							SET pipe_name_pos = pipe_name_pos + 3;
						ELSE
							SET pipe_name_pos = pipe_name_pos + 4;
						END IF;
					END IF;
				END IF;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_name);
				SET prop_name_pos = LENGTH(res_str);
				SET prop_nb = prop_nb + 1;
				SET prop_elt_nb = 0;
			END IF;
						
			SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_value);
			SET prop_elt_nb = prop_elt_nb + 1;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_dev_pipe_prop;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,pipe_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
		IF prop_nb < 10 THEN
			SET prop_name_pos = prop_name_pos + 2;
		ELSEIF prop_nb < 100 THEN
			SET prop_name_pos = prop_name_pos + 3;
		ELSE
			SET prop_name_pos = prop_name_pos + 4;
		END IF;	
		IF prop_elt_nb != 0 THEN
			SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
		END IF;
	END IF;

	IF pipe_nb != 0 THEN	
		SET res_str = INSERT(res_str,dev_name_pos+1,1,CONCAT_WS(CONCAT(pipe_nb),CHAR(0),CHAR(0)));
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,0);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dev_prop` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `dev_prop`(IN dev_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_name VARCHAR(255);
	DECLARE tmp_value TEXT;
	DECLARE tmp_count INT;
	DECLARE done,prop_nb,prop_elt_nb INT DEFAULT 0;
	DECLARE dev_name_pos,prop_name_pos INT;
	
	DECLARE cur_dev CURSOR FOR 
	SELECT name,count,value 
	FROM tango.property_device 
	WHERE device = dev_name ORDER BY name,count;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');
	
	SET res_str = CONCAT_WS(CHAR(0),res_str,dev_name);
	SET dev_name_pos = LENGTH(res_str);

	OPEN cur_dev;

	REPEAT
		FETCH cur_dev INTO tmp_name,tmp_count,tmp_value;
		IF NOT done THEN
		
			IF tmp_count = 1 THEN
				IF prop_elt_nb != 0 THEN
					SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
				END IF;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_name);
				SET prop_name_pos = LENGTH(res_str);
				SET prop_nb = prop_nb + 1;
				SET prop_elt_nb = 0;
			END IF;
						
			SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_value);
			SET prop_elt_nb = prop_elt_nb + 1;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_dev;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
	END IF;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,dev_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,0);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ds_start` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ds_start`(IN ds_name VARCHAR(255),
 IN recev_host VARCHAR(255),
 OUT res_str MEDIUMBLOB)
    READS SQL DATA
    COMMENT 'release 1.13'
proc: BEGIN

	DECLARE notifd_event_name VARCHAR(255) DEFAULT 'notifd/factory/';
	DECLARE adm_dev_name VARCHAR(255) DEFAULT 'dserver/';
	DECLARE done, dev_nb,class_nb INT DEFAULT 0;
	DECLARE tmp_class,d_name,rel_str VARCHAR(255);
	DECLARE dev_list BLOB;
	DECLARE start,pos,ds_pipe INT DEFAULT 1;
	DECLARE class_nb_pos INT;
	DECLARE ca_dev_name VARCHAR(255);
	DECLARE host VARCHAR(255);

	DECLARE cur_class_list CURSOR FOR 
	SELECT DISTINCT class 
	FROM tango.device 
	WHERE server = ds_name;
		
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;	
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET adm_dev_name = CONCAT(adm_dev_name,ds_name);





	SET pos = LOCATE('%%',recev_host);
	IF pos != 0 THEN
		SET rel_str = SUBSTRING(recev_host,pos+2);
		SET host = SUBSTRING(recev_host,1,pos-1);
		IF rel_str >= 9 THEN
			SET ds_pipe = 1;
		ELSE
			SET ds_pipe = 0;
		END IF;
	ELSE
		SET ds_pipe = 0;
		SET host = recev_host;
	END IF;





	IF ds_pipe = 1 THEN
		CALL tango.proc_release_nb(res_str);
	END IF;





	CALL tango.import_device(adm_dev_name,res_str);

	IF LOCATE('Not Found',res_str) != 0 OR LOCATE('MySQL Error',res_str) != 0 THEN
		LEAVE proc;
	END IF;
	




	SET notifd_event_name = CONCAT(notifd_event_name,host);
	CALL tango.import_event(notifd_event_name,res_str);
	SET done = 0;
	
	IF LOCATE('MySQL Error',res_str) != 0 THEN
		SET res_str = 'MySQL ERROR during import_event procedure for event factory';
		LEAVE proc;
	END IF;




	
	CALL tango.import_event(adm_dev_name,res_str);
	SET done = 0;
	
	IF LOCATE('MySQL Error',res_str) != 0 THEN
		SET res_str = 'MySQL ERROR during import_event procedure for DS event channel';
		LEAVE proc;
	END IF;
	




	CALL tango.class_prop('DServer',res_str);
	SET done = 0;
	
	IF LOCATE('MySQL Error',res_str) != 0 THEN
		SET res_str = 'MySQL ERROR while getting DServer class property(ies)';
		LEAVE proc;
	END IF;





	CALL tango.class_prop('Default',res_str);
	SET done = 0;
	
	IF LOCATE('MySQL Error',res_str) != 0 THEN
		SET res_str = 'MySQL ERROR while getting Default class property(ies)';
		LEAVE proc;
	END IF;
	




	CALL tango.dev_prop(adm_dev_name,res_str);
	SET done = 0;
	



	SET res_str = CONCAT_WS(CHAR(0),res_str,ds_name);
	SET class_nb_pos = LENGTH(res_str);		



	
	OPEN cur_class_list;

	REPEAT
		FETCH cur_class_list INTO tmp_class;
		IF NOT done THEN

			IF tmp_class != 'dserver' THEN
				SET class_nb = class_nb + 1;
						
				CALL tango.class_prop(tmp_class,res_str);
				
				IF LOCATE('MySQL Error',res_str) != 0 THEN
					SET res_str = 'MySQL ERROR while getting DS class(es) property(ies)';
					CLOSE cur_class_list;
					LEAVE proc;
				END IF;
				
				CALL tango.class_att_prop(tmp_class,res_str);
				
				IF LOCATE('MySQL Error',res_str) != 0 THEN
					SET res_str = 'MySQL ERROR while getting DS class(es) attribute(s) property(ies)';
					CLOSE cur_class_list;
					LEAVE proc;
				END IF;

				IF ds_pipe = 1 THEN
					CALL tango.class_pipe_prop(tmp_class,res_str);
				
					IF LOCATE('MySQL Error',res_str) != 0 THEN
						SET res_str = 'MySQL ERROR while getting DS class(es) pipe(s) property(ies)';
						CLOSE cur_class_list;
						LEAVE proc;
					END IF;
				END IF;
				
				CALL tango.get_dev_list(tmp_class,ds_name,res_str,dev_list,dev_nb);
				
				IF LOCATE('MySQL Error',res_str) != 0 THEN
					SET res_str = 'MySQL ERROR while getting DS class(es) device list';
					CLOSE cur_class_list;
					LEAVE proc;
				END IF;




				
				WHILE dev_nb > 0 DO
					SET pos = LOCATE(CHAR(0),dev_list,start);
					IF pos = 0 THEN
						SET d_name = SUBSTRING(dev_list,start);
					ELSE
						SET d_name = SUBSTRING(dev_list,start,pos-start);
						SET start = pos + 1;
					END IF;

										
					CALL tango.dev_prop(d_name,res_str);
					
					IF LOCATE('MySQL Error',res_str) != 0 THEN
						SET res_str = 'MySQL ERROR while getting device property(ies)';
						CLOSE cur_class_list;
						LEAVE proc;
					END IF;
					
					CALL tango.dev_att_prop(d_name,res_str);
					
					IF LOCATE('MySQL Error',res_str) != 0 THEN
						SET res_str = 'MySQL ERROR while getting device attribute property(ies)';
						CLOSE cur_class_list;
						LEAVE proc;
					END IF;

					IF ds_pipe = 1 THEN
						CALL tango.dev_pipe_prop(d_name,res_str);
					
						IF LOCATE('MySQL Error',res_str) != 0 THEN
							SET res_str = 'MySQL ERROR while getting device pipe property(ies)';
							CLOSE cur_class_list;
							LEAVE proc;
						END IF;
					END IF;
					
					SET dev_nb = dev_nb - 1;
				END WHILE;
				SET start = 1;
			END IF;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_class_list;
	
	SET res_str = INSERT(res_str,class_nb_pos+1,1,CONCAT_WS(CONCAT(class_nb),CHAR(0),CHAR(0)));
	




	SET ca_dev_name = 'Empty';
	
	CALL tango.obj_prop('CtrlSystem',ca_dev_name,res_str);
	
	IF ca_dev_name != 'Empty' THEN





		CALL tango.import_device(ca_dev_name,res_str);

	END IF;
	
	
END proc ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_dev_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_dev_list`(IN class_name VARCHAR(255), IN serv VARCHAR(255),
 INOUT res_str MEDIUMBLOB, OUT d_list TEXT, OUT d_num INT)
    READS SQL DATA
BEGIN
	DECLARE tmp_name VARCHAR(255);
	DECLARE done INT DEFAULT 0;
	DECLARE nb_dev INT DEFAULT 0;
	DECLARE class_name_pos INT;
		
	DECLARE cur_dev_list CURSOR FOR 
	SELECT DISTINCT name 
	FROM tango.device 
	WHERE class = class_name AND server = serv ORDER BY name;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET res_str = CONCAT_WS(CHAR(0),res_str,class_name);
	SET class_name_pos = LENGTH(res_str);
	
	OPEN cur_dev_list;

	REPEAT
		FETCH cur_dev_list INTO tmp_name;
		IF NOT done THEN						
			SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_name);
			IF nb_dev = 0 THEN
				SET d_list = CONCAT_WS("",d_list,tmp_name);
			ELSE
				SET d_list = CONCAT_WS(CHAR(0),d_list,tmp_name);
			END IF;
			SET nb_dev = nb_dev + 1;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_dev_list;
	
	SET res_str = INSERT(res_str,class_name_pos+1,1,CONCAT_WS(CONCAT(nb_dev),CHAR(0),CHAR(0)));
	SET d_num = nb_dev;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `import_device` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_device`(IN dev_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
imp_proc: BEGIN
	DECLARE tmp_ior TEXT;
	DECLARE tmp_version VARCHAR(8);
	DECLARE tmp_host,tmp_server,tmp_class VARCHAR(255);
	DECLARE tmp_exp, tmp_pid INT;
	DECLARE not_found INT DEFAULT 0;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET not_found = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');
		
	SELECT exported,ior,version,pid,server,host,class
	INTO tmp_exp,tmp_ior,tmp_version,tmp_pid,tmp_server,tmp_host,tmp_class
	FROM tango.device 
	WHERE name = dev_name;
	
	SET res_str = CONCAT_WS(CHAR(0),res_str,dev_name);
	
	IF not_found = 1 THEN
		SET res_str = CONCAT_WS(CHAR(0),res_str,'Not Found');
		LEAVE imp_proc;
	END IF;
	
	IF tmp_ior IS NULL THEN
		SET res_str = CONCAT_WS(CHAR(0),res_str,'');
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_ior);
	END IF;

	IF tmp_version IS NULL THEN
		SET res_str = CONCAT_WS(CHAR(0),res_str,'');
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_version);
	END IF;
			
	SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_server,tmp_host,CONCAT(tmp_exp));
	
	IF tmp_pid IS NULL THEN
		SET res_str = CONCAT_WS(CHAR(0),res_str,'');
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,CONCAT(tmp_pid));
	END IF;

	SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_class);	
END imp_proc ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `import_event` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_event`(IN ev_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_ior TEXT;
	DECLARE tmp_version VARCHAR(8);
	DECLARE tmp_host VARCHAR(255);
	DECLARE tmp_ev_name VARCHAR(255);
	DECLARE tmp_ev_name_canon VARCHAR(255);
	DECLARE tmp_exp, tmp_pid, dot INT;
	DECLARE not_found INT DEFAULT 0;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET not_found = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET tmp_ev_name = ev_name;
	SET tmp_ev_name = REPLACE(tmp_ev_name,'_','\_');
		
	SELECT exported,ior,version,pid,host
	INTO tmp_exp,tmp_ior,tmp_version,tmp_pid,tmp_host
	FROM tango.event
	WHERE name = tmp_ev_name;
	
	IF not_found = 1 THEN
		SET dot = LOCATE('.',tmp_ev_name);
		IF dot != 0 THEN
			SET tmp_ev_name_canon = SUBSTRING(tmp_ev_name,1,dot - 1);
			SET not_found = 0;

			SELECT exported,ior,version,pid,host
			INTO tmp_exp,tmp_ior,tmp_version,tmp_pid,tmp_host
			FROM tango.event
			WHERE name = tmp_ev_name_canon;
	
			IF not_found = 1 THEN
				SET res_str = CONCAT_WS(CHAR(0),res_str,ev_name,'Not Found');
			ELSE
				SET res_str = CONCAT_WS(CHAR(0),res_str,ev_name,tmp_ior,tmp_version,tmp_host,CONCAT(tmp_exp),CONCAT(tmp_pid));
			END IF;
		ELSE
			SET res_str = CONCAT_WS(CHAR(0),res_str,ev_name,'Not Found');
		END IF;
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,ev_name,tmp_ior,tmp_version,tmp_host,CONCAT(tmp_exp),CONCAT(tmp_pid));
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `init_history_ids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `init_history_ids`()
BEGIN
	DECLARE ret_id INT DEFAULT -1;

    SELECT COUNT(*) INTO ret_id FROM tango.object_history_id;
    IF ret_id = 0 THEN
        INSERT INTO tango.object_history_id VALUES (0);
    END IF;


    SET ret_id = -1;
    SELECT COUNT(*) INTO ret_id FROM tango.class_attribute_history_id;
    IF ret_id = 0 THEN
        INSERT INTO tango.class_attribute_history_id VALUES (0);
    END IF;


    SET ret_id = -1;
    SELECT COUNT(*) INTO ret_id FROM tango.class_history_id;
    IF ret_id = 0 THEN
        INSERT INTO tango.class_history_id VALUES (0);
    END IF;


    SET ret_id = -1;
    SELECT COUNT(*) INTO ret_id FROM tango.class_pipe_history_id;
    IF ret_id = 0 THEN
        INSERT INTO tango.class_pipe_history_id VALUES (0);
    END IF;


    SET ret_id = -1;
    SELECT COUNT(*) INTO ret_id FROM tango.device_attribute_history_id;
    IF ret_id = 0 THEN
        INSERT INTO tango.device_attribute_history_id VALUES (0);
    END IF;


    SET ret_id = -1;
    SELECT COUNT(*) INTO ret_id FROM tango.device_history_id;
    IF ret_id = 0 THEN
        INSERT INTO tango.device_history_id VALUES (0);
    END IF;


    SET ret_id = -1;
    SELECT COUNT(*) INTO ret_id FROM tango.device_pipe_history_id;
    IF ret_id = 0 THEN
        INSERT INTO tango.device_pipe_history_id VALUES (0);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `init_tac_tables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `init_tac_tables`()
BEGIN
	DECLARE ret_id INT DEFAULT -1;

    SELECT COUNT(*) INTO ret_id FROM tango.access_address WHERE user='*' and address='*.*.*.*';
    IF ret_id = 0 THEN
        INSERT INTO tango.access_address VALUES ('*','*.*.*.*','FF.FF.FF.FF',20060824131221,20060824131221);
    END IF;


    SET ret_id = -1;
    SELECT COUNT(*) INTO ret_id FROM tango.access_device WHERE user='*' and device='*/*/*';
    IF ret_id = 0 THEN
        INSERT INTO tango.access_device VALUES ('*','*/*/*','write',20060824131221,20060824131221);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `obj_prop` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `obj_prop`(IN obj_name VARCHAR(255),OUT serv_dev_name VARCHAR(255),
 INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_name VARCHAR(255);
	DECLARE tmp_value TEXT;
	DECLARE tmp_count INT;
	DECLARE done,prop_nb,prop_elt_nb INT DEFAULT 0;
	DECLARE dev_name_pos,prop_name_pos INT;
	DECLARE serv_defined INT DEFAULT 0;
	
	DECLARE cur_dev CURSOR FOR 
	SELECT name,count,value 
	FROM tango.property 
	WHERE object = obj_name ORDER BY name,count;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');

	SET res_str = CONCAT_WS(CHAR(0),res_str,obj_name);
	SET dev_name_pos = LENGTH(res_str);
	
	OPEN cur_dev;

	REPEAT
		FETCH cur_dev INTO tmp_name,tmp_count,tmp_value;
		IF NOT done THEN
		
			IF tmp_count = 1 THEN
				IF prop_elt_nb != 0 THEN
					SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
				END IF;
				SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_name);
				SET prop_name_pos = LENGTH(res_str);
				SET prop_nb = prop_nb + 1;
				SET prop_elt_nb = 0;
				IF tmp_name = 'Services' THEN
					SET serv_defined = 1;
				ELSE
					SET serv_defined = 0;
				END IF;
			END IF;

			IF serv_defined = 1 THEN
				IF LOCATE('AccessControl/tango:',tmp_value) != 0 THEN
					SET serv_dev_name = SUBSTRING(tmp_value,21);
						IF LOCATE('tango://',serv_dev_name) != 0 THEN
							SET serv_dev_name = SUBSTRING_INDEX(serv_dev_name,'/',-3);
						END IF;
				END IF;
			END IF;
								
			SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_value);
			SET prop_elt_nb = prop_elt_nb + 1;
		END IF;
	UNTIL done END REPEAT;
	
	CLOSE cur_dev;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,prop_name_pos+1,1,CONCAT_WS(CONCAT(prop_elt_nb),CHAR(0),CHAR(0)));
	END IF;

	IF prop_nb != 0 THEN	
		SET res_str = INSERT(res_str,dev_name_pos+1,1,CONCAT_WS(CONCAT(prop_nb),CHAR(0),CHAR(0)));
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,0);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_release_nb` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_release_nb`(INOUT res_str MEDIUMBLOB)
    READS SQL DATA
BEGIN
	DECLARE tmp_rel VARCHAR(255);
	DECLARE not_found INT DEFAULT 0;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET not_found = 1;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION SET res_str = CONCAT_WS(CHAR(0),res_str,'MySQL Error');
	
	SELECT ROUTINE_COMMENT
	INTO tmp_rel
	FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'ds_start' AND ROUTINE_SCHEMA = 'tango';
	
	IF not_found = 1 THEN
		SET res_str = CONCAT_WS(CHAR(0),res_str,'Not Found');
	ELSE
		SET res_str = CONCAT_WS(CHAR(0),res_str,tmp_rel);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-19 13:31:39

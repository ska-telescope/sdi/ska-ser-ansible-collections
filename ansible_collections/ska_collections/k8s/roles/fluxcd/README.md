# FluxCD: GitOps ToolKit

[FluxCD](https://fluxcd.io/) GitOps for Apps and Infrastructure

## What's included

This role installs FluxCD to the Kubernetes cluster.

from atlassian import Confluence
import json
from datetime import datetime


def create_confluence_page(params):
    confluence = Confluence(
        url=params['url'],
        token=params['token']
    )

    # Read the data
    with open(params['file_path'], 'r') as fd:
        data = json.load(fd)

    # Convert the timestamp to a readable format
    readable_timestamp = datetime.fromtimestamp(int(params['timestamp']))

    # Create the Confluence page body
    body = """<h1><strong>GPU Stress Tests</strong></h1>"""

    # Add a list of tested nodes with the logs
    for test in data:
        body = body + f"<h2>{test['item']['metadata']['labels']['node']}</h2>"
        body = body + f"""<ac:structured-macro ac:name="code" ac:schema-version="1" ac:macro-id="519f12ed-7996-4fef-adf1-b634dc2a2c52"><ac:parameter ac:name="title">GPU Stress Test Result</ac:parameter><ac:parameter ac:name="collapse">true</ac:parameter><ac:plain-text-body><![CDATA[{test['log']}]]></ac:plain-text-body></ac:structured-macro>"""  # noqa: E501

    # Create a new page
    page_title = f"GPU Stress tests ran at {readable_timestamp}"
    page = confluence.create_page(
        space=params['space_key'],
        title=page_title,
        body=body,
        parent_id=params['parent_page_id'],
        type='page',
        representation='storage'
    )

    # Check if page creation was successful
    if not page:
        print("Failed to create page")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Create a Confluence page.")
    parser.add_argument("--url", required=True, help="Confluence URL")
    parser.add_argument("--token", required=True, help="Confluence token")  # noqa: E501
    parser.add_argument("--space_key", required=True, help="Confluence space key")  # noqa: E501
    parser.add_argument("--parent_page_id", required=True, help="Parent page ID")  # noqa: E501
    parser.add_argument("--file_path", required=True, help="File path of the gpu-burn results")  # noqa: E501
    parser.add_argument("--timestamp", required=True, help="Timestamp of the gpu-burn run")  # noqa: E501

    args = parser.parse_args()
    params = {
        'url': args.url,
        'token': args.token,
        'space_key': args.space_key,
        'parent_page_id': args.parent_page_id,
        'file_path': args.file_path,
        'timestamp': args.timestamp,
    }
    create_confluence_page(params)

# Mysql exporter task

This will install a mysql exporter:
* On a namespace
* It will connect to a mariadb server on that namespace
* As a loadbalancer service on an external IP

It also requires the monitoring password from Vault

The variables on the workload-cluster.yml file should look like this:
```
k8s_mysql_exporter_config:
  - external_ip: "10.164.11.100"
    namespace: "integration"
    servername: "tangodb-pod-name-0"
  - external_ip: "10.164.11.101"
    namespace: "test-equipment"
    servername: "tangodb-pod-name-b"
k8s_mariadb_monitor_password: "{{ secrets[ska_datacentre][ska_environment].monitoring.mariadb_sql_exporter_password | default_to_env('K8S_MARIADB_SQL_EXPORTER_PASSWORD') }}"
```
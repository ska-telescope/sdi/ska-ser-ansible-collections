# What am I?

Ansible script to create a monitoring user on a namespace/pod.

# How to use this

You must define at least these variables:

```
in_password: the password for this user, it should be read from vault 
in_namespace: the namespace
```

Defaults included:

```
host: host where the user connects, default is %
user: the username, default is mon
dbpod: the pod name, default is databaseds-tangodb-tango-databaseds-0
mysqluser: the mysql user to run this, default is root
mysqlpassenv: the environment variable with the mysqluser password, default is MYSQL_ROOT_PASSWORD
```




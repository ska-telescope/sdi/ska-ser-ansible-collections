# Shrink a pg deployment

This collection will shrink to one node a timescale deployment. You will need to define these variables:
* pg_namespace: namespace where it will happen
* pg_name: name of the statefulset
* k8s_kubeconfig: custom location of the kubeconfig. Optional

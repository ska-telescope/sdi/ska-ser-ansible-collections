# Grow a pg deployment

This collection will grow to two nodes a timescale deployment. You will need to define these variables:
* pg_namespace: namespace where it will happen
* pg_name: name of the statefulset
* pg_replicas: number of replicas to scale, default is 2. Optional
* k8s_kubeconfig: custom location of the kubeconfig. Optional


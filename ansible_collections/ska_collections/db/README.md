# Ansible Collection - ska_collections.db

This directory contains the `ska_collections.db` Ansible Collection. The collection includes roles and playbooks to do things on databases.

This collection is currently maintained by [SKAO](https://www.skao.int/).

## Ansible

Tested with the current Ansible 7.2.x releases.

## Ansible Roles

Each role encompasses a individual component/service/integration to be deployed in a Kubernetes cluster.  In most cases, the only input required is the `KUBECONFIG` file/path for the target cluster to apply the role to.  Note that this is the `KUBECONFIG` on the target inventory host that the Ansible role will be run against.

| Name | Description | Version | db Requirements |
| ---- | ----------- | ------- | --- |
| [db.mariadb.mkmon](./mariadb/mkmon) | Creates a monitoring user on a namespace/pod | master | MariaDB |
| [db.mariadb.singlebackup](./mariadb/singlebackup) | Full MariaDB backup job on the maria backup volume | master | MariaDB operator |
| [db.mariadb.s3backup](./mariadb/s3backup) |  Full MariaDB backup job on an s3 bucket | master | MariaDB operator |
| [db.mariadb.s3cronbackup](./mariadb/s3cronbackup) |  Full scheduled MariaDB backup job on an s3 bucket | master | MariaDB operator |
| [db.postgresql.singlebackup](./postgresql/singlebackup) | Full PostgreSQL backup job on the backup volume | master | CNPG |
| [db.postgresql.s3backup](./postgresql/s3backup) |  Full PostgreSQL backup job on an s3 bucket | master | CNPG |
| [db.postgresql.s3cronbackup](./postgreesql/s3cronbackup) |  Full scheduled PostgreSQL backup job on an s3 bucket | master | CNPG |



## Installation

Before using the collection, you need to install the collection with the `ansible-galaxy` CLI:

    ansible-galaxy collection install ska_collections.db

You can also include it in a `requirements.yml` file and install it via ansible-galaxy collection install -r requirements.yml` using the format:

```yaml
collections:
- name: ska_collections.db
```

## Usage

`Make` targets are available for each role/playbook combination in [db.mk](../../../resources/jobs/db.mk).

Installation playbooks for each component can be found in the [playbooks/](./playbooks) folder in the following files:

| Name | Description |
| ---- | ----------- |
| [mariadb_singlebackup.yml](./playbooks/mariadb_singlebackup.yml) | MariaDB single backup |
| [mariadb_s3backup.yml](./playbooks/mariadb_s3backup.yml) | MariaDB S3 backup |
| [mariadb_s3cronbackup.yml](./playbooks/mariadb_s3cronbackup.yml) | MariaDB s3 cron backup |
| [postgresql_singlebackup.yml](./playbooks/postgresql_singlebackup.yml) | PostgreSQL single backup TODO |
| [postgresql_s3backup.yml](./playbooks/postgresql_s3backup.yml) | PostgreSQL S3 backup TODO |
| [postgresql_s3cronbackup.yml](./playbooks/postgresql_s3cronbackup.yml) | PostgreSQL s3 cron backup TODO |

In order to run these playbooks, it's needed to specify the Ansible Inventory location and the respective group/hosts ***target_hosts*** variable.

Launch **mariadb_singlebackup** as an example:
```
make playbooks db launch TAGS=mariadb_singlebackup \
  PLAYBOOKS_HOSTS=<host with cluster's kubeconfig>  \
```

Ideally, one should set `db_kubeconfig` in the host's `host_vars` or relevant `group_vars`. Optionally, you can target any cluster by setting the `KUBECONFIG` environment variable:

```
make playbooks db launch TAGS=mariadb_singlebackup \
  PLAYBOOKS_HOSTS=<cluster host> \
  KUBECONFIG=<path to a kubeconfig>
```


> To run all the playbooks then omit the `TAGS`

## More information

- [Ansible Collection overview](https://github.com/ansible-collections/overview)
- [Ansible User guide](https://docs.ansible.com/ansible/latest/user_guide/index.html)
- [Ansible Developer guide](https://docs.ansible.com/ansible/latest/dev_guide/index.html)
- [Ansible Community code of conduct](https://docs.ansible.com/ansible/latest/community/code_of_conduct.html)

## Licensing

BSD-3.

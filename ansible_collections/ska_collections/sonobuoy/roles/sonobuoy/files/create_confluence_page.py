from atlassian import Confluence
import json
import html
from datetime import datetime
from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
from oauth2client.service_account import ServiceAccountCredentials


def push_google_drive(params):
    # Parse the credentials JSON string to a dictionary
    credentials_dict = json.loads(str(params['google_drive_credentials']).replace("'", '"'))  # noqa: E501
    gauth = GoogleAuth()
    gauth.credentials = ServiceAccountCredentials.from_json_keyfile_dict(
        credentials_dict, scopes=['https://www.googleapis.com/auth/drive'])

    drive = GoogleDrive(gauth)

    file1 = drive.CreateFile(
        {'parents': [{"id": params['google_drive_folder']}], 'title': f'{params["timestamp"]}-{params["k8s_cluster_local_name"]}-sonobuoy-results.json'})  # noqa: E501

    file1.SetContentFile(params['file_path'])
    file1.Upload()

    print("Tests results JSON file uploaded to GD successfully")

    return file1['id']


def create_confluence_page(params):
    confluence = Confluence(
        url=params['url'],
        token=params['token']
    )

    data = [json.loads(line) for line in open(params['file_path'], 'r')]
    # Count the number of passed, failed, and skipped tests
    passed_tests = [item for item in data if item['status'] == 'passed']
    failed_tests = [item for item in data if item['status'] == 'failed']
    skipped_tests = [item for item in data if item['status'] == 'skipped']

    passed_count = len(passed_tests)
    failed_count = len(failed_tests)
    skipped_count = len(skipped_tests)

    # Convert the timestamp to a readable format
    readable_timestamp = datetime.fromtimestamp(int(params['timestamp']))

    # Create the Confluence page body
    body = f"""
    <h1><strong>Command run:</strong></h1>
    <p>{html.escape(params['sonobuoy_command'])}</p>

    <h2><strong>Tests</strong></h2>
    <table>
        <tr>
            <th>Test Status</th>
            <th>Count</th>
        </tr>
        <tr>
            <td>Passed</td>
            <td>{passed_count}</td>
        </tr>
        <tr>
            <td>Failed</td>
            <td>{failed_count}</td>
        </tr>
        <tr>
            <td>Skipped</td>
            <td>{skipped_count}</td>
        </tr>
    </table>

    <h2>Failed Tests</h2>
    """

    # Add details of the failed tests
    for test in failed_tests:
        body += f"""
        <ac:structured-macro ac:name="expand">
            <ac:parameter ac:name="title">
            {html.escape(test['name'])}
            </ac:parameter>
            <ac:rich-text-body>
                <p>
                {html.escape(str(test.get('details', 'No details')))}
                </p>
            </ac:rich-text-body>
        </ac:structured-macro>
        """

    # Create a new page
    page_title = f"Sonobuoy tests on {params['k8s_cluster_local_name']} ran at {readable_timestamp}"  # noqa: E501
    page = confluence.create_page(
        space=params['space_key'],
        title=page_title,
        body=body,
        parent_id=params['parent_page_id'],
        type='page',
        representation='storage'
    )

    # Check if page creation was successful
    if page:
        print("Confluence page created successfully")
        confluence.attach_file(
            filename=params['file_path'],
            name='results.json',
            content_type='application/json',
            page_id=page['id']
        )

        # Push results file into google drive folder and get the ID
        drive_file_id = push_google_drive(params)

        print("https://confluence.skatelescope.org/pages/viewpage.action?pageId={}".format(page['id']))  # noqa: E501

        # Embed the file in the page body using the google-drive macro
        body += f"""
        <h2>
            <strong>
                All tests are described in the results.json file
            </strong>
        </h2>
        <p>
        <strong>
        <ac:structured-macro ac:name="lref-gdrive-embedded-file">
        <ac:parameter ac:name="url">
        https://drive.google.com/file/d/{drive_file_id}/view?usp=drivesdk
        </ac:parameter>
        <ac:parameter ac:name="fullwidth">true</ac:parameter>
        <ac:parameter ac:name="height">600</ac:parameter>
        </ac:structured-macro>
        </strong>
        </p>
        """

        # Update the page with the embedded attachment link
        confluence.update_page(
            page_id=page['id'],
            title=page['title'],
            body=body,
            representation='storage'
        )

    else:
        print("Failed to create page")


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Create a Confluence page.")
    parser.add_argument("--url", required=True, help="Confluence URL")
    parser.add_argument("--token", required=True, help="Confluence token")  # noqa: E501
    parser.add_argument("--space_key", required=True, help="Confluence space key")  # noqa: E501
    parser.add_argument("--parent_page_id", required=True, help="Parent page ID")  # noqa: E501
    parser.add_argument("--k8s_cluster_local_name", required=True, help="k8s cluster local name")  # noqa: E501
    parser.add_argument("--file_path", required=True, help="File path of the Sonobuoy results")  # noqa: E501
    parser.add_argument("--timestamp", required=True, help="Timestamp of the Sonobuoy run")  # noqa: E501
    parser.add_argument("--sonobuoy_command", required=True, help="Command that was run to trigger sonobuoy tests")  # noqa: E501
    parser.add_argument("--google_drive_folder", required=True, help="Google Drive Folder to save results file")  # noqa: E501
    parser.add_argument("--google_drive_credentials", required=True, help="Google Drive credentials.json")  # noqa: E501

    args = parser.parse_args()
    params = {
        'url': args.url,
        'token': args.token,
        'space_key': args.space_key,
        'parent_page_id': args.parent_page_id,
        'k8s_cluster_local_name': args.k8s_cluster_local_name,
        'file_path': args.file_path,
        'timestamp': args.timestamp,
        'sonobuoy_command': args.sonobuoy_command,
        'google_drive_folder': args.google_drive_folder,
        'google_drive_credentials': args.google_drive_credentials
    }

    create_confluence_page(params)

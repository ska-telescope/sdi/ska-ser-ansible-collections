
# Sonobuoy

Sonobuoy is a diagnostic tool for Kubernetes clusters that helps to understand the state of a cluster by running a set of conformance and custom tests. It's designed to be a versatile tool that can be extended with plugins to suit specific testing needs.

## Pre-Requesites

The machine used to run this playbooks need to have an **admin** kubeconfig and the **KUBECONFIG** variable set, so sonobuoy can run its tests.

## Run run-tests

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks sonobuoy run-tests
```

## Destroy

```shell
PLAYBOOKS_HOSTS=[HOSTNAME] make playbooks sonobuoy destroy
```

If you want to run a new set of tests you will need to destroy before reruning the test playbook

## Test setup

You can enable or skip tests according to your needs by using the variables:

- **sonobuoy_focus_test**: This variable allows you to pick the set of tests you want to test your cluster against. The default value of this variable is ``"\\[Conformance\\]"``, and with this sonobuoy only runs the Conformance tests.

- **sonobuoy_skip_test**: This variable allows you to pick the set of tests you want to skip, like for example Disruptive tests or tests that are only directed towards windows systems. The default value of this variable is ``"\\[Disruptive\\]|NoExecuteTaintManager"``.

- **sonobuoy_version**: Variable to control version of sonobuoy to install.

- **k8s_cluster_domain**: Variable to pass the cluster domain.

- **sonobuoy_non_blocking_taints**: Variable to allow sonobuoy to recognize blocking taints. Default is ``node-role.kubernetes.io/control-plane``

## Automatic creation of Confluence page

This Role also allows an automatic parsing and creation of a confluence page with the data retrieved from the sonobuoy tests. For that there is a need of using a personal token created by IT and shared in vault throughout the different clusters. There is the feature to disable the creation of the confluence page by setting **sonobuoy_confluence_enabled** to `False`. If this is set to true, the space of where the pages will be created need to be set with:

- set the confluence space (**sonobuoy_confluence_space_key**) being the default SE
- set the parent page id with **sonobuoy_parent_page_id**

The tests will also be pushed to a google drive folder that needs the Id definition using `sonobuoy_google_drive_folder`, it also needs credentials that were generated using google api serviceaccount and are shared with vault. The email to be added permissions that is associated with the service account credentials is `ska-platform-test@ska-platform-test.iam.gserviceaccount.com`


## How to Contribute

### Adding a new role
A new role can be added to the [roles](./roles/) folder and then included into the sonobuoy playbook in the [sonobuoy_install.yml](./playbooks/sonobuoy_install.yml) file.

### Updating an existing role
The existing roles can be found in the [roles](./roles/) folder and are already included into the sonobuoy playbook in the [sonobuoy_install](./playbooks/sonobuoy_install) file. To update a role, the role's tasks can be simply modified.


### Add/Update new variables
Ansible variables that are datacentre specific should be added to the `host_vars` folder of the inventory.

Finally, the secret variables are defined in the Sonobuoy [Makefile](../../../resources/jobs/sonobuoy.mk) and can be modified there. To assign proper values to these variables, please use a `PrivateRules.mak` file.

## More information

- [Ansible Using collections](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html)
- [Ansible Collection overview](https://github.com/ansible-collections/overview)
- [Ansible User guide](https://docs.ansible.com/ansible/latest/user_guide/index.html)
- [Ansible Developer guide](https://docs.ansible.com/ansible/latest/dev_guide/index.html)
- [Ansible Community code of conduct](https://docs.ansible.com/ansible/latest/community/code_of_conduct.html)
- [Sonobuoy Investigation](https://confluence.skatelescope.org/pages/viewpage.action?pageId=280872105)

## License

BSD-3.
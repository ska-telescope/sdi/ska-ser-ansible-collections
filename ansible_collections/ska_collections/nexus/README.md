# SKA Nexus Collection
This collection includes a variety of Ansible roles to help automate the installation and configuration of Nexus OSS edition.
This collection is currently maintained by [SKAO](https://www.skao.int/).

## Ansible
Tested with the current Ansible 2.17.5 release.

## Ansible Roles
| Name                                             | Description                                | Version | OS Requirements  | Dependencies |
| ------------------------------------------------ | ------------------------------------------ | ------- | ---------------- | ------------ |
| [nexus.nexus_container](./roles/nexus_container) | Install, configure, test and destroy Nexus |         | Ubuntu 22+ (LTS) | None         |

## Production site
The production Nexus instance for the Central Artefact Repository is hosted at https://artefact.skao.int/.

## Backup site
The backup Nexus instance for the Central Artefact Repository is hosted at https://car-test-1.skao.int/.

## Installation
There are no special requirements for using this playbook. The only requirement is to have Ansible installed on the machine where the playbook will be executed.

## Testing
This role includes a playbook for testing Nexus functionality in both car (hosted) and cache (proxy) modes. By default, the `nexus_mode` variable is set to `car`. This variable can be overridden in the host variables to use a different mode. The playbook tests the following repository types:

- **PyPI**: Upload and retrieve packages (hosted and proxy modes).
- **Helm**: Manage charts and repositories (hosted and proxy modes).
- **APT**: Validate package caching (proxy mode).
- **OCI**: Verify image caching (proxy mode).

The tests utilize SKAO Makefiles to simulate the procedures used in our pipelines. All tests run inside an isolated OCI container, ensuring a controlled and consistent environment.

## Pre-requisites
The instance where Nexus will be installed should first be provisioned by the **init** playbook from the [SKA Common](./roles/instance_common) role.

```shell
make playbooks common init PLAYBOOKS_HOSTS=<nexus-host>
```

## Usage
Playbooks can be found in the [playbooks/](./playbooks) folder in the following files:

| Name                                             | Description          |
| ------------------------------------------------ | -------------------- |
| [install.yml](./playbooks/install.yml)           | Install Nexus        |
| [configure.yml](./playbooks/configure.yml)       | Configure Nexus      |
| [test.yml](./playbooks/test.yml)                 | Test Nexus           |
| [destroy.yml](./playbooks/destroy.yml)           | Destroy Nexus        |
| [sync.yml](./playbooks/sync.yml)                 | Install Sync Service |
| [sync-disable.yml](./playbooks/sync-disable.yml) | Remove Sync Service  |

In order to run these playbooks, call the respective make target with the required `PLAYBOOKS_HOSTS` variable set.
For example, to install Nexus on a host, run the following command:

```shell
make playbooks nexus install PLAYBOOKS_HOSTS=<nexus-host>
```

**Warning: The `destroy.yml` playbook will delete all the data in the Nexus instance. You should never run this playbook on a production instance.**

### Required variables
| Name                                  | Ansible variable            | Obs                 |
| ------------------------------------- | --------------------------- | ------------------- |
| Nexus Admin Password                  | nexus_admin_password        | Datacentre variable |
| Nexus Publisher User Password         | nexus_publisher_password    | Datacentre variable |
| Nexus Gitlab Repository Password      | nexus_gitlab_password       | Datacentre variable |
| Nexus Quarantiner Password            | nexus_quarantiner_password  | Datacentre variable |
| Nexus Conan Repository Password       | nexus_conan_password        | Datacentre variable |
| Nexus RAW Publisher Password          | nexus_rawpublisher_password | Datacentre variable |
| Nexus P4 Repository Password          | nexus_p4_password           | Datacentre variable |
| Nexus RAW STS-608 Repository Password | nexus_rawsts608_password    | Datacentre variable |
| Nexus Webhook Secret Key              | nexus_webhook_secret_key    | Datacentre variable |

### Adding new roles
New roles can be added to the [roles/nexus_container](./roles/nexus_container/) folder and then included into the nexus configurations playbook at [configure.yml](./playbooks/configure.yml) file.

### Add/Update new variables
Ansible variables that are datacentre specific should be added under `group_vars` to the respective datacentre group on [SKA Infrastructure Machinery](https://gitlab.com/ska-telescope/sdi/ska-ser-infra-machinery).

Variables not specific to the datacentre should be added to the [defaults](./roles/nexus_container/defaults/main.yml) folder in the respective role.

## More information
- [Ansible Using collections](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html)
- [Ansible Collection overview](https://github.com/ansible-collections/overview)
- [Ansible User guide](https://docs.ansible.com/ansible/latest/user_guide/index.html)
- [Ansible Developer guide](https://docs.ansible.com/ansible/latest/dev_guide/index.html)
- [Ansible Community code of conduct](https://docs.ansible.com/ansible/latest/community/code_of_conduct.html)

## License
BSD-3.
"""This script migrates components from one Nexus instance to another
"""

import argparse
import logging
import os
import time

import requests

# Parse command line arguments
parser = argparse.ArgumentParser(description="Nexus Migration Script")
parser.add_argument("--username", required=True, help="Nexus username")
parser.add_argument("--password", required=True, help="Nexus password")
parser.add_argument("--source_url", required=True, help="Source Nexus URL")
parser.add_argument("--target_url", required=True, help="Target Nexus URL")
parser.add_argument(
    "--repo", required=False, help="Single hosted repository for repository"
)
parser.add_argument(
    "--check",
    action="store_true",
    default=False,
    help="Enable check mode (default: False)",
)
parser.add_argument(
    "--daemon",
    action="store_true",
    default=False,
    help="Enable daemon mode. (default: False)",
)
args = parser.parse_args()

# Configuration
source_url = args.source_url
target_url = args.target_url
username = args.username
password = args.password
single_repo_name = args.repo
check_mode = args.check
daemon_mode = args.daemon

NEXUS_TEMP_DIR = "/tmp/nexus-migration"

# Authenticate with Nexus
auth = (username, password)

# Configure logging
FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
if not check_mode:
    LEVEL = logging.INFO
    HANDLERS = [
        logging.FileHandler("/var/log/nexus_migration.log"),
        logging.StreamHandler(),
    ]
else:
    LEVEL = logging.INFO
    HANDLERS = [
        logging.FileHandler("/var/log/nexus_migration_check.log"),
        logging.StreamHandler(),
    ]
logging.basicConfig(level=LEVEL, format=FORMAT, handlers=HANDLERS)


def get_repositories(nexus_url):
    """
    Fetches and returns list of hosted repositories\
         from specified Nexus instance.

    :param nexus_url: The base URL of the Nexus instance.
    :return: A list of dictionaries representing hosted repositories.
    """

    logging.info("Fetching repositories from %s", nexus_url)
    url = f"{nexus_url}/service/rest/v1/repositories"
    response = requests.get(url, auth=auth, timeout=120)
    response.raise_for_status()

    # Filter and return only hosted repositories
    repositories = response.json()
    repos = [
        repo
        for repo in repositories
        if repo.get("type") in ["hosted", "proxy"]
    ]

    return repos


def get_assets(url, repo_name):
    """
    Get all assets in a specific repository.

    :param url: The base URL of the Nexus instance.
    :param repo_name: The name of the repository.
    :return: A list of dictionaries representing assets in the repository.
    """

    logging.info("Fetching assets from repository %s on %s", repo_name, url)
    assets = []
    continuation_token = None
    url = f"{url}/service/rest/v1/assets?repository={repo_name}"
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    while True:
        if continuation_token:
            full_url = f"{url}&continuationToken={continuation_token}"
        else:
            full_url = url

        response = requests.get(
            full_url, auth=auth, headers=headers, timeout=120
        )
        response.raise_for_status()
        data = response.json()

        assets.extend(data["items"])

        continuation_token = data.get("continuationToken")
        if not continuation_token:
            break
    return assets


def download_component_asset(asset, download_dir):
    """
    Download asset to a specified directory.

    :param asset: The asset to download.
    :param download_dir: The directory to download the asset to.
    :return: The path to the downloaded asset.
    """

    logging.info("Downloading asset %s", asset.get("path"))
    os.makedirs(download_dir, exist_ok=True)
    asset_url = asset.get("downloadUrl")
    file_name = os.path.join(download_dir, asset.get("path").split("/")[-1])

    # Check if file already exists
    if os.path.exists(file_name):
        logging.debug("File %s already exists. Skipping download.", file_name)
    else:
        with requests.get(
            asset_url, auth=auth, stream=True, timeout=120
        ) as response:
            response.raise_for_status()
            with open(file_name, "wb") as file:
                for chunk in response.iter_content(chunk_size=8192):
                    file.write(chunk)

    return file_name


def download_proxy_asset(asset, download_dir):
    """
    Download all assets of a proxy to a specified directory.
    This is enough to trigger the proxy cache to download the asset.

    :param asset: The asset to download.
    :param download_dir: The directory to download the asset to.
    :return: The path to the downloaded asset.
    """

    logging.info("Downloading asset %s", asset.get("path"))
    os.makedirs(download_dir, exist_ok=True)
    file_name = os.path.join(download_dir, asset.get("path").split("/")[-1])

    with requests.get(
        asset.get("downloadUrl"), auth=auth, stream=True, timeout=120
    ) as response:
        response.raise_for_status()
        with open(file_name, "wb") as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)

    return file_name


def find_missing_assets(source_assets, target_assets, proxy):
    """
    Find assets in source_assets that are not present in target_assets
    based on path and SHA1 checksum.

    Handles paths with or without a leading slash.

    :param source_nexus_assets: List of dictionaries containing source assets
    :param target_nexus_assets: List of dictionaries containing target assets
    :param proxy: If True, allows different SHA1 checksums
    :return: List of assets from source_assets not found in target_assets
    """
    # Filter out directories from target assets and normalize paths
    if proxy:
        # If proxy is True, only check the path
        target_asset_keys = {
            asset.get("path", "").lstrip("/")
            for asset in target_assets
            if os.path.basename(asset.get("path", ""))
        }

        # Find file assets from source not in target assets
        missing_assets = [
            asset
            for asset in source_assets
            if os.path.basename(asset.get("path", ""))
            and asset.get("path", "").lstrip("/") not in target_asset_keys
        ]
    else:
        # If proxy is False, check both path and SHA1 checksum
        target_asset_keys = {
            (asset.get("path", "").lstrip("/"), asset["checksum"]["sha1"])
            for asset in target_assets
            if os.path.basename(asset.get("path", ""))
        }

        # Find file assets from source not in target assets
        missing_assets = [
            asset
            for asset in source_assets
            if os.path.basename(asset.get("path", ""))
            and (asset.get("path", "").lstrip("/"), asset["checksum"]["sha1"])
            not in target_asset_keys
        ]

    return missing_assets


def upload_asset(url, asset, asset_path):
    """
    Upload asset to specified Nexus repository.

    :param url: The base URL of the Nexus instance.
    :param asset: The asset to upload.
    :param asset_path: The path to the asset to upload.
    :return: None
    """

    logging.info(
        "Uploading asset %s to repository %s",
        asset.get("path"),
        asset.get("repository"),
    )
    data = None
    files = None
    params = {"repository": asset.get("repository")}
    repo_format = asset.get("format")
    upload_url = f"{url}/service/rest/v1/components"

    # Build files based on the repository format
    with open(asset_path, "rb") as file:
        if repo_format in ["helm", "docker", "npm", "pypi", "yum", "apt"]:
            files = {"{repo_format}.asset": file}
        # API doesn't support these formats properly
        elif repo_format in ["gitlfs", "conan"]:
            path = asset.get("path")
            repository = asset.get("repository")
            upload_url = f"{url}/repository/{repository}/{path}"
            response = requests.put(
                upload_url, auth=auth, data=file, timeout=120
            )
            response.raise_for_status()
            return
        elif repo_format in ["raw"]:
            files = {
                "raw.asset1": file,
            }
            data = {
                "raw.directory": (None, os.path.dirname(asset.get("path"))),
                "raw.asset1.filename": (
                    None,
                    os.path.basename(asset.get("path")),
                ),
            }
        else:
            raise ValueError(f"Unsupported repository format: {repo_format}")

        if data is None and files is not None:
            response = requests.post(
                upload_url,
                auth=auth,
                files=files,
                params=params,
                stream=True,
                timeout=120,
            )
        elif files is not None:
            response = requests.post(
                upload_url,
                auth=auth,
                data=data,
                files=files,
                params=params,
                stream=True,
                timeout=120,
            )

        for file in files.values():
            if hasattr(file, "close"):
                file.close()

    return


def parse_assets(repo_name, source_assets, target_assets, proxy=False):
    """
    Check if the asset already exists on the new instance

    :param repo_name: The name of the repository.
    :param source_assets: The assets to migrate.
    :param target_assets: The assets already present on the target Nexus.
    :param proxy: Flag to indicate if the repository is a proxy.
    :return: None
    """

    missing_assets = find_missing_assets(source_assets, target_assets, proxy)
    for asset in missing_assets:
        temp_dir = f"{NEXUS_TEMP_DIR}/{repo_name}/{asset.get('path')}"
        if not proxy:
            asset_path = download_component_asset(asset, temp_dir)
            try:
                upload_asset(target_url, asset, asset_path)
                logging.debug(
                    "Uploaded %s to %s",
                    asset.get("path"),
                    repo_name,
                )
            except requests.exceptions.HTTPError as e:
                logging.error(
                    "Failed to upload %s to %s: %s",
                    asset.get("path"),
                    repo_name,
                    e,
                )
            finally:
                # Clean up downloaded assets
                logging.debug(
                    "Cleaning up local assets for %s.",
                    asset.get("path"),
                )
                os.remove(asset_path)
        else:
            try:
                asset_path = download_proxy_asset(asset, temp_dir)
                # Clean up downloaded assets
                logging.debug(
                    "Cleaning up local assets for %s.",
                    source_assets,
                )
                os.remove(asset_path)
            except requests.exceptions.HTTPError as e:
                logging.error(
                    "Failed to download proxy asset %s from %s: %s",
                    asset.get("path"),
                    repo_name,
                    e,
                )


def migrate_assets():
    """
    Migrate components from old Nexus to new Nexus.

    :return: None
    """
    if not single_repo_name:
        old_repos = get_repositories(source_url)
        new_repos = get_repositories(target_url)

        new_repo_names = {repo["name"] for repo in new_repos}
        common_repos = [
            repo for repo in old_repos if repo["name"] in new_repo_names
        ]
    else:
        common_repos = [{"name": single_repo_name, "type": "hosted"}]

    for source_repo in common_repos:
        source_repo_name = source_repo.get("name")
        source_assets = get_assets(source_url, source_repo_name)
        target_assets = get_assets(target_url, source_repo_name)
        if source_repo.get("type") == "proxy":
            parse_assets(
                source_repo_name,
                source_assets,
                target_assets,
                proxy=True,
            )
        elif source_repo.get("type") == "hosted":
            parse_assets(
                source_repo_name,
                source_assets,
                target_assets,
                proxy=False,
            )


def check_assets():
    """
    Checks for assets not present on both nexus instances.

    :return: None
    """
    logging.info("Checking assets.")

    if not single_repo_name:
        old_repos = get_repositories(source_url)
        new_repos = get_repositories(target_url)

        new_repo_names = {repo["name"] for repo in new_repos}
        common_repos = [
            repo for repo in old_repos if repo["name"] in new_repo_names
        ]
    else:
        common_repos = [{"name": single_repo_name, "type": "hosted"}]

    for source_repo in common_repos:
        # Check hosted repositories
        if source_repo.get("type") == "hosted":
            source_repo_name = source_repo.get("name")
            source_assets = get_assets(source_url, source_repo_name)
            target_assets = get_assets(target_url, source_repo_name)
            missing_assets = find_missing_assets(
                source_assets, target_assets, proxy=False
            )

            for asset in missing_assets:
                logging.warning(
                    "Asset %s, created at %s, is missing from %s",
                    asset.get("path"),
                    asset.get("blobCreated"),
                    source_repo_name,
                )


def main_loop():
    """
    Main loop of the script.

    :return: None
    """

    start_time = time.time()
    try:
        os.mkdir(NEXUS_TEMP_DIR)
    except FileExistsError:
        logging.debug("%s already exists", NEXUS_TEMP_DIR)

    if not check_mode:
        logging.info(
            "Migration mode enabled. Assets that are not in the new Nexus\
                will be copied from the old Nexus."
        )
        migrate_assets()
    else:
        logging.info("Check mode enabled. No assets will be uploaded.")
        logging.info(
            "Script will only check if assets exist on the new Nexus.\
                In case the asset is missing, a WARNING will be raised."
        )
        logging.info(
            "To disable check mode, re-run script without --check flag."
        )
        check_assets()

    end_time = time.time()
    elapsed_time = end_time - start_time
    logging.info("Script executed in %.2f seconds.", elapsed_time)


if __name__ == "__main__":
    if daemon_mode:
        logging.info("Running sync in daemon mode.")
        while True:
            main_loop()
    else:
        main_loop()
